module.exports = {
  closedForMaintenance: false,
  PG_CONNECTION_STRING: 'pg://<user>:<password>@127.0.0.1:5432/<dbname>',
  SECRET: 'some secret string to hash stuff',
  db: 'dev db',
  mongooseConnectionString: 'mongodb://127.0.0.1/session_store',
  // mongooseConnectionStringForErrors: 'mongodb://127.0.0.1:27020/errors',
  useMinIO: false,
  minioConfig: {
    endPoint: '123.123.123.123',
    port: 9000,
    useSSL: false,
    accessKey: 'edgar',
    secretKey: '...',
  },
  mail: {
    intervalSeconds: 5 * 60, // how often will mailer daemon check/send mails
    waitBetweenMailsMinMillis: 500, // 500 is also a minimal allowed value
    waitBetweenMailsMaxMillis: 2500,
    batchSize: 50, // max number of recipients in the BCC field (eg. 370 recipient = 370 / 50 = 8 emails)
    host: 'smtp.gmail.com',
    secureConnection: false, // TLS requires secureConnection to be false
    port: 587, // port for secure SMTP
    user: 'changeme@gmail.com',
    from: 'changeme@fer.hr',
    password: '********',
    tls: {
      ciphers: 'SSLv3',
    },
  },
  redirectToOtherProxy: {
    fromProxy: 'some ip',
    toProxies: [
      {
        toProxyFullUrl: 'https://ed2.fer.hr',
        percentage: 20,
      },
    ],
  },
  googleAuth: {
    clientID: '<CLIENT_ID>',
    clientSecret: '<CLIENT_SECRET>',
    callbackURL: 'http://<ENDPOINT>/auth/google/callback',
  },
  facebookAuth: {
    clientID: '<CLIENT_ID>',
    clientSecret: '<CLIENT_SECRET>',
    callbackURL: 'http://<ENDPOINT>/auth/facebook/callback',
  },
  twitterAuth: {
    consumerKey: '<CONSUMER_KEY>',
    consumerSecret: '<CLIENT_SECRET>',
    callbackURL: 'http://<ENDPOINT>/auth/twitter/callback',
  },
  samlAAIAuth: {
    path: '<PATH>',
    entryPoint: '<ENTRYPOINT>',
    logoutUrl: '<LOGOUTURL>',
    logoutCallbackUrl: '<LOGOUTCALLBACKURL>',
    issuer: '<ISSUER>',
    cert: '<CERT>',
    proxyIssuerMap: {
      proxy_ip_1: 'issuer_for_ip1',
      proxy_ip_2: 'issuer_for_ip2',
    },
  },
  providers: {
    google: true,
    facebook: true,
    aai: true,
    twitter: true,
    local: true,
    dummy: true,
    aai: false,
  },
  register: {
    hashSaltRounds: 9,
    hashEncryptionSecret: '<SECRET>',
    tokenExpiry: 1000 * 2 * 7 * 24 * 3600, // 2 weeks
  },
  tmpPath: '/tmp',
};
