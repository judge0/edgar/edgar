'use strict';
var TwitterStrategy = require('passport-twitter').Strategy;
var winston = require('winston');
var db = require.main.require('./db').db;
var sessionInit = require.main.require('./config/strategies/sessionInit');
var config = require.main.require('./config/config');

var strategy = new TwitterStrategy(
  {
    consumerKey: config.twitterAuth.consumerKey,
    consumerSecret: config.twitterAuth.consumerSecret,
    callbackURL: config.twitterAuth.callbackURL,
    passReqToCallback: true,
  },
  function (req, token, tokenSecret, profile, done) {
    winston.info('User signed in with twitter, id: ' + profile.id);
    if (req.session.registerStudentID) {
      db.any(
        `UPDATE student SET (alt_id, provider) = ($(alt_id), $(provider))
                WHERE id = $(id) RETURNING id_app_user`,
        {
          alt_id: profile.id,
          provider: 'twitter',
          id: req.session.registerStudentID,
        }
      )
        .then((data) => {
          if (data[0].id_app_user) {
            db.any(
              `UPDATE app_user SET (username, provider) = ($(username), $(provider))
                            WHERE id = $(id)`,
              {
                username: profile.id,
                provider: 'twitter',
                id: data[0].id_app_user,
              }
            )
              .then(() => {
                return sessionInit.init(req, profile.id, 'twitter', done);
              })
              .catch((err) => {
                winston.error('Error updateing user in app_user table');
                return done(err);
              });
          } else {
            return sessionInit.init(req, profile.id, 'twitter', done);
          }
        })
        .catch((err) => {
          winston.error('Error updateing user in student table');
          return done(err);
        });
    } else {
      return sessionInit.init(req, profile.id, 'twitter', done);
    }
  }
);

module.exports = {
  attachStrategy(passport) {
    winston.info('Attaching twitter strategy...');
    passport.use(strategy);
  },
  logoutStrategy(req, res) {
    winston.info('twitterLogout');
    req.session.destroy(function (err) {
      if (err) {
        winston.error(err);
      }
      res.status(200).send();
    });
  },
};
