const TokenStrategy = require('passport-accesstoken').Strategy;
const winston = require('winston');
const sessionInit = require.main.require('./config/strategies/sessionInit');
const db = require.main.require('./db').db;

const tokenStrategy = new TokenStrategy(
  {
    tokenField: 'token',
    passReqToCallback: true,
  },
  (req, token, done) => {
    db.any(
      `
    SELECT student_id, alt_id AS username
    FROM student_token JOIN student
    ON student_token.student_id = student.id
    WHERE token = $(token)
    AND is_activated = TRUE
  `,
      { token }
    ).then((response) => {
      if (response.length) {
        const username = response[0].username;
        return sessionInit.init(req, username, 'token', done);
      } else {
        return done(null, false, { message: 'Unknown token' });
      }
    });
  }
);

module.exports = {
  attachStrategy(passport) {
    winston.info('Attaching token strategy...');
    passport.use(tokenStrategy);
  },
  logoutStrategy(req, res) {
    winston.info('tokenLogout');
    req.session.destroy((err) => {
      if (err) {
        winston.error(err);
      }
      res.status(200).send();
    });
  },
};
