'use strict';
var LocalStrategy = require('passport-local').Strategy;
var winston = require('winston');
var bcrypt = require('bcryptjs');
var config = require.main.require('./config/config');
var db = require.main.require('./db').db;
var sessionInit = require.main.require('./config/strategies/sessionInit');

var strategy = new LocalStrategy(
  {
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true,
  },
  function (req, username, password, done) {
    winston.info(username + ' ' + password);

    if (req.session.registerStudentID) {
      db.any(
        `SELECT * FROM local_provider
                    WHERE username = $(username) AND id_student != $(id_student)`,
        { username: username, id_student: req.session.registerStudentID }
      ).then((data) => {
        if (data.length) {
          winston.info(`Username exists -> ${username}.`);
          done(null, false, req.flash('auth', 'Username taken.'));
        } else {
          db.any(
            `UPDATE student SET (alt_id, provider) = ($(alt_id), $(provider))
                                WHERE id = $(id) RETURNING id_app_user`,
            {
              alt_id: username,
              provider: 'local',
              id: req.session.registerStudentID,
            }
          )
            .then((data) => {
              if (data[0].id_app_user) {
                db.any(
                  `UPDATE app_user SET (username, provider) = ($(username), $(provider))
                                            WHERE id = $(id)`,
                  {
                    username: username,
                    provider: 'local',
                    id: data[0].id_app_user,
                  }
                )
                  .then(() => {
                    bcrypt.hash(password, config.register.hashSaltRounds, (err, hash) => {
                      if (err) {
                        winston.error('Error hashing user password: ' + err);
                        return done(err);
                      }

                      db.none(
                        `INSERT INTO local_provider (id_student, username, password)
                                                     VALUES ($(id_student), $(username), $(password)) 
                                                     ON CONFLICT (id_student) DO UPDATE
                                                     SET (username, password) = ($(username), $(password))`,
                        {
                          id_student: req.session.registerStudentID,
                          username: username,
                          password: hash,
                        }
                      )
                        .then(() => {
                          return sessionInit.init(req, username, 'local', done);
                        })
                        .catch((error) => {
                          winston.error('Error upserting user in local_provider: ' + error);
                          return done(error);
                        });
                    });
                  })
                  .catch((err) => {
                    winston.error('Error updateing user in app_user table');
                    return done(err);
                  });
              } else {
                bcrypt.hash(password, config.register.hashSaltRounds, (err, hash) => {
                  if (err) {
                    winston.error('Error hashing user password: ' + err);
                    return done(err);
                  }

                  db.none(
                    `INSERT INTO local_provider (id_student, username, password)
                                                 VALUES ($(id_student), $(username), $(password)) 
                                                 ON CONFLICT (id_student) DO UPDATE
                                                 SET (username, password) = ($(username), $(password))`,
                    {
                      id_student: req.session.registerStudentID,
                      username: username,
                      password: hash,
                    }
                  )
                    .then(() => {
                      return sessionInit.init(req, username, 'local', done);
                    })
                    .catch((error) => {
                      winston.error('Error upserting user in local_provider: ' + error);
                      return done(error);
                    });
                });
              }
            })
            .catch((err) => {
              winston.error('Error updateing user in student table');
              return done(err);
            });
        }
      });
    } else {
      db.any(
        `SELECT password 
                    FROM local_provider 
                    WHERE username = $(username)`,
        { username: username }
      )
        .then((data) => {
          if (data.length) {
            bcrypt.compare(password, data[0].password, (err, res) => {
              if (err) {
                winston.error('Error comparing local passwords: ' + err);
                done(error);
              }

              if (res) {
                return sessionInit.init(req, username, 'local', done);
              } else {
                winston.info(`Bad password for local user -> ${username}.`);
                done(null, false, req.flash('auth', 'Unknown user or bad password, go figure.'));
              }
            });
          } else {
            winston.info(`Unknown local user -> ${username}.`);
            done(null, false, req.flash('auth', 'Unknown user or bad password, go figure.'));
          }
        })
        .catch((err) => {
          winston.error("Error retrieving user's local password: " + err);
          done(error);
        });
    }
  }
);

module.exports = {
  attachStrategy: function (passport) {
    winston.info('Attaching local strategy...');
    passport.use(strategy);
  },
  logoutStrategy(req, res) {
    winston.info('localLogout for ' + req.session.passport.user);
    req.session.destroy(function (err) {
      if (err) {
        req.flash('error', 'Error logging out: ' + JSON.stringify(err));
        winston.error(err);
      }
      res.redirect('/auth/logout');
    });
  },
};
