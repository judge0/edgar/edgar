import HttpService from './HttpService';

export = class HttpServiceCommonJS {
  // This is just a bridge so that CommonJS modules can use CodeRunnerService as well as ES6 modules/typescript.
  public static async postJsonToUrl(host, path, port, payload) {
    return HttpService.postJsonToUrl(host, path, port, payload);
  }
};
