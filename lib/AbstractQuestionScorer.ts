import Score from "./Score";

export default abstract class AbstractQuestionScorer {
  // Remove all whitespace that cannot be seen by the user.
  // That means trailing WS at every line and trailing newlines at the very end
  trimWhiteSpace(s, leadingWhitespace: boolean = false): string {
    // /\s+$/:  matches any whitespace character(equal to[\r\n\t\f\v ]) $ asserts position at the end of the string
    return s
      .split('\n') // split by newlines
      .map((item) => (leadingWhitespace ? item.trim() : item.replace(/\s+$/, ''))) // trim trailing whitespace in every line
      .join('\n') // join back to single string separated with newlines
      .replace(/\s+$/, ''); // remove all whitespace at the very end of master string : this could be multiple \n's.
  }

  getScoreFromPenalty(penalty: number, gradingModel: GradingModel): Score {

    if (penalty > 100) penalty = 100; // to be sure...
    if (penalty < 0) penalty = 0; // to be sure...

    let score = gradingModel.correct_score - (penalty * (gradingModel.correct_score - gradingModel.incorrect_score)) / 100;

    return {
      is_correct: penalty === 0,
      is_incorrect: penalty === 100,
      is_unanswered: false,
      is_partial: penalty > 0 && penalty < 100,
      score,
      score_perc: gradingModel.correct_score !== 0 ? score / gradingModel.correct_score : 0,
      hint:
        penalty > 0 && penalty < 100
          ? `Partial score: penalty: ${penalty}%`
          : undefined,
    };

  }
}
