let winston = require('winston');
import { IQuestionScorer } from './IQuestionScorer';
import Score from './Score';
import { ScoreResult } from './Score';
import AbstractQuestionScorer from './AbstractQuestionScorer';

export default class DiagramQuestionScorer extends AbstractQuestionScorer implements IQuestionScorer {
  private gradingModel: GradingModel;

  constructor(model: GradingModel) {
    super();
    this.gradingModel = model;
  }

  getName(): string {
    return 'Default Diagram Question Scorer';
  }

  getScore(record): Promise<ScoreResult> {
    try {
      let { diagram_answer, student_answer_text } = record;

      let scoreResult: ScoreResult = {
        score: null,
        record: record,
      };
      if (student_answer_text === null || student_answer_text.trim().length === 0) {
        scoreResult.score = Score.newUnansweredScore(this.gradingModel);
        return Promise.resolve(scoreResult);
      }

      let penalty = 0;
      let normalizedCorrect = this.trimWhiteSpace(diagram_answer, true);
      let normalizedStudent = this.trimWhiteSpace(student_answer_text, true);

      if (normalizedCorrect === normalizedStudent) {
        penalty = 0;
      } else {
        penalty = 100;
      }

      scoreResult.score = this.getScoreFromPenalty(penalty, this.gradingModel);
      if (penalty > 0) scoreResult.score.hint = `student <> correct :\n${normalizedStudent}\n != \n${normalizedCorrect}`;
      return Promise.resolve(scoreResult);

    } catch (error) {
      winston.error(error);
      return Promise.resolve({
        score: Score.newEmptyScore('Error occured during grading of diagram question.'),
        record: record,
        success: true,
      });
    }
  }
}
