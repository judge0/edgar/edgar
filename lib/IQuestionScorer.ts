import { ScoreResult } from './Score';

export interface IQuestionScorerConstructor {
  new (gradingModel: GradingModel): IQuestionScorer;
}
export interface IQuestionScorer {
  getName(): string;
  getScore(args: any, priority?: number): Promise<ScoreResult>;
}
