let winston = require('winston');
import { IQuestionScorer } from './IQuestionScorer';
import Score from './Score';
import { ScoreResult } from './Score';
export default class FreeTextQuestionScorer implements IQuestionScorer {
  private gradingModel: GradingModel;

  constructor(model: GradingModel) {
    this.gradingModel = model;
  }

  getName(): string {
    return 'Free Text Question Scorer';
  }

  getScore(record): Promise<ScoreResult> {
    try {
      let scoreResult: ScoreResult = {
        score: Score.newCorrectScore(this.gradingModel),
        record: record,
      };

      return Promise.resolve(scoreResult);
    } catch (error) {
      winston.error(error);
      return Promise.resolve({
        score: Score.newEmptyScore('Error occured during grading...'),
        record: record,
        success: true,
      });
    }
  }
}
