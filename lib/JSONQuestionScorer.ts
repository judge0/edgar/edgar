let winston = require('winston');
const utils = require('./../common/utils');
const assert = require('assert');
const assertDiff = require('assert-diff');
import { ScoreResult } from './Score';
import Score from './Score';
import { IQuestionScorer } from './IQuestionScorer';
import CodeRunnerService from './CodeRunnerService';

export default class JSONQuestionScorer implements IQuestionScorer {
  private gradingModel: GradingModel;
  constructor(model: GradingModel) {
    this.gradingModel = model;
  }

  getName(): string {
    return 'Default JSON Question Scorer';
  }

  async getScore(rec): Promise<ScoreResult> {
    try {
      // Merged below, when invokeing execJSCodeQuestion(...)
      // let corrCode = utils.mergeSQLCode(
      //     rec.json_test_fixture,
      //     rec.json_answer,
      //     rec.json_alt_assertion
      // );
      // let studCode = utils.mergeSQLCode(
      //     rec.json_test_fixture,
      //     rec.student_answer_code,
      //     rec.json_alt_assertion
      // );
      let res = await this.execAndScoreJSONSequential(
        rec.id_question,
        rec.student_answer_code,
        rec.json_answer,
        rec.assert_deep,
        rec.assert_strict
      );
      res.record = rec;
      return res;
    } catch (error) {
      winston.error(error);
      return {
        score: Score.newEmptyScore('Error occured during grading...'),
        record: rec,
        data: 'Error occured:' + JSON.stringify(error),
        success: true,
      };
    }
  }

  getJSONQuestionScore(userData, correctData, assertDeep, assertStrict): Score {
    if (!correctData) {
      return Score.newUnansweredScore(
        this.gradingModel,
        "Question has a wrong answer (teacher's error!)!?"
      );
    }
    try {
      if (assertDeep && assertStrict) {
        assert.deepStrictEqual(userData, correctData);
      } else if (assertDeep) {
        assert.deepEqual(userData, correctData);
      } else if (assertStrict) {
        assert.strictEqual(userData, correctData);
      } else {
        assert.equal(userData, correctData);
      }
      return Score.newCorrectScore(this.gradingModel);
    } catch (error) {
      assertDiff.options.strict = assertStrict;
      return Score.newInCorrectScore(
        this.gradingModel,
        this.getAssertDiff(userData, correctData, assertDeep, assertStrict)
      );
    }
  }

  getAssertDiff(userData, correctData, assertDeep, assertStrict): string {
    try {
      assertDiff.options.strict = assertStrict;
      assertDiff.deepEqual(
        userData,
        correctData,
        `Object differences (assertDeep = ${assertDeep}, assertStrict = ${assertStrict}):`
      );
      return 'Objects are equal!? Maybe you need assert deep.';
    } catch (error) {
      // bcs some guy on the internet said so: https://stackoverflow.com/questions/25245716/remove-all-ansi-colors-styles-from-strings
      let rv = error.message.replace(
        /[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]/g,
        ''
      );
      if (rv.length > 1000) rv = `${rv.substring(0, 999)}\n... (more than 1000 chars, truncated)`;
      return rv;
    }
  }
  async execAndScoreJSONSequential(
    id_question,
    userJS,
    correctJS,
    assertDeep,
    assertStrict
  ): Promise<ScoreResult> {
    if (userJS.trim() === '') {
      return {
        score: Score.newUnansweredScore(this.gradingModel, 'Not executed: empty answer.'),
        data: 'Not executed: empty answer.',
        success: true,
      };
    } else {
      let score: Score;
      let userResult: any = await CodeRunnerService.execJSCodeQuestion(
        id_question,
        userJS,
        true,
        false
      );
      if (userResult.success) {
        let corrResult: any = await CodeRunnerService.execJSCodeQuestion(
          id_question,
          correctJS,
          true,
          false
        );
        if (
          // Sort by key before compare for newer versions of Mongo:
          Array.isArray(userResult.data) &&
          Array.isArray(corrResult.data) &&
          userResult.data
            .map((x) => (x['_id'] ? 1 : 0))
            .reduce((partialSum, a) => partialSum + a, 0) === userResult.data.length &&
          corrResult.data
            .map((x) => (x['_id'] ? 1 : 0))
            .reduce((partialSum, a) => partialSum + a, 0) === corrResult.data.length
        ) {
          userResult.data.sort((a, b) => {
            let crca = utils.objCRC32(a['_id']);
            let crcb = utils.objCRC32(b['_id']);
            if (crca < crcb) return -1;
            if (crca > crcb) return 1;
            return 0;
          });
          corrResult.data.sort((a, b) => {
            let crca = utils.objCRC32(a['_id']);
            let crcb = utils.objCRC32(b['_id']);
            if (crca < crcb) return -1;
            if (crca > crcb) return 1;
            return 0;
          });
        }
        score = this.getJSONQuestionScore(
          userResult.data,
          corrResult.data,
          assertDeep,
          assertStrict
        );
      } else {
        score = Score.newInCorrectScore(this.gradingModel, JSON.stringify(userResult.error));
      }
      return {
        score,
        ...userResult, // TODO: strongly type this?
      };
    }
  }
}
