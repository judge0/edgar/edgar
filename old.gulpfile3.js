var gulp = require('gulp');
var jsFiles = [
  '!./angular/**/*.*',
  '!./node_modules/**/*.*',
  '!./public/lib/**/*.js',
  '!./public/assets/**/*.js',
  './*.js',
  '**/*.js',
];
var gulpprint = require('gulp-print');
var nodemon = require('gulp-nodemon');

// gulp-util - https://www.npmjs.com/package/gulp-util
var gutil = require('gulp-util');
// Minimist - https://www.npmjs.com/package/minimist
var argv = require('minimist')(process.argv);
// gulp-rsync - https://www.npmjs.com/package/gulp-rsync

var prompt = require('gulp-prompt');
// gulp-if - https://www.npmjs.com/package/gulp-if
var gulpif = require('gulp-if');
// gulp-typescript - https://www.npmjs.com/package/gulp-typescript
var ts = require('gulp-typescript');

const mocha = require('gulp-mocha');

gulp.task('test', () => {
  return (
    gulp
      .src('./test/*.js', { read: false })
      // gulp-mocha needs filepaths so you can't have any plugins before it
      .pipe(
        mocha({
          require: ['should'],
          //timeout: 3000
          //reporter: 'nyan'
        })
      )
  ); // {reporter: 'nyan'}
});
gulp.task('test2', () => {
  return (
    gulp
      .src('./test/*.js', { read: false })
      // gulp-mocha needs filepaths so you can't have any plugins before it
      .pipe(
        mocha({
          require: ['should'],
          //timeout: 3000
          reporter: 'nyan',
        })
      )
  ); // {reporter: 'nyan'}
});
gulp.task('test-core', () => {
  return (
    gulp
      .src(['./test/*EvalLibCore*.js'], { read: false })
      // gulp-mocha needs filepaths so you can't have any plugins before it
      .pipe(
        mocha({
          require: ['should'],
          //timeout: 3000
          //reporter: 'nyan'
        })
      )
  ); // {reporter: 'nyan'}
});

gulp.task('test-c', () => {
  return (
    gulp
      .src('./test/*CodeRunner*.js', { read: false })
      // gulp-mocha needs filepaths so you can't have any plugins before it
      .pipe(
        mocha({
          require: ['should'],
          //timeout: 3000
          //reporter: 'nyan'
        })
      )
  ); // {reporter: 'nyan'}
});

// gulp.task('test-sql', () => {
//   return (
//     gulp
//       .src('./test/*SQL.js', { read: false })
//       // gulp-mocha needs filepaths so you can't have any plugins before it
//       .pipe(
//         mocha({
//           require: ['should'],
//           //timeout: 3000
//           //reporter: 'nyan'
//         })
//       )
//   ); // {reporter: 'nyan'}
// });

// gulp.task('devtest', () => {
//   return (
//     gulp
//       .src('./test/testEvalLibScore.js', { read: false })
//       // gulp-mocha needs filepaths so you can't have any plugins before it
//       .pipe(
//         mocha({
//           require: ['should'],
//           //timeout: 3000
//           //reporter: 'nyan'
//         })
//       )
//   ); // {reporter: 'nyan'}
// });

// gulp.task('lint', function() {
//     return gulp.src(jsFiles)
//         .pipe(jshint())
//         .pipe(jshint.reporter('jshint-stylish', {
//             verbose: true
//         }));
// });

// gulp.task('copy-angular', function () {
//   return gulp
//     .src([
//       './node_modules/es6-shim/es6-shim.min.js',
//       './node_modules/systemjs/dist/system-polyfills.js',
//       './node_modules/angular2/bundles/angular2-polyfills.js',
//       './node_modules/systemjs/dist/system.src.js',
//       './node_modules/rxjs/bundles/Rx.js',
//       './node_modules/angular2/bundles/angular2.dev.js',
//       './node_modules/angular2/bundles/router.dev.js',
//       './node_modules/angular2/bundles/http.dev.js',
//     ])
//     .pipe(gulp.dest('./public/lib/angular'));
// });
// gulp.task('inject', function() {
//     var wiredep = require('wiredep').stream;
//     var inject = require('gulp-inject');
//     var options = {
//         bowerJson: require('./bower.json'),
//         directory: './public/lib',
//         ignorePath: '../public',
//         cwd: './'
//     };
//     var injectSrc = gulp.src(['./public/css/**/*.css', './public/js/**/*.js'], {
//         read: false
//     });
//     var injectOptions = {
//         ignorePath: 'public'
//     };
//     return gulp.src('./views/**/*.*')
//         .pipe(wiredep(options))
//         .pipe(inject(injectSrc, injectOptions))
//         .pipe(gulp.dest('./views'));
// });

// gulp.task('dev-lint', gulp.series('lint', 'copy-angular'), function() {
//     var options = {
//         execMap: {
//             // 'js': 'pm2 start server.js --node-args='--harmony --use_strict''
//             'js': 'npm start --harmony --use_strict'
//         },
//         script: 'server.js',
//         delay: 3500,
//         ext: 'js html',
//         env: {
//             'NODE_ENV': 'development',
//             'PORT': 1337,
//             'ADMIN_EMAIL': 'edgar.webapp@gmail.com'
//         },
//         ignore: ['.git', 'node_modules'],
//         //watch: jsFiles
//     };
//     return nodemon(options)
//         .on('restart', function(argument) {
//             console.log('Restarting ...');
//         });
// });

gulp.task('start-nodemon', function (done) {
  var options = {
    execMap: {
      js: 'npm start --harmony --use_strict',
    },
    script: 'server.js',
    delay: 1500,
    ext: 'js html',
    env: {
      NODE_ENV: 'development',
      PORT: 1337,
      METRICS: true,
      ADMIN_EMAIL: 'edgar.webapp@gmail.com',
    },
    ignore: ['.git', 'node_modules', 'ng'],
    done: done,
    //watch: jsFiles
  };
  nodemon(options).on('restart', function (argument) {
    console.log('Restarting ...');
  });
});

gulp.task('compile-lib', function () {
  const tsProject = ts.createProject('lib/tsconfig.json');
  return tsProject.src().pipe(tsProject()).js.pipe(gulp.dest('lib'));
});

gulp.task('dev', gulp.series('compile-lib', 'copy-angular', 'start-nodemon'), function (done) {
  console.log('Good luck!');
  done();
});

// gulp.task('postman', function () {
//   var options = {
//     execMap: {
//       js: 'node --use_strict',
//     },
//     script: 'postman.js',
//     delay: 1500,
//     ext: 'js html',
//     env: {
//       NODE_ENV: 'development',
//       ADMIN_EMAIL: 'igor.mekterovic@fer.hr',
//     },
//     ignore: ['.git', 'node_modules'],
//   };
//   return nodemon(options).on('restart', function (argument) {
//     console.log('Restarting ...');
//   });
// });

// gulp.task('start-production', gulp.series('copy-angular'), function () {
//   var options = {
//     execMap: {
//       // 'js': 'pm2 start server.js --node-args='--harmony --use_strict''
//       js: 'npm start --harmony --use_strict',
//     },
//     script: 'server.js',
//     delay: 1500,
//     ext: 'js html',
//     env: {
//       NODE_ENV: 'production',
//       PORT: 1337,
//       ADMIN_EMAIL: 'edgar.webapp@gmail.com',
//     },
//     ignore: ['.git', 'node_modules'],
//   };
//   return nodemon(options).on('restart', function (argument) {
//     console.log('Restarting ...');
//   });
// });

// gulp.task('deploy', function () {
//   // // Dirs and Files to sync
//   // var rsyncPaths = ['./'];

//   // // Default options for rsync
//   // var rsyncConf = {
//   //     progress: true,
//   //     incremental: true,
//   //     relative: true,
//   //     emptyDirectories: true,
//   //     recursive: true,
//   //     clean: true,
//   //     exclude: ['/node_modules', '/typings*', '/test*', '/junk', '/.git*', '/db/errors.log'],
//   // };

//   // Staging
//   if (argv.staging) {
//     // rsyncConf.hostname = ''; // hostname
//     // rsyncConf.username = ''; // ssh username
//     // rsyncConf.destination = ''; // path where uploaded files go
//     // Production
//   } else if (argv.production) {
//     // rsyncConf.hostname = 'minerva.zpr.fer.hr'; // hostname
//     // rsyncConf.username = 'root'; // ssh username
//     // rsyncConf.destination = '/opt/apps/a3'; // path where uploaded files go
//   } else if (argv.oop1) {
//     gulp.series('deploy-edgar1')();
//     // rsyncConf.hostname = '161.53.19.149'; // hostname
//     // rsyncConf.username = 'root'; // ssh username
//     // rsyncConf.destination = '/opt/apps/a3'; // path where uploaded files go
//   } else if (argv.ed) {
//     gulp.series('deploy-ed')();
//     // rsyncConf.hostname = '161.53.78.88'; // hostname
//     // rsyncConf.username = 'root'; // ssh username
//     // rsyncConf.destination = '/opt/apps/a3'; // path where uploaded files go
//   } else if (argv.ed2) {
//     gulp.series('deploy-ed2')();
//     // rsyncConf.hostname = '161.53.78.93'; // hostname
//     // rsyncConf.username = 'root'; // ssh username
//     // rsyncConf.destination = '/opt/apps/a3'; // path where uploaded files go
//   } else {
//     throwError('deploy', gutil.colors.red('Missing or invalid target'));
//   }

//   // Use gulp-rsync to sync the files
//   // return gulp.src(rsyncPaths)
//   //     .pipe(gulpif(
//   //         argv.production,
//   //         prompt.confirm({
//   //             message: 'Heads Up! Are you SURE you want to push to PRODUCTION?',
//   //             default: false
//   //         })
//   //     ))
//   //     .pipe(gulpif(
//   //         argv.oop1,
//   //         prompt.confirm({
//   //             message: 'Heads Up! Are you SURE you want to push to edgar1.fer.hr (OOP1): PRODUCTION?',
//   //             default: false
//   //         })
//   //     ))
//   //     .pipe(gulpif(
//   //         argv.ed,
//   //         prompt.confirm({
//   //             message: 'Heads Up! Are you SURE you want to push to ed.fer.hr: PRODUCTION?',
//   //             default: false
//   //         })
//   //     ))
//   //     .pipe(rsync(rsyncConf));
// });
// gulp.task('deploy-edgar1', function () {
//   // Dirs and Files to sync
//   var rsyncPaths = ['./'];

//   // Default options for rsync
//   var rsyncConf = {
//     progress: true,
//     incremental: true,
//     relative: true,
//     emptyDirectories: true,
//     recursive: true,
//     clean: true,
//     exclude: ['/node_modules', '/typings*', '/test*', '/junk', '/.git*', '/db/errors.log'],
//   };

//   let hostname = 'edgar1.fer.hr (OOP1)';
//   rsyncConf.hostname = '161.53.19.149'; // hostname
//   rsyncConf.username = 'root'; // ssh username
//   rsyncConf.destination = '/opt/apps/a3'; // path where uploaded files go

//   // Use gulp-rsync to sync the files
//   return gulp
//     .src(rsyncPaths)
//     .pipe(
//       gulpif(
//         true,
//         prompt.confirm({
//           message: 'Heads Up! Are you SURE you want to push to ' + hostname + ': PRODUCTION?',
//           default: false,
//         })
//       )
//     )
//     .pipe(rsync(rsyncConf));
// });
// gulp.task('deploy-ed', function () {
//   // Dirs and Files to sync
//   var rsyncPaths = ['./'];

//   // Default options for rsync
//   var rsyncConf = {
//     progress: true,
//     incremental: true,
//     relative: true,
//     emptyDirectories: true,
//     recursive: true,
//     clean: true,
//     exclude: ['/node_modules', '/typings*', '/test*', '/junk', '/.git*', '/db/errors.log'],
//   };

//   let hostname = 'ed.fer.hr';
//   rsyncConf.hostname = '161.53.78.88'; // hostname
//   rsyncConf.username = 'root'; // ssh username
//   rsyncConf.destination = '/opt/apps/a3'; // path where uploaded files go

//   // Use gulp-rsync to sync the files
//   return gulp
//     .src(rsyncPaths)
//     .pipe(
//       gulpif(
//         true,
//         prompt.confirm({
//           message: 'Heads Up! Are you SURE you want to push to ' + hostname + ': PRODUCTION?',
//           default: false,
//         })
//       )
//     )
//     .pipe(rsync(rsyncConf));
// });
// gulp.task('deploy-ed2', function () {
//   // Dirs and Files to sync
//   var rsyncPaths = ['./'];

//   // Default options for rsync
//   var rsyncConf = {
//     progress: true,
//     incremental: true,
//     relative: true,
//     emptyDirectories: true,
//     recursive: true,
//     clean: true,
//     exclude: ['/node_modules', '/typings*', '/test*', '/junk', '/.git*', '/db/errors.log'],
//   };

//   let hostname = 'ed2.fer.hr';
//   rsyncConf.hostname = '161.53.78.93'; // hostname
//   rsyncConf.username = 'root'; // ssh username
//   rsyncConf.destination = '/opt/apps/a3'; // path where uploaded files go

//   // Use gulp-rsync to sync the files
//   return gulp
//     .src(rsyncPaths)
//     .pipe(
//       gulpif(
//         true,
//         prompt.confirm({
//           message: 'Heads Up! Are you SURE you want to push to ' + hostname + ': PRODUCTION?',
//           default: false,
//         })
//       )
//     )
//     .pipe(rsync(rsyncConf));
// });
// gulp.task('deploy-edgar1-light', function () {
//   // Dirs and Files to sync
//   var rsyncPaths = ['./'];

//   // Default options for rsync
//   var rsyncConf = {
//     progress: true,
//     incremental: true,
//     relative: true,
//     emptyDirectories: true,
//     recursive: true,
//     clean: true,
//     exclude: [
//       '/public',
//       '/node_modules',
//       '/typings*',
//       '/test*',
//       '/junk',
//       '/.git*',
//       '/db/errors.log',
//     ],
//   };

//   let hostname = 'edgar1.fer.hr (OOP1)';
//   rsyncConf.hostname = '161.53.19.149'; // hostname
//   rsyncConf.username = 'root'; // ssh username
//   rsyncConf.destination = '/opt/apps/a3'; // path where uploaded files go

//   // Use gulp-rsync to sync the files
//   return gulp
//     .src(rsyncPaths)
//     .pipe(
//       gulpif(
//         true,
//         prompt.confirm({
//           message: 'Heads Up! Are you SURE you want to push to ' + hostname + ': PRODUCTION?',
//           default: false,
//         })
//       )
//     )
//     .pipe(rsync(rsyncConf));
// });
// gulp.task('deploy-ed-light', function () {
//   // Dirs and Files to sync
//   var rsyncPaths = ['./'];

//   // Default options for rsync
//   var rsyncConf = {
//     progress: true,
//     incremental: true,
//     relative: true,
//     emptyDirectories: true,
//     recursive: true,
//     clean: true,
//     exclude: [
//       '/public',
//       '/node_modules',
//       '/typings*',
//       '/test*',
//       '/junk',
//       '/.git*',
//       '/db/errors.log',
//     ],
//   };

//   let hostname = 'ed.fer.hr';
//   rsyncConf.hostname = '161.53.78.88'; // hostname
//   rsyncConf.username = 'root'; // ssh username
//   rsyncConf.destination = '/opt/apps/a3'; // path where uploaded files go

//   // Use gulp-rsync to sync the files
//   return gulp
//     .src(rsyncPaths)
//     .pipe(
//       gulpif(
//         true,
//         prompt.confirm({
//           message: 'Heads Up! Are you SURE you want to push to ' + hostname + ': PRODUCTION?',
//           default: false,
//         })
//       )
//     )
//     .pipe(rsync(rsyncConf));
// });
// gulp.task('deploy-ed2-light', function () {
//   // Dirs and Files to sync
//   var rsyncPaths = ['./'];

//   // Default options for rsync
//   var rsyncConf = {
//     progress: true,
//     incremental: true,
//     relative: true,
//     emptyDirectories: true,
//     recursive: true,
//     clean: true,
//     exclude: [
//       '/public',
//       '/node_modules',
//       '/typings*',
//       '/test*',
//       '/junk',
//       '/.git*',
//       '/db/errors.log',
//     ],
//   };

//   let hostname = 'ed2.fer.hr';
//   rsyncConf.hostname = '161.53.78.93'; // hostname
//   rsyncConf.username = 'root'; // ssh username
//   rsyncConf.destination = '/opt/apps/a3'; // path where uploaded files go

//   // Use gulp-rsync to sync the files
//   return gulp
//     .src(rsyncPaths)
//     .pipe(
//       gulpif(
//         true,
//         prompt.confirm({
//           message: 'Heads Up! Are you SURE you want to push to ' + hostname + ': PRODUCTION?',
//           default: false,
//         })
//       )
//     )
//     .pipe(rsync(rsyncConf));
// });

// gulp.task('deploy-all', gulp.series(['deploy-edgar1', 'deploy-ed', 'deploy-ed2']), function (done) {
//   console.log('Good luck!');
//   done();
// });
// gulp.task(
//   'deploy-all-light',
//   gulp.series(['deploy-edgar1-light', 'deploy-ed-light', 'deploy-ed2-light']),
//   function (done) {
//     console.log('Good luck!');
//     done();
//   }
// );

// function throwError(taskName, msg) {
//   throw new gutil.PluginError({
//     plugin: taskName,
//     message: msg,
//   });
// }
