'use strict';
const express = require('express');
const router = express.Router();
const middleware = require('../common/middleware');
const globals = require('../common/globals');
const utils = require('../common/utils');
const db = require('../db').db;
const winston = require('winston');

const tutorialAnalyticsService = require('../services/tutorialAnalyticsService');

router.get(
  '/:id_tutorial([0-9]{1,10})/groups',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res, next) => {
    const data = await tutorialAnalyticsService.byGroups(
      parseInt(req.params.id_tutorial),
      req.session.currCourseId,
      req.session.currAcademicYearId
    );

    const chartData = data.data.map((row) =>
      Object.entries(row)
        .map(([key, value]) => (!isNaN(key) ? parseInt(key) : null))
        .filter((x) => x != null)
    );

    res.appRender(
      'groupsView',
      {
        analytics: {
          headers: [
            {
              class_group: 'Class group',
            },
            {
              started: 'Students started',
            },
            {
              finished: 'Students finished',
            },
            ...data.ordinals.map((o) => ({
              [o.ordinal]: o.ordinal,
            })),
          ],
          rows: data.data,
        },
        chartData: chartData,
      },
      'groupsViewScript',
      'groupsViewCSS'
    );
  }
);

router.get(
  '/:id_tutorial([0-9]{1,10})/students',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res, next) => {
    const data = await tutorialAnalyticsService.byStudents(
      parseInt(req.params.id_tutorial),
      req.session.currCourseId,
      req.session.currAcademicYearId
    );

    res.appRender('studentsView', {
      analytics: {
        headers: [
          {
            image: 'Image',
            __raw: true,
            __formatter: function (row) {
              return utils.getTinyImageHtml(row.alt_id2);
            },
          },
          {
            student: 'Student',
            __raw: true,
          },
          {
            class_group: 'Class group',
          },
          {
            is_finished: 'Finished',
          },
          {
            ts_created: 'Created at',
          },
          {
            ts_finished: 'Finished at',
          },
          ...data.ordinals.map((o) => ({
            [o.ordinal]: o.ordinal,
          })),
        ],
        rows: data.data,
        links: [
          {
            label: 'Log',
            href: (row) => `student/${row.id_student}/log`,
          },
        ],
      },
    });
  }
);

router.get(
  '/:id_tutorial([0-9]{1,10})/student/:id_student([0-9]{1,10})/log',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res, next) => {
    const data = await tutorialAnalyticsService.studentLog(
      parseInt(req.params.id_tutorial),
      req.session.currCourseId,
      parseInt(req.params.id_student)
    );

    res.appRender('studentLogView', {
      events: data.events,
    });
  }
);

module.exports = router;
