'use strict';
const express = require('express');
const router = express.Router();
const middleware = require('../common/middleware');
const globals = require('../common/globals');
const db = require('../db').db;
const winston = require('winston');

const exerciseService = require('../services/exerciseService');

router.get('/', async (req, res, next) => {
  const data = await exerciseService.listExercises(
    req.session.currCourseId,
    req.session.currAcademicYearId,
    req.session.studentId,
    req.session.rolename === globals.ROLES.STUDENT
  );

  res.appRender('listExercisesView', {
    exercises: {
      headers: [
        {
          title: 'Title',
        },
        {
          description: 'Description',
        },
        {
          no_questions: 'Questions',
        },
      ],
      rows: data,
      buttons: [
        {
          label: 'Start',
          method: 'GET',
          action: (row) => `/exercise/${row['id']}`,
        },
      ],
    },
  });
});

router.get(
  '/:id_exercise([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  (req, res, next) => {
    res.publicRender('exercise/exerciseView'); // break out of Layout - "full screen"
  }
);

// API routes

router.get(
  '/api/:id_exercise([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async (req, res, next) => {
    try {
      const data = await exerciseService.getExercise(parseInt(req.params.id_exercise));

      winston.debug('Fetching exercise id ' + req.params.id_exercise);

      res.json({
        ...data,
        cmSkin: req.session.cmSkin,
      });
    } catch (err) {
      winston.error(
        `Failed to fetch exercise id ${req.params.id_exercise}: ` + JSON.stringify(err)
      );
      res.json({
        success: false,
      });
    }
  }
);

router.post(
  '/api/start/:id_exercise([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.STUDENT, globals.ROLES.TEACHER]),
  async (req, res, next) => {
    try {
      const status = await exerciseService.start(
        parseInt(req.params.id_exercise),
        req.session.studentId
      );
      req.session.currExerciseId = parseInt(req.params.id_exercise);
      winston.debug(
        `Student ${req.session.studentId} starts/continues exercise at step ${status.latestQuestionOrdinal}`
      );
      res.json(status);
    } catch (error) {
      winston.error(`Failed to start exercise ${req.params.id_exercise} ` + JSON.stringify(error));
      res.json({
        success: false,
      });
    }
  }
);

router.get(
  '/api/:id_exercise([0-9]{1,10})/question/:question_ordinal',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async (req, res, next) => {
    try {
      const question = await exerciseService.getQuestion(
        parseInt(req.params.id_exercise),
        req.session.studentId,
        parseInt(req.params.question_ordinal),
        parseInt(req.query.difficultyLevelId)
      );
      req.session.currExerciseId = parseInt(req.params.id_exercise);
      res.json(question);
    } catch (error) {
      winston.error(
        `Error when fetching exercise ${req.params.id_exercise} question ordinal ${req.params.question_ordinal} ` +
          JSON.stringify(error)
      );
      res.json({
        success: false,
      });
    }
  }
);

router.put(
  '/api/:id_exercise([0-9]{1,10})/answers',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async (req, res, next) => {
    try {
      const rv = await exerciseService.updateStudentAnswers(
        parseInt(req.params.id_exercise),
        req.session.studentId,
        req.body.answers
      );
      res.json(rv);
    } catch (error) {
      winston.error(
        `Error when saving answers for exercise ${req.params.id_exercise} ` + JSON.stringify(error)
      );
      res.json({
        success: false,
      });
    }
  }
);

router.put(
  '/api/:id_exercise([0-9]{1,10})/difficultyLevel',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async (req, res, next) => {
    try {
      const rv = await exerciseService.updateCurrentDifficultyLevel(
        parseInt(req.params.id_exercise),
        req.session.studentId,
        req.body.difficultyLevelId
      );
      res.json({
        success: true,
      });
    } catch (error) {
      winston.error(
        `Error when updating difficulty level for exercise ${req.params.id_exercise} ` +
          JSON.stringify(error)
      );
      res.json({
        success: false,
      });
    }
  }
);

router.post(
  '/api/:id_exercise([0-9]{1,10})/question/:question_ordinal([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.STUDENT, globals.ROLES.TEACHER]),
  async (req, res, next) => {
    // get question id
    const questionId = parseInt(
      (
        await db.one(
          `SELECT id_question
            FROM exercise_student_question
            WHERE id_exercise = $(id_exercise)
                AND id_student = $(id_student)
                AND ordinal = $(ordinal)`,
          {
            id_exercise: req.session.currExerciseId,
            id_student: req.session.studentId,
            ordinal: parseInt(req.params.question_ordinal),
          }
        )
      ).id_question
    );

    // check if answer correct
    try {
      const result = await exerciseService.evaluateQuestionAnswer(
        parseInt(req.params.id_exercise),
        req.session.studentId,
        questionId,
        req.body.answer,
        parseInt(req.params.question_ordinal)
      );

      res.json({
        correct: result.score.is_correct,
      });
    } catch (err) {
      res.json({
        success: false,
        error: {
          message: JSON.stringify(err.message),
        },
      });
    }
  }
);

router.post(
  '/question/exec/:question_ordinal([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.STUDENT, globals.ROLES.TEACHER]),
  async (req, res, next) => {
    // get question id
    const questionId = parseInt(
      (
        await db.one(
          `SELECT id_question
            FROM exercise_student_question
            WHERE id_exercise = $(id_exercise)
                AND id_student = $(id_student)
                AND ordinal = $(ordinal)`,
          {
            id_exercise: req.session.currExerciseId,
            id_student: req.session.studentId,
            ordinal: parseInt(req.params.question_ordinal),
          }
        )
      ).id_question
    );

    // check if answer correct
    try {
      const result = await exerciseService.evaluateQuestionAnswer(
        parseInt(req.session.currCourseId),
        parseInt(req.session.currExerciseId),
        req.session.studentId,
        questionId,
        req.body.code,
        parseInt(req.params.question_ordinal)
      );

      res.json(result);
    } catch (err) {
      winston.error(
        `Failed to evalute question (id = ${questionId}) at exercise (id = ${req.session.currExerciseId}) ` +
          JSON.stringify(err)
      );
      res.json({
        success: false,
        error: {
          message: `An error occured while evaluating question ${req.params.question_ordinal} (more info @server).`,
        },
      });
    }
  }
);

router.post(
  '/api/:id_exercise([0-9]{1,10})/finished',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async (req, res, next) => {
    try {
      const rv = await exerciseService.markAsFinished(
        parseInt(req.params.id_exercise),
        req.session.studentId
      );
      res.json({
        success: true,
      });
    } catch (error) {
      winston.error(
        `Error when marking exercise ${req.params.id_exercise} as finished ` + JSON.stringify(error)
      );
      res.json({
        success: false,
      });
    }
  }
);

router.post(
  '/log',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  function (req, res) {
    let event = req.body.e;
    event.ip = req.ip;
    if (req.session.currExerciseId) {
      exerciseService.logEvent(parseInt(req.session.currExerciseId), req.session.studentId, event);

      res.json({
        done: true,
      });
    } else {
      res.json({
        done: true,
        message: 'Unknown exercise id',
      });
    }
  }
);

module.exports = router;
