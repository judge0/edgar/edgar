var express = require('express');
var router = express.Router();
var winston = require('winston');
var errors = require('../common/errors');
var config = require.main.require('./config/config');
var crypto = require('crypto');
var passport = require('passport');
const db = require('../db').db;

router.route('/token/:token').get(function (req, res, next) {
  var decipher = crypto.createDecipher('aes-128-ctr', config.register.hashEncryptionSecret);
  var messageString = decipher.update(req.params.token, 'hex', 'utf8');
  messageString += decipher.final('utf8');
  var message = JSON.parse(messageString);

  if (Date.now() - message.iat > config.register.tokenExpiry) {
    // dva tjedna
    res.errRender(
      'Your register token expired. ' +
        'Contact your teacher or admin so they can send you a new one.'
    );
  }

  db.any(`SELECT * FROM student WHERE id = $(id)`, { id: message.id })
    .then((data) => {
      if (data.length > 0) {
        req.session.registerStudentID = message.id;
        res.publicRender('registerView', { providers: config.providers });
      } else {
        res.errRender('We have no record of you, where did you get your token?');
      }
    })
    .catch(function (error) {
      winston.error('Error finding user that wants to register. ' + error);
      return next(error);
    });
});

module.exports = router;
