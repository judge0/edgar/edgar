'use strict';

var express = require('express');
var router = express.Router();
var winston = require('winston');
var crypto = require('crypto');
var middleware = require('../common/middleware');
var globals = require('../common/globals');
var AppUserSettings = require.main.require('./models/AppUserSettingsModel.js');
//router.post('/:id_question([0-9]{1,10})', [middleware.requireRole(globals.ROLES.TEACHER), upload], function(req, res, next) {
//middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
router.get(
  '/cmskin',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  function (req, res, next) {
    if (req.session.showCodeMirror) {
      res.appRender('cmSkinView', {}, 'cmSkinViewScript', '~/shared/cmSkinViewCSS');
    } else {
      res.status(404).send('Course does not use code mirror.');
    }
  }
);

router.post(
  '/cmskin',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  function (req, res, next) {
    if (req.session.showCodeMirror) {
      req.session.cmSkin = req.body.cmSkin;
      AppUserSettings.update(
        {
          username: req.session.passport.user,
        },
        {
          $set: {
            cmSkin: req.body.cmSkin,
          },
        },
        {
          upsert: true,
        },
        function (err, count) {
          if (err) {
            req.flash('error', JSON.stringify(err));
          } else {
            req.flash('info', `Skin ${req.session.cmSkin} set.`);
          }
          res.appRender('cmSkinView', {}, 'cmSkinViewScript', '~/shared/cmSkinViewCSS');
        }
      );
    } else {
      res.status(404).send('Course does not use code mirror.');
    }
  }
);

router.post(
  '/save/:paramName',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    if (
      req.params.paramName === 'markWhitespace' ||
      req.params.paramName === 'showQuestionHeader'
    ) {
      let paramValue = req.body.value || false;
      req.session.AppUserSettings[req.params.paramName] = paramValue;
      let mongoObj = {};
      mongoObj[req.params.paramName] = paramValue;
      AppUserSettings.update(
        {
          username: req.session.passport.user,
        },
        {
          $set: mongoObj,
        },
        {
          upsert: true,
        },
        function (err, count) {
          if (err) {
            winston.error(err);
            res.json({
              success: false,
              error: {
                message: 'An error occured: ' + JSON.stringify(err) + '.',
              },
            });
          } else {
            res.json({
              success: true,
            });
          }
        }
      );
    }
  }
);
module.exports = router;
