'use strict';

var express = require('express');
var router = express.Router();
var winston = require('winston');
var middleware = require('../common/middleware');
var globals = require('../common/globals');

router.get('/glossary', middleware.requireRoles([globals.ROLES.TEACHER]), function (req, res) {
  res.appRender('helpGlossaryView');
});
router.get('/ticketing', middleware.requireRoles([globals.ROLES.TEACHER]), function (req, res) {
  res.appRender('helpTicketingView');
});
router.get('/manualgrading', middleware.requireRoles([globals.ROLES.TEACHER]), function (req, res) {
  res.appRender('helpManualGradingView');
});

router.get('/examform', middleware.requireRoles([globals.ROLES.TEACHER]), function (req, res) {
  res.appRender('helpExamFormView');
});
router.get('/tutorials', middleware.requireRoles([globals.ROLES.TEACHER]), function (req, res) {
  res.appRender('helpTutorialsView');
});
router.get('/faq', middleware.requireRoles([globals.ROLES.TEACHER]), function (req, res) {
  res.appRender('helpFAQView');
});
router.get('/pa', middleware.requireRoles([globals.ROLES.TEACHER]), function (req, res) {
  res.appRender('helpPeerAssessmentView');
});
router.get('/questionops', middleware.requireRoles([globals.ROLES.TEACHER]), function (req, res) {
  res.appRender('helpQuestionOpsView');
});
router.get('/questionedit', middleware.requireRoles([globals.ROLES.TEACHER]), function (req, res) {
  res.appRender('helpQuestionEditView');
});
router.get(
  '/questiontemplate',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res) {
    res.appRender('helpQuestionTemplateView');
  }
);
router.get(
  '/questiongraderobject',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res) {
    res.appRender('helpQuestionGraderObjectView');
  }
);
router.get(
  '/questionscript',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res) {
    res.appRender('helpQuestionScriptView');
  }
);

module.exports = router;
