var fs = require('fs');
var unzip = require('unzipper');
const globals = require('globals');

const unzipPromise = (from, to) => {
  return new Promise((resolve, reject) => {
    fs.createReadStream(from)
      .pipe(
        unzip.Extract({
          path: to,
        })
      )
      .on('finish', function () {
        resolve();
      })
      .on('error', function (e) {
        reject(e);
      });
  });
};
router.get('/fromzip/:filename', async function (req, res) {
  try {
    const folder = `${global.appRoot}/${globals.PUBLIC_FOLDER}/${globals.PUBLIC_TI_DOWNLOAD_FOLDER}}`;
    const zipname = req.query.zip;
    if (!fs.existsSync(`${global.appRoot}/${filename}`)) {
      await unzipPromise(`${folder}/${zipname}`, folder);
    }
    if (!fs.existsSync(`${global.appRoot}/${filename}`)) {
      res.status(404);
    } else {
      path = zipname.split('/');
      path.pop();
      res.redirect(`/${path.join('/')}/${filename}`);
    }
  } catch (error) {
    winston.error('Error preparing download.');
    winston.error(error);
  }
});
