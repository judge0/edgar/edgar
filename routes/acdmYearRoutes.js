var express = require('express');
var router = express.Router();
var errors = require('../common/errors');
const mongoose = require('mongoose');
var mongoDb = mongoose.connection;
var AppUserSettings = require.main.require('./models/AppUserSettingsModel.js');
var winston = require('winston');

router.get('/:id(\\d+)/', function (req, res, next) {
  req.session.currAcademicYearId = parseInt(req.params.id);
  AppUserSettings.findOneAndUpdate(
    {
      username: req.user,
    },
    {
      currAcademicYearId: req.session.currAcademicYearId,
    },
    function (err) {
      if (err) {
        winston.error(err);
      } else {
        winston.debug(`AppUserSettings for ${req.user} saved.`);
      }
    }
  );
  res.redirect('/');
});

module.exports = router;
