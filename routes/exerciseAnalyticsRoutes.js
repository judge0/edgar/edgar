'use strict';
const express = require('express');
const router = express.Router();
const middleware = require('../common/middleware');
const globals = require('../common/globals');
const utils = require('../common/utils');
const db = require('../db').db;
const winston = require('winston');

const exerciseAnalyticsService = require('../services/exerciseAnalyticsService');

router.get(
  '/:id_exercise([0-9]{1,10})/groups',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res, next) => {
    const data = await exerciseAnalyticsService.byGroups(
      parseInt(req.params.id_exercise),
      req.session.currCourseId,
      req.session.currAcademicYearId
    );

    res.appRender(
      'groupsView',
      {
        analytics: {
          headers: [
            {
              class_group: 'Class group',
            },
            {
              started: 'Students started',
            },
            {
              finished: 'Students finished',
            },
            ...data.questionIds.map((qId) => ({
              [qId.id_question]: qId.id_question,
            })),
          ],
          rows: data.data,
        },
      },
      'groupsViewScript',
      'groupsViewCSS'
    );
  }
);

router.get(
  '/:id_exercise([0-9]{1,10})/students',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res, next) => {
    const data = await exerciseAnalyticsService.byStudents(
      parseInt(req.params.id_exercise),
      req.session.currCourseId,
      req.session.currAcademicYearId
    );

    res.appRender('studentsView', {
      analytics: {
        headers: [
          {
            image: 'Image',
            __raw: true,
            __formatter: function (row) {
              return utils.getImageHtml(row.alt_id2);
            },
          },
          {
            student: 'Student',
            __raw: true,
          },
          {
            class_group: 'Class group',
          },
          {
            no_questions: 'Questions',
          },
          {
            is_finished: 'Finished',
          },
          {
            ts_finished: 'Finished at',
          },
          ...data.questionIds.map((qId) => ({
            [qId.id_question]: qId.id_question,
          })),
        ],
        rows: data.data,
        links: [
          {
            label: 'Log',
            href: (row) => `student/${row.id_student}/log`,
          },
        ],
      },
    });
  }
);

router.get(
  '/:id_exercise([0-9]{1,10})/student/:id_student([0-9]{1,10})/log',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res, next) => {
    const data = await exerciseAnalyticsService.studentLog(
      parseInt(req.params.id_exercise),
      parseInt(req.params.id_student)
    );

    res.appRender('studentLogView', {
      events: data.events,
    });
  }
);

module.exports = router;
