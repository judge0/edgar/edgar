'use strict';
const express = require('express');
const router = express.Router();
const errors = require('../common/errors');
const middleware = require('../common/middleware');
const utils = require('../common/utils');
const globals = require('../common/globals');
const db = require('../db').db;
const winston = require('winston');
const randToken = require('rand-token');

const mongoose = require('mongoose');
const mongoDb = mongoose.connection;
const moment = require('moment');

const pinGenerator = randToken.generator({
  chars: 'numeric',
});

const PIN_VALIDATION_DURATION = {
  hours: 1,
};

router.get(
  '/view',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  (req, res, next) => {
    if (req.session.usesMobileApp) {
      const studentID = req.session.studentId;
      db.any(
        `
                SELECT pin
                FROM student_token
                WHERE student_id = $(studentID)
                AND ts_created = (
                  SELECT max(ts_created)
                  FROM student_token
                  WHERE student_id = $(studentID)
                )
            `,
        { studentID }
      ).then((response) => {
        if (response.length) {
          res.appRender('tokenView', {
            pin: response[0].pin,
          });
        } else {
          res.appRender('tokenView');
        }
      });
    } else {
      res.status(404).send('Course does not use mobile app.');
    }
  }
);

router.post(
  '/generate',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  (req, res, next) => {
    if (req.session.usesMobileApp) {
      const studentID = req.session.studentId;

      // Generate new PIN and token
      const token = randToken.uid(50);
      const pin = pinGenerator.generate(6);

      // Deactivate all old PINs
      db.any(
        `
                UPDATE student_token
                SET is_activated = FALSE
                WHERE student_id = $1
            `,
        [studentID]
      )
        .then(() => {
          const validEndDate = moment().add(PIN_VALIDATION_DURATION).toISOString();

          // Add new pin and token
          return db.any(
            `
                    INSERT INTO student_token (student_id, ts_created, ts_valid_until, token, pin)
                    VALUES ($(studentID), current_timestamp, $(validEndDate), $(token), $(pin))
                `,
            { studentID, validEndDate, token, pin }
          );
        })
        .then(() => {
          res.status(201).redirect('/token/view');
        })
        .catch((error) => {
          winston.error(
            new errors.DbError('An error occured while generating pin and token: ' + error.message)
          );
          res.status(500).send();
        });
    } else {
      res.status(404).send('Course does not use mobile app.');
    }
  }
);

module.exports = router;
