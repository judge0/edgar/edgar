'use strict';
const db = require('../db').db;
const Scorer = require('../lib/Scorer');
var utils = require('../common/utils');

async function evaluateQuestion(
  courseId,
  questionId,
  answer,
  id_programming_language /*, abcEvalFunc, sqlEvalFunc, cLangEvalFunc*/
) {
  const question = await db.one(
    `SELECT type_name
        FROM question JOIN question_type
            ON question.id_question_type = question_type.id
            WHERE question.id = $(id_question)`,
    {
      id_question: questionId,
    }
  );

  let result;

  if (question.type_name.toUpperCase().includes('ABC')) {
    throw new Error('Not impl');
    result = await abcEvalFunc(questionId, answer);
  } else if (question.type_name.toUpperCase().includes('SQL')) {
    result = await evaluateSqlQuestion(courseId, questionId, answer);
  } else if (
    question.type_name.toUpperCase().includes('C-LANG') ||
    question.type_name.toUpperCase().includes('JAVA') ||
    question.type_name.toUpperCase().includes('CODE')
  ) {
    result = await evaluateCLangQuestion(courseId, questionId, answer, id_programming_language);
  } else {
    throw new Error('Unknown question type');
  }

  return {
    ...result,
    question_type: question.type_name,
  };
}

async function evlauateTutorialAbcQuestion(
  courseId,
  tutorialId,
  studentId,
  questionId,
  answerOrdinal
) {
  const data = await db.task((t) =>
    t.batch([
      db.one(
        `SELECT *
              FROM question_answer
              WHERE id_question = $(id_question)
                  AND is_correct = true`,
        {
          id_question: questionId,
        }
      ),
      db.one(
        `SELECT *
              FROM tutorial_student_question
              WHERE id_question = $(id_question)
                AND id_course = $(id_course)
                AND id_tutorial = $(id_tutorial)
                AND id_student = $(id_student)`,
        {
          id_course: courseId,
          id_tutorial: tutorialId,
          id_student: studentId,
          id_question: questionId,
        }
      ),
    ])
  );
  const correctAnswer = data[0],
    studentAnswer = data[1];
  let correctStudentOrdinal;
  studentAnswer.answers_permutation.forEach((ordinal, index) => {
    if (ordinal === correctAnswer.ordinal) {
      correctStudentOrdinal = index + 1;
    }
  });

  if (correctStudentOrdinal == answerOrdinal) {
    return {
      success: true,
      score: {
        is_correct: true,
      },
    };
  } else {
    return {
      success: true,
      score: {
        is_correct: false,
      },
    };
  }
}

async function evaluateExerciseAbcQuestion(exerciseId, studentId, questionId, answerOrdinal) {
  const data = await db.task((t) =>
    t.batch([
      db.one(
        `SELECT *
              FROM question_answer
              WHERE id_question = $(id_question)
                  AND is_correct = true`,
        {
          id_question: questionId,
        }
      ),
      db.one(
        `SELECT *
              FROM exercise_student_question
              WHERE id_question = $(id_question)
                AND id_exercise = $(id_exercise)
                AND id_student = $(id_student)`,
        {
          id_exercise: exerciseId,
          id_student: studentId,
          id_question: questionId,
        }
      ),
    ])
  );
  const correctAnswer = data[0],
    studentAnswer = data[1];
  let correctStudentOrdinal;
  studentAnswer.answers_permutation.forEach((ordinal, index) => {
    if (ordinal === correctAnswer.ordinal) {
      correctStudentOrdinal = index + 1;
    }
  });

  if (correctStudentOrdinal == answerOrdinal) {
    return {
      success: true,
      score: {
        is_correct: true,
      },
    };
  } else {
    return {
      success: true,
      score: {
        is_correct: false,
      },
    };
  }
}

async function evaluateSqlQuestion(courseId, questionId, userSql) {
  //console.log('evaluateSqlQuestion', courseId, questionId, userSql);
  let rv = await Scorer.scoreQuestionTutorial(courseId, questionId, userSql);
  delete rv.record;
  //console.log('evaluateSqlQuestion', rv);
  return rv;
  // return await lib.checkSingleSQLQuestionSequentiallyQ(
  //     questionId, userSql, db, testService
  // );
}

async function evaluateCLangQuestion(courseId, questionId, userCode, id_programming_language) {
  //console.log('evaluateCLangQuestion', courseId, questionId, userCode, id_programming_language);
  let rv = await Scorer.scoreQuestionTutorial(
    courseId,
    questionId,
    userCode,
    id_programming_language
  );
  let j0 = rv.j0;

  let formattedHtmlResult = utils.judge0response2Html({
    resp: j0,
    score: rv.score,
    hideTestDetails: false,
    verboseTests: true,
  });
  return {
    success: true,
    coderesult: formattedHtmlResult,
    score: rv.score,
  };
}

module.exports = {
  evaluateQuestion,
  evlauateTutorialAbcQuestion,
  evaluateExerciseAbcQuestion,
  evaluateSqlQuestion,
  evaluateCLangQuestion,
};
