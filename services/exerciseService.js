'use strict';
const db = require('../db').db;
const mongoose = require('mongoose');
const mongoDb = mongoose.connection;
const ExerciseProgress = require.main.require('./models/ExerciseProgressModel');
const questionEvalService = require('../services/questionEvalService');
const utils = require('../common/utils');

async function listExercises(courseId, academicYearId, studentId, onlyActive = true) {
  const activeFilter = onlyActive ? 'AND exercise.is_active = true' : '';
  return await db.any(
    `SELECT exercise.id, title, description, is_active,
                exercise_student.ts_modified :: timestamp(0) :: varchar AS last_activity,
                COUNT(v_exercise_question) AS no_questions
            FROM exercise LEFT JOIN v_exercise_question 
                ON exercise.id = v_exercise_question.id_exercise
            LEFT JOIN exercise_student
                ON exercise.id = exercise_student.id_exercise
                AND exercise_student.id_student = $(id_student)
            WHERE exercise.id_course = $(id_course)
                AND id_academic_year = $(id_academic_year)
                ${activeFilter}
            GROUP BY exercise.id, title, description, 
                exercise_student.ts_modified
            ORDER BY title`,
    {
      id_course: courseId,
      id_academic_year: academicYearId,
      id_student: studentId,
    }
  );
}

async function start(exerciseId, studentId) {
  const trackingId = parseInt(
    (await db.func('start_exercise', [exerciseId, studentId]))[0].start_exercise
  );

  const status = await ExerciseProgress.getStatus(exerciseId, studentId);

  if (status) {
    return status;
  } else {
    return {
      latestQuestionOrdinal: 1,
    };
  }
}

async function getExercise(exerciseId) {
  const exercise = await db.one(
    `SELECT id, title, description, id_adaptivity_model
        FROM exercise
        WHERE id = $(id_exercise)`,
    {
      id_exercise: exerciseId,
    }
  );

  const data = await db.task((t) =>
    t.batch([
      db.many(
        `SELECT *
            FROM adaptivity_model_behaviour
            WHERE id_adaptivity_model = $(id_adaptivity_model)`,
        {
          id_adaptivity_model: exercise.id_adaptivity_model,
        }
      ),
      db.many(
        `SELECT id, name, ordinal, rgb_color
            FROM question_difficulty_level
            WHERE id_adaptivity_model = $(id_adaptivity_model)`,
        {
          id_adaptivity_model: exercise.id_adaptivity_model,
        }
      ),
    ])
  );

  return {
    exercise: exercise,
    adaptivityModelBehaviour: data[0],
    difficultyLevels: data[1],
  };
}

async function addNewQuestion(exerciseId, studentId, difficultyLevelId) {
  return parseInt(
    (
      await db.func('exercise_student_assign_new_question', [
        exerciseId,
        studentId,
        difficultyLevelId,
      ])
    )[0].exercise_student_assign_new_question
  );
}

async function getQuestionById(exerciseId, studentId, questionId) {
  let question = await db.one(
    `SELECT question.id, ordinal, has_answers, type_name as type, question_text as text, 
            id_question_type, answers_permutation, id_difficulty_level
        FROM question JOIN question_type
            ON question.id_question_type = question_type.id
        JOIN exercise_student_question esq
            ON question.id = esq.id_question
            AND esq.id_exercise = $(id_exercise)
            AND esq.id_student = $(id_student)
        JOIN question_assigned_difficulty qad
            ON question.id = qad.id_question
        JOIN exercise
            ON qad.id_adaptivity_model = exercise.id_adaptivity_model
            AND exercise.id = $(id_exercise)
        WHERE question.id = $(id_question)`,
    {
      id_exercise: exerciseId,
      id_student: studentId,
      id_question: questionId,
    }
  );

  question.html = await utils.md2Html(question.text);

  const data = await db.task((t) =>
    t.batch([
      question.has_answers &&
        db.any(
          `SELECT id, answer_text, ordinal
                                FROM question_answer
                            WHERE id_question = $(id_question)
                            ORDER BY ordinal`,
          {
            id_question: question.id,
          }
        ),
      question.type.toUpperCase().indexOf('SQL') >= 0 &&
        db.any(
          `SELECT sql_answer, mode_desc, sql_alt_assertion, sql_test_fixture, 
                        sql_alt_presentation_query, check_tuple_order, id_check_column_mode
                                FROM sql_question_answer
                                JOIN check_column_mode
                                    ON sql_question_answer.id_check_column_mode = check_column_mode.id
                            WHERE id_question = $(id_question)`,
          {
            id_question: question.id,
          }
        ),
      (question.type.toUpperCase().indexOf('C-LANG') >= 0 ||
        question.type.toUpperCase().indexOf('JAVA') >= 0) &&
        db.any(
          `SELECT c_prefix, c_suffix, c_source
                            FROM c_question_answer
                            -- LEFT JOIN c_question_test
                            --   ON c_question_answer.id = c_question_test.c_question_answer_id
                            WHERE id_question = $(id_question)`,
          {
            id_question: question.id,
          }
        ),
    ])
  );

  let answers = data[0],
    htmlAnswersPerm = [];

  if (question.has_answers) {
    for (let i = 0; question.answers_permutation && i < question.answers_permutation.length; ++i) {
      let permIdx = question.answers_permutation[i] - 1;
      htmlAnswersPerm.push({
        aHtml: await utils.md2Html(answers[permIdx].answer_text),
        ordinal: i + 1,
      });
    }
  }

  question.answers = htmlAnswersPerm;

  delete question.id;
  delete question.id_question_type;
  delete question.answers_permutation;
  delete question.text;

  return question;
}

async function getQuestion(exerciseId, studentId, questionOrdinal, difficultyLevelId) {
  let questionId;
  let trial = await db.any(
    `SELECT id_question
        FROM exercise_student_question
        WHERE id_exercise = $(id_exercise)
            AND id_student = $(id_student)
            AND ordinal = $(ordinal)`,
    {
      id_exercise: exerciseId,
      id_student: studentId,
      ordinal: questionOrdinal,
    }
  );

  if (trial.length && trial[0].id_question) {
    questionId = parseInt(trial[0].id_question);
  } else {
    questionId = await addNewQuestion(exerciseId, studentId, difficultyLevelId);
    await progressToNextQuestion(exerciseId, studentId, questionOrdinal - 1);
  }

  if (questionId == -1) {
    return {
      noMoreQuestions: true,
    };
  } else {
    const question = await getQuestionById(exerciseId, studentId, questionId);
    return {
      ...question,
      ordinal: questionOrdinal,
    };
  }
}

async function updateStudentAnswers(exerciseId, studentId, answers) {
  return await ExerciseProgress.updateExerciseAnswers(exerciseId, studentId, answers);
}

async function updateCurrentDifficultyLevel(exerciseId, studentId, difficultyLevelId) {
  return await ExerciseProgress.updateDifficultyLevel(exerciseId, studentId, difficultyLevelId);
}

async function progressToNextQuestion(exerciseId, studentId, currQuestionOrdinal) {
  return await ExerciseProgress.updateLatestQuestionOrdinal(
    exerciseId,
    studentId,
    currQuestionOrdinal + 1
  );
}

async function evaluateQuestionAnswer(
  courseId,
  exerciseId,
  studentId,
  questionId,
  answer,
  currQuestionOrdinal
) {
  const abcEvalFunc = async function (questionId, answerOrdinal) {
    return await questionEvalService.evaluateExerciseAbcQuestion(
      exerciseId,
      studentId,
      questionId,
      answerOrdinal
    );
  };
  const sqlEvalFunc = questionEvalService.evaluateSqlQuestion;
  const cLangEvalFunc = questionEvalService.evaluateCLangQuestion; // to be implemented

  const result = await questionEvalService.evaluateQuestion(
    courseId,
    questionId,
    answer,
    abcEvalFunc,
    sqlEvalFunc,
    cLangEvalFunc
  );

  const event = {
    clientTs: new Date(),
    eventName: 'Answer evaluation',
    eventData: `question ordinal: ${currQuestionOrdinal}, correct: ${result.score.is_correct}`,
  };

  ExerciseProgress.logEvent(exerciseId, studentId, event);

  if (result.score.is_correct) {
    progressToNextQuestion(exerciseId, studentId, currQuestionOrdinal);
  }

  let answers, codeAnswer;

  if (result.question_type.toUpperCase().includes('ABC')) {
    // ABC question
    answers = answer || [];
  } else if (
    result.question_type.toUpperCase().includes('SQL') ||
    result.question_type.toUpperCase().includes('C-LANG')
  ) {
    // SQL question
    codeAnswer = answer || '';
  }

  const predicate = {
    id_exercise: exerciseId,
    id_student: studentId,
    id_question: questionId,
  };

  await db.task((t) =>
    t.batch([
      db.none(
        `UPDATE exercise_student_question
                SET ${answers ? 'student_answers = $(answers),' : ''}
                    ${codeAnswer ? 'student_answer_code = $(code_answer),' : ''}
                    is_correct = $(is_correct)
                WHERE id_exercise = $(id_exercise)
                    AND id_student = $(id_student)
                    AND id_question = $(id_question)`,
        {
          ...predicate,
          answers: answers,
          code_answer: codeAnswer,
          is_correct: result.score.is_correct,
        }
      ),
      db.none(
        `INSERT INTO exercise_student_question_attempt (
                    id_exercise, id_student, id_question, is_correct)
                    VALUES ($(id_exercise), $(id_student), $(id_question), $(is_correct))`,
        {
          ...predicate,
          is_correct: result.score.is_correct,
        }
      ),
    ])
  );

  if (result.score.is_correct) {
  } else {
  }

  return result;
}

async function markAsFinished(exerciseId, studentId) {
  const ts = new Date();
  await Promise.all([
    ExerciseProgress.markAsFinished(exerciseId, studentId, ts),
    db.none(
      `UPDATE exercise_student
            SET is_finished = TRUE,
            ts_finished = $(ts)
            WHERE id_exercise = $(id_exercise)
                AND id_student = $(id_student)`,
      {
        id_exercise: exerciseId,
        id_student: studentId,
        ts: ts,
      }
    ),
  ]);
}

async function logEvent(exerciseId, studentId, event) {
  return await ExerciseProgress.logEvent(exerciseId, studentId, event);
}

module.exports = {
  listExercises: listExercises,
  start: start,
  getExercise: getExercise,
  getQuestion: getQuestion,
  getQuestionById: getQuestionById,
  updateStudentAnswers: updateStudentAnswers,
  updateCurrentDifficultyLevel: updateCurrentDifficultyLevel,
  evaluateQuestionAnswer: evaluateQuestionAnswer,
  markAsFinished: markAsFinished,
  logEvent: logEvent,
};
