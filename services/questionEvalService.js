'use strict';
const db = require('../db').db;
const Scorer = require('../lib/Scorer');

async function evaluateQuestion(
  courseId,
  questionId,
  answer,
  abcEvalFunc,
  sqlEvalFunc,
  cLangEvalFunc
) {
  const question = await db.one(
    `SELECT type_name
        FROM question JOIN question_type
            ON question.id_question_type = question_type.id
            WHERE question.id = $(id_question)`,
    {
      id_question: questionId,
    }
  );

  let result;

  if (question.type_name.toUpperCase().includes('ABC')) {
    result = await abcEvalFunc(questionId, answer);
  } else if (question.type_name.toUpperCase().includes('SQL')) {
    result = await sqlEvalFunc(courseId, questionId, answer);
    if (result.score.is_correct) {
    } else {
    }
  } else if (question.type_name.toUpperCase().includes('C-LANG')) {
    // to be implemented
    result = await cLangEvalFunc(questionId, answer);
  } else {
    throw new Error('Unknown question type');
  }

  return {
    ...result,
    question_type: question.type_name,
  };
}

async function evlauateTutorialAbcQuestion(
  courseId,
  tutorialId,
  studentId,
  questionId,
  answerOrdinal
) {
  const data = await db.task((t) =>
    t.batch([
      db.one(
        `SELECT *
              FROM question_answer
              WHERE id_question = $(id_question)
                  AND is_correct = true`,
        {
          id_question: questionId,
        }
      ),
      db.one(
        `SELECT *
              FROM tutorial_student_question
              WHERE id_question = $(id_question)
                AND id_course = $(id_course)
                AND id_tutorial = $(id_tutorial)
                AND id_student = $(id_student)`,
        {
          id_course: courseId,
          id_tutorial: tutorialId,
          id_student: studentId,
          id_question: questionId,
        }
      ),
    ])
  );
  const correctAnswer = data[0],
    studentAnswer = data[1];
  let correctStudentOrdinal;
  studentAnswer.answers_permutation.forEach((ordinal, index) => {
    if (ordinal === correctAnswer.ordinal) {
      correctStudentOrdinal = index + 1;
    }
  });

  if (correctStudentOrdinal == answerOrdinal) {
    return {
      success: true,
      score: {
        is_correct: true,
      },
    };
  } else {
    return {
      success: true,
      score: {
        is_correct: false,
      },
    };
  }
}

async function evaluateExerciseAbcQuestion(exerciseId, studentId, questionId, answerOrdinal) {
  const data = await db.task((t) =>
    t.batch([
      db.one(
        `SELECT *
              FROM question_answer
              WHERE id_question = $(id_question)
                  AND is_correct = true`,
        {
          id_question: questionId,
        }
      ),
      db.one(
        `SELECT *
              FROM exercise_student_question
              WHERE id_question = $(id_question)
                AND id_exercise = $(id_exercise)
                AND id_student = $(id_student)`,
        {
          id_exercise: exerciseId,
          id_student: studentId,
          id_question: questionId,
        }
      ),
    ])
  );
  const correctAnswer = data[0],
    studentAnswer = data[1];
  let correctStudentOrdinal;
  studentAnswer.answers_permutation.forEach((ordinal, index) => {
    if (ordinal === correctAnswer.ordinal) {
      correctStudentOrdinal = index + 1;
    }
  });

  if (correctStudentOrdinal == answerOrdinal) {
    return {
      success: true,
      score: {
        is_correct: true,
      },
    };
  } else {
    return {
      success: true,
      score: {
        is_correct: false,
      },
    };
  }
}

async function evaluateSqlQuestion(courseId, questionId, userSql) {
  let rv = await Scorer.scoreQuestionTutorial(courseId, questionId, userSql);
  delete rv.record;
  return rv;
  // return await lib.checkSingleSQLQuestionSequentiallyQ(
  //     questionId, userSql, db, testService
  // );
}

async function evaluateCLangQuestion(questionId, userCode) {}

module.exports = {
  evaluateQuestion: evaluateQuestion,
  evlauateTutorialAbcQuestion: evlauateTutorialAbcQuestion,
  evaluateExerciseAbcQuestion: evaluateExerciseAbcQuestion,
  evaluateSqlQuestion: evaluateSqlQuestion,
  evaluateCLangQuestion: evaluateCLangQuestion,
};
