'use strict';
var db = require('../db').db;
// const globals = require('../common/globals');

async function getPaAttachment(id_test_instance) {
  return await db.oneOrNone(
    `
          SELECT -- test_instance.id, test_instance_question.id, jobs[test.test_ordinal - master.test_ordinal] as job_id,
                    other_student.uploaded_files,
                    question_attachment.filename
            FROM test_instance
            JOIN peer_Test
                ON test_instance.id_test = id_test_phase_1
            JOIN test
                ON id_test_phase_2 = test.id_parent
            JOIN test master
                ON test.id_parent = master.id
            JOIN test_instance curr
                ON curr.id = $(id_test_instance)
                AND test.id = curr.id_test
                AND test_instance.id_student = curr.id_student
            JOIN test_instance_question
                ON test_instance_question.id_test_instance = test_instance.id
                and test_instance_question.ordinal = 1
            JOIN peer_shuffle
                ON peer_shuffle.id_test_instance_question = test_instance_question.id
            LEFT JOIN test_instance_question other_student
                ON other_student.id = jobs[test.test_ordinal - master.test_ordinal]
            LEFT JOIN question calib_question
                ON calib_question.id = -1 * jobs[test.test_ordinal - master.test_ordinal]
            LEFT JOIN question_attachment
                ON calib_question.id = question_attachment.id_question`,
    {
      id_test_instance,
    }
  );
}

module.exports = {
  getPaAttachment,
};
