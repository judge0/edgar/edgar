'use strict';
let db = require('../db').db;
let winston = require('winston');
var moment = require('moment');
let CodeRunLog = require.main.require('./models/CodeRunLogModel.js');
let plagiarismDetectionService = require('./plagiarismDetectionService');

async function updateSimilarity(studentId, examId, questionId, code, id_plag_method = 1) {
  let cleanCode = code.replace(/\s+/g, '');
  if (cleanCode.length) {
    cleanCode = code.replace(/\s+/g, ',');
    let row = await db.any(`
        SELECT 
            similarities,
            data, 
            colors
        FROM real_time_plag_detection_data
        WHERE id_test = ${examId} and 
        id_question = ${questionId} and 
        id_plag_detection_algorithm = ${id_plag_method};`);

    let similarities = {};
    let data = {};
    let colors = {};

    if (row.length > 0) {
      row = row[0];
      similarities = JSON.parse(row['similarities']);
      data = JSON.parse(row['data']);
      colors = JSON.parse(row['colors']);
    }

    let similarityChanged = false;
    if (!studentId || !examId) {
      winston.error(
        'Warning: updateSimilarity: unknown session user or id_test_instance for log entry:',
        JSON.stringify({
          studentId,
          examId,
          questionId,
          code,
          id_plag_method,
        })
      );
      resolve(); // don't want logging to crash anything
    } else {
      let codeRuns = await CodeRunLog.getCodeRuns(examId, questionId);
      for (let codeRun of codeRuns) {
        if (studentId != codeRun.id_student) {
          let codeRunCleanCode = codeRun.code.replace(/\s+/g, '');
          if (codeRunCleanCode.length) {
            codeRunCleanCode = codeRun.code.replace(/\s+/g, ',');
            let key =
              studentId < codeRun.id_student
                ? `${studentId}+${codeRun.id_student}`
                : `${codeRun.id_student}+${studentId}`;
            let similarity = plagiarismDetectionService.similarityMethods[id_plag_method](
              cleanCode,
              codeRunCleanCode
            );
            similarity = Math.round((similarity + Number.EPSILON) * 10000) / 100;

            if (!similarities[key] || similarities[key] < similarity) {
              similarities[key] = similarity;
              similarityChanged = true;
            }
          }
        }
      }
      if (similarityChanged) {
        let allStudents = new Set();
        data.minSimilarity = undefined;
        data.maxSimilarity = undefined;
        data.averageSimilarity = 0;
        for (let key of Object.keys(similarities)) {
          allStudents.add(key.split('+')[0]);
          allStudents.add(key.split('+')[1]);
          data.averageSimilarity += similarities[key];
          if (data.minSimilarity == undefined || similarities[key] < data.minSimilarity)
            data.minSimilarity = similarities[key];
          if (data.maxSimilarity == undefined || similarities[key] > data.maxSimilarit)
            data.maxSimilarity = similarities[key];
        }
        data.averageSimilarity =
          Math.round(
            (data.averageSimilarity / Object.keys(similarities).length + Number.EPSILON) * 100
          ) / 100;
        data.studentsTotal = allStudents.size;
        let students = Array.from(allStudents).sort();

        let colors = plagiarismDetectionService.getColorPerQuestion(
          similarities,
          data.averageSimilarity
        );

        if (row.length == 0) {
          db.none(
            `
                    INSERT INTO real_time_plag_detection_data(id_test, id_question, similarities, students, data, colors, id_plag_detection_algorithm)
                    VALUES ( $1, $2, $3, $4, $5, $6, $7 );`,
            [
              examId,
              questionId,
              JSON.stringify(similarities),
              JSON.stringify(students),
              JSON.stringify(data),
              JSON.stringify(colors),
              id_plag_method,
            ]
          );
        } else {
          db.any(
            `
                    UPDATE real_time_plag_detection_data
                    SET (
                        similarities, 
                        students,
                        data,
                        colors
                        ) = ( $3, $4, $5, $6 )
                    WHERE id_test = $1 AND id_question = $2;`,
            [
              examId,
              questionId,
              JSON.stringify(similarities),
              JSON.stringify(students),
              JSON.stringify(data),
              JSON.stringify(colors),
            ]
          );
        }
      }
    }
  }
}

async function getExamTableData(examId, cutoff, id_plag_method) {
  let rows = await db.any(`
        SELECT similarities, students FROM real_time_plag_detection_data
        WHERE id_test = ${examId} and id_plag_detection_algorithm = ${id_plag_method}
        ORDER BY id_question;
    `);

  let examTitle = await db.one(`SELECT title FROM test WHERE id = ${examId}`);
  examTitle = examTitle.title;

  let plagMethod = (
    await db.one(`
        SELECT name FROM plag_detection_algorithm where id = ${id_plag_method};
    `)
  ).name;

  let data = {};
  let allStudents = new Set();
  let suspiciousStudents = new Set();
  let similarities = {};

  if (rows.length == 0) {
    return {
      examTitle: examTitle,
      examId: examId,
      plagMethod: plagMethod,
    };
  }

  for (let row of rows) {
    let similarityPerQuestion = JSON.parse(row['similarities']);
    let students = JSON.parse(row['students']);
    for (let key of Object.keys(similarityPerQuestion)) {
      if (similarities[key] == undefined || similarities[key] < similarityPerQuestion[key])
        similarities[key] = similarityPerQuestion[key];
    }
    for (let student of students) {
      allStudents.add(student);
    }
  }

  data.minSimilarity = undefined;
  data.maxSimilarity = undefined;
  data.averageSimilarity = 0;
  for (let key of Object.keys(similarities)) {
    if (similarities[key] >= cutoff) {
      suspiciousStudents.add(key.split('+')[0]);
      suspiciousStudents.add(key.split('+')[1]);
    }
    data.averageSimilarity += similarities[key];
    if (data.minSimilarity == undefined || similarities[key] < data.minSimilarity)
      data.minSimilarity = similarities[key];
    if (data.maxSimilarity == undefined || similarities[key] > data.maxSimilarity)
      data.maxSimilarity = similarities[key];
  }
  data.averageSimilarity =
    Math.round((data.averageSimilarity / Object.keys(similarities).length + Number.EPSILON) * 100) /
    100;
  data.studentsTotal = allStudents.size;

  // remove all under cutoff
  let suspiciousSimilarities = {};
  for (let key of Object.keys(similarities)) {
    if (suspiciousStudents.has(key.split('+')[0]) || suspiciousStudents.has(key.split('+')[1])) {
      suspiciousSimilarities[key] = similarities[key];
    }
  }

  let colors = plagiarismDetectionService.getColorPerQuestion(
    suspiciousSimilarities,
    data.averageSimilarity
  );

  return {
    examTitle: examTitle,
    data: data,
    students: Array.from(suspiciousStudents).sort(),
    similarities: suspiciousSimilarities,
    colors: colors,
    examId: examId,
    plagMethod: plagMethod,
  };
}

async function getQuestionTableData(examId, questionId, cutoff, id_plag_method) {
  let row = await db.any(`
            SELECT * FROM real_time_plag_detection_data
            WHERE id_test = ${examId}
            AND id_question = ${questionId}
            AND id_plag_detection_algorithm = ${id_plag_method}
        `);

  let examTitle = await db.one(`SELECT title FROM test WHERE id = ${examId}`);
  examTitle = examTitle.title;

  let plagMethod = (
    await db.one(`
        SELECT name FROM plag_detection_algorithm where id = ${id_plag_method};
    `)
  ).name;

  if (row.length == 0) {
    return {
      examTitle: examTitle,
      examId: examId,
      questionId: questionId,
      plagMethod: plagMethod,
    };
  }

  let data = row[0];
  let similarities = JSON.parse(data.similarities);
  let suspiciousStudents = new Set();
  let newSimilarities = {};
  for (let key in similarities) {
    if (similarities[key] >= cutoff) {
      suspiciousStudents.add(key.split('+')[0]);
      suspiciousStudents.add(key.split('+')[1]);
    }
  }

  for (let key in similarities) {
    if (suspiciousStudents.has(key.split('+')[0]) || suspiciousStudents.has(key.split('+')[1])) {
      newSimilarities[key] = similarities[key];
    }
  }
  return {
    examTitle: examTitle,
    examId: examId,
    questionId: questionId,
    similarities: newSimilarities,
    students: Array.from(suspiciousStudents).sort(),
    data: JSON.parse(data.data),
    colors: JSON.parse(data.colors),
    plagMethod: plagMethod,
  };
}

async function getQuestionCompareData(examId, questionId, firstStudentId, secondStudentId) {
  let firstStudent = await db.one(
    `SELECT id, first_name, last_name
        FROM student 
        WHERE id = ${firstStudentId}`
  );
  let secondStudent = await db.one(
    `SELECT id, first_name, last_name
        FROM student 
        WHERE id = ${secondStudentId}`
  );
  let examTitle = await db.one(`SELECT title FROM test WHERE id = ${examId}`);
  examTitle = examTitle.title;

  let codeRuns = await CodeRunLog.getCodeRuns(examId, questionId);
  let runPairs = [];
  let tmpObj = {};
  for (let codeRun of codeRuns) {
    if (codeRun.id_student == firstStudentId || codeRun.id_student == secondStudentId) {
      if (tmpObj[codeRun.id_student] == undefined) {
        tmpObj[codeRun.id_student] = {
          code: codeRun.code,
          timestamp: moment(codeRun.ts).format('HH:mm:ss'),
          ts: codeRun.ts,
        };
      } else {
        // student already added
        let anotherStudentId =
          codeRun.id_student == firstStudentId ? secondStudentId : firstStudentId;
        if (tmpObj[anotherStudentId] == undefined) {
          tmpObj[anotherStudentId] = {
            code: false,
          };
        } else {
          let timestampFirst = moment(tmpObj[codeRun.id_student].ts);
          let timestampSecond = moment(tmpObj[anotherStudentId].ts);

          if (timestampFirst.isAfter(timestampSecond)) {
            tmpObj[codeRun.id_student].timestamp += ` (${Math.round(
              moment.duration(timestampFirst.diff(timestampSecond)).asSeconds()
            )} seconds later)`;
          } else {
            tmpObj[anotherStudentId].timestamp += ` (${Math.round(
              moment.duration(timestampSecond.diff(timestampFirst)).asSeconds()
            )} seconds later)`;
          }
        }

        runPairs.push(tmpObj);
        tmpObj = {};
        tmpObj[codeRun.id_student] = {
          code: codeRun.code,
          timestamp: moment(codeRun.ts).format('HH:mm:ss'),
          ts: codeRun.ts,
        };
      }
    }
  }
  if (tmpObj[firstStudentId] != undefined && tmpObj[secondStudentId] != undefined) {
    runPairs.push(tmpObj);
  } else if (tmpObj[firstStudentId] != undefined && tmpObj[secondStudentId] == undefined) {
    tmpObj[secondStudentId] = {
      code: false,
    };
    runPairs.push(tmpObj);
  } else if (tmpObj[secondStudentId] != undefined && tmpObj[firstStudentId] == undefined) {
    tmpObj[firstStudentId] = {
      code: false,
    };
    runPairs.push(tmpObj);
  }

  return {
    examTitle,
    questionId,
    firstStudent,
    secondStudent,
    runPairs,
  };
}

async function getTestCompareData(examId, firstStudentId, secondStudentId, id_plag_method) {
  let firstStudent = await db.one(
    `SELECT id, first_name, last_name
        FROM student 
        WHERE id = ${firstStudentId}`
  );
  let secondStudent = await db.one(
    `SELECT id, first_name, last_name
        FROM student 
        WHERE id = ${secondStudentId}`
  );
  let examTitle = await db.one(`SELECT title FROM test WHERE id = ${examId}`);
  examTitle = examTitle.title;

  let plagMethod = (
    await db.one(`
        SELECT name FROM plag_detection_algorithm where id = ${id_plag_method};
    `)
  ).name;

  let questions = await db.any(`
        SELECT DISTINCT id_question 
        FROM test_instance_question 
        JOIN test_instance ON test_instance.id = test_instance_question.id_test_instance
        WHERE id_test_instance in (SELECT id from test_instance where id_test = ${examId})
        ORDER BY id_question;
    `);

  let importantQuestions = [];
  let data = {};
  for (let question of questions) {
    let row = await db.any(`
            SELECT 
                similarities,
                data, 
                colors
            FROM real_time_plag_detection_data
            WHERE id_test = ${examId} and 
            id_question = ${question.id_question} and 
            id_plag_detection_algorithm = ${id_plag_method};
        `);
    if (row.length == 0) continue;

    row = row[0];
    let similarities = JSON.parse(row['similarities']);
    if (similarities[`${firstStudentId}+${secondStudentId}`] == undefined) continue;

    let questionData = JSON.parse(row['data']);

    importantQuestions.push(question.id_question);
    data[question.id_question] = {
      similarity: similarities[`${firstStudentId}+${secondStudentId}`],
      averageSimilarity: questionData.averageSimilarity,
      minSimilarity: questionData.minSimilarity,
      maxSimilarity: questionData.maxSimilarity,
    };
  }

  return {
    examTitle,
    examId,
    firstStudent,
    secondStudent,
    questions: importantQuestions,
    data,
    plagMethod,
  };
}

async function calculateDataAfterExam(examId, testId) {
  let questions = await db.any(`
        SELECT DISTINCT id_question 
        FROM test_instance_question 
        JOIN test_instance ON test_instance.id = test_instance_question.id_test_instance
        WHERE id_test_instance in (SELECT id from test_instance where id_test = ${examId})
        ORDER BY id_question;
    `);

  for (let q of questions) {
    let codeRuns = await CodeRunLog.getCodeRuns(examId, q.id_question);
    let similarities = {};
    let data = {};
    let colors = {};
    let row = await db.any(`
            SELECT id_question
            FROM real_time_plag_detection_data
            WHERE id_test = ${examId} and 
            id_question = ${q.id_question} and 
            id_plag_detection_algorithm = ${testId};`);
    for (let i = 0; i < codeRuns.length - 1; i++) {
      let firstCodeRun = codeRuns[i];
      for (let j = i + 1; j < codeRuns.length; j++) {
        let secondCodeRun = codeRuns[j];

        if (firstCodeRun.id_student != secondCodeRun.id_student) {
          let firstCode = firstCodeRun.code.replace(/\s+/g, '');
          let secondCode = secondCodeRun.code.replace(/\s+/g, '');
          if (firstCode.length && secondCode.length) {
            let key =
              firstCodeRun.id_student < secondCodeRun.id_student
                ? `${firstCodeRun.id_student}+${secondCodeRun.id_student}`
                : `${secondCodeRun.id_student}+${firstCodeRun.id_student}`;
            let similarity = plagiarismDetectionService.similarityMethods[testId](
              firstCodeRun.code.replace(/\s+/g, ','),
              secondCodeRun.code.replace(/\s+/g, ',')
            );
            similarity = Math.round((similarity + Number.EPSILON) * 10000) / 100;
            if (similarities[key] == undefined || similarities[key] < similarity)
              similarities[key] = similarity;
          }
        }
      }
    }
    let allStudents = new Set();
    data.minSimilarity = undefined;
    data.maxSimilarity = undefined;
    data.averageSimilarity = 0;
    for (let key of Object.keys(similarities)) {
      allStudents.add(key.split('+')[0]);
      allStudents.add(key.split('+')[1]);
      data.averageSimilarity += similarities[key];
      if (data.minSimilarity == undefined || similarities[key] < data.minSimilarity)
        data.minSimilarity = similarities[key];
      if (data.maxSimilarity == undefined || similarities[key] > data.maxSimilarity)
        data.maxSimilarity = similarities[key];
    }
    data.averageSimilarity =
      Math.round(
        (data.averageSimilarity / Object.keys(similarities).length + Number.EPSILON) * 100
      ) / 100;
    data.studentsTotal = allStudents.size;
    let students = Array.from(allStudents).sort();
    colors = plagiarismDetectionService.getColorPerQuestion(similarities, data.averageSimilarity);

    if (students.length > 1) {
      if (row.length == 0) {
        db.none(
          `
                INSERT INTO real_time_plag_detection_data(id_test, id_question, similarities, students, data, colors, id_plag_detection_algorithm)
                VALUES ( $1, $2, $3, $4, $5, $6, $7 );`,
          [
            examId,
            q.id_question,
            JSON.stringify(similarities),
            JSON.stringify(students),
            JSON.stringify(data),
            JSON.stringify(colors),
            testId,
          ]
        );
      } else {
        db.any(
          `
                UPDATE real_time_plag_detection_data
                SET (
                    similarities, 
                    students,
                    data,
                    colors
                    ) = ( $3, $4, $5, $6 )
                WHERE id_test = $1 AND id_question = $2;`,
          [
            examId,
            q.id_question,
            JSON.stringify(similarities),
            JSON.stringify(students),
            JSON.stringify(data),
            JSON.stringify(colors),
          ]
        );
      }
    }
  }
}

module.exports = {
  updateSimilarity,
  getExamTableData,
  getQuestionTableData,
  getQuestionCompareData,
  getTestCompareData,
  calculateDataAfterExam,
};
