'use strict';
const db = require('../db').db;
const questionService = require('./questionService');
const mongoose = require('mongoose');
const mongoDb = mongoose.connection;
const TutorialProgress = require.main.require('./models/TutorialProgressModel');

async function listTutorials(courseId) {
  return await db.any(
    `SELECT tutorial.id, tutorial_title, tutorial_desc,
                tutorial.id || '<br/>' ||
                CASE WHEN (is_active)  THEN '&#10003;' ELSE '&#10008;' END
                || '<br/>' || CASE WHEN (is_virgin_tutorial(tutorial.id)) 
                THEN '&#10003;'  ELSE '&#10008;' END id_act_vir,
                uc.first_name || ' ' || uc.last_name AS user_created_name,
                um.first_name || ' ' || um.last_name AS user_modified_name,
                tutorial.ts_created :: timestamp(0) :: varchar, 
                tutorial.ts_modified :: timestamp(0) :: varchar,
                string_agg(DISTINCT course_name, ', ') AS courses,
                COUNT(DISTINCT tutorial_step.id) AS no_steps
            FROM tutorial JOIN tutorial_course
                    ON tutorial.id = tutorial_course.id_tutorial
                JOIN course
                    ON tutorial_course.id_course = course.id
                JOIN app_user AS uc
                    ON tutorial.id_user_created = uc.id
                JOIN app_user AS um
                    on tutorial.id_user_modified = um.id
                LEFT JOIN tutorial_step
                    ON tutorial.id = tutorial_step.id_tutorial
            WHERE id_course = $(id_course)
            GROUP BY tutorial.id, tutorial_title, tutorial_desc,
                uc.first_name, uc.last_name, um.first_name, um.last_name,
                tutorial.ts_created, tutorial.ts_modified
            ORDER BY tutorial.ts_created`,
    {
      id_course: courseId,
    }
  );
}

async function createNewTutorial(
  title,
  description,
  isActive,
  numberOfSteps,
  questionTypeId,
  answersCount,
  codeRunnerId,
  courseId,
  authorUserId
) {
  const rv = await db.func('public.new_tutorial', [
    title,
    description,
    isActive,
    numberOfSteps,
    answersCount,
    questionTypeId,
    codeRunnerId,
    courseId,
    authorUserId,
  ]);

  return parseInt(rv[0].new_tutorial);
}

async function assignTutorialToCourse(tutorialId, courseId) {
  await db.none(
    `INSERT INTO public.tutorial_course(
                id_course, id_tutorial)
                VALUES ($(id_course), $(id_tutorial));`,
    {
      id_course: courseId,
      id_tutorial: tutorialId,
    }
  );
}

async function getRootTutorialsNodeForCourse(courseId) {
  return await db.one(
    `SELECT id_child
                FROM v_roots_children JOIN node
                    ON v_roots_children.id_child = node.id
                    JOIN node_type 
                        ON node.id_node_type = node_type.id
                WHERE type_name='Tutorial'
                    AND id_parent = $(id_course)`,
    {
      id_course: courseId,
    }
  );
}

async function setActive(tutorialId, active) {
  return await db.none(
    `UPDATE public.tutorial
        SET is_active = $(is_active)
        WHERE id = $(id_tutorial)`,
    {
      is_active: active,
      id_tutorial,
      tutorialId,
    }
  );
}

async function addStep(
  tutorialId,
  courseId,
  questionTypeId,
  codeRunnerId,
  answersCount,
  appUserId
) {
  const tutorialNodeId = parseInt(
    (
      await db.one(
        `SELECT id_node
        FROM tutorial
        WHERE id = $(id_tutorial)`,
        {
          id_tutorial: tutorialId,
        }
      )
    ).id_node
  );

  return db
    .func('new_question', [
      parseInt(questionTypeId),
      codeRunnerId,
      answersCount,
      parseInt(appUserId),
      [parseInt(tutorialNodeId)],
      false,
      false,
      null,
    ])
    .then(async (question) => {
      const latestOrdinal = await db.one(
        `SELECT MAX(ordinal) AS ord
            FROM public.tutorial_step
            WHERE id_tutorial = $(id_tutorial)`,
        {
          id_tutorial: tutorialId,
        }
      );
      const nextOrdinal = latestOrdinal && latestOrdinal.ord ? latestOrdinal.ord + 1 : 1;
      const step = await db.one(
        `INSERT INTO public.tutorial_step(id_tutorial, id_question, id_user_created, id_user_modified, ordinal, title, text)
            VALUES($(id_tutorial), $(id_question), $(id_user), $(id_user), $(ordinal), $(title), $(description))
            RETURNING id, ordinal;`,
        {
          id_tutorial: tutorialId,
          id_question: question[0].new_question,
          id_user: appUserId,
          ordinal: nextOrdinal,
          title: 'Generated title, please update',
          description: 'Generated description, please update',
        }
      );
      return step;
    });
}

function saveStep(stepId, step, stepHints, appUserId) {
  return db.tx((t) => {
    return t.batch([
      t.none('SET CONSTRAINTS tutorial_step_hint_ordinal_uniq DEFERRED'),
      t.none(
        `UPDATE public.tutorial_step
                    SET title = $(title), text = $(text), id_user_modified = $(id_user_modified)
                  WHERE id = $(id_step)`,
        {
          id_step: stepId,
          title: step.title,
          text: step.text,
          id_user_modified: appUserId,
        }
      ),
      t.none('SET CONSTRAINTS tutorial_step_hint_ordinal_uniq DEFERRED'),
      stepHints.map((stepHint) => {
        if (stepHint.hint_text.trim().length > 0) {
          if (stepHint.id !== undefined)
            return t.none(
              `UPDATE public.tutorial_step_hint
                            SET is_active = $(is_active),
                                ordinal = $(ordinal), 
                                hint_text = $(hint_text), 
                                id_user_modified = $(id_user_modified)
                            WHERE id = $(id);`,
              {
                is_active: stepHint.is_active ? true : false,
                ordinal: stepHint.ordinal,
                hint_text: stepHint.hint_text,
                id_user_modified: appUserId,
                id: stepHint.id,
              }
            );
          else
            return t.none(
              `INSERT INTO public.tutorial_step_hint(
                                id_step, id_user_created, id_user_modified, is_active, ordinal, hint_text) VALUES(
                                    $(id_step), $(id_user), $(id_user), $(is_active), $(ordinal), $(hint_text));`,
              {
                id_step: stepId,
                id_user: appUserId,
                is_active: stepHint.is_active ? true : false,
                ordinal: stepHint.ordinal,
                hint_text: stepHint.hint_text,
              }
            );
        } else if (stepHint.id !== undefined)
          return t.none(
            `DELETE FROM public.tutorial_step_hint
                        WHERE id = $(id);`,
            {
              id: stepHint.id,
            }
          );
      }),
      t.none('SET CONSTRAINTS tutorial_code_question_hint_ordinal_uniq DEFERRED'),
    ]);
  });
}

// to be deleted once all tuts are upgraded
function saveStepOld(
  stepId,
  step,
  stepHints,
  question,
  questionHints,
  codeQuestionHints,
  answers,
  correctAnswers,
  thresholds,
  penaltyPercentages,
  appUserId,
  req_body
) {
  const funcArguments = questionService.prepareSaveFunctionArguments(req_body, appUserId);
  return db.tx((t) => {
    return t.batch([
      t.func('save_in_place_question', funcArguments),
      t.none('SET CONSTRAINTS tutorial_step_hint_ordinal_uniq DEFERRED'),
      t.none(
        `UPDATE public.tutorial_step
                SET title = $(title), text = $(text), id_user_modified = $(id_user_modified)
                WHERE id = $(id_step)`,
        {
          id_step: stepId,
          title: step.title,
          text: step.text,
          id_user_modified: appUserId,
        }
      ),
      t.none(
        `UPDATE public.question
                SET question_text = $(question_text), id_user_modified = $(id_user_modified)
                WHERE id = $(id_question)`,
        {
          question_text: question.question_text,
          id_user_modified: appUserId,
          id_question: question.id,
        }
      ),
      t.none('SET CONSTRAINTS tutorial_step_hint_ordinal_uniq DEFERRED'),
      stepHints.map((stepHint) => {
        if (stepHint.hint_text.trim().length > 0) {
          if (stepHint.id !== undefined)
            return t.none(
              `UPDATE public.tutorial_step_hint
                            SET is_active = $(is_active),
                                ordinal = $(ordinal), 
                                hint_text = $(hint_text), 
                                id_user_modified = $(id_user_modified)
                            WHERE id = $(id);`,
              {
                is_active: stepHint.is_active ? true : false,
                ordinal: stepHint.ordinal,
                hint_text: stepHint.hint_text,
                id_user_modified: appUserId,
                id: stepHint.id,
              }
            );
          else
            return t.none(
              `INSERT INTO public.tutorial_step_hint(
                                id_step, id_user_created, id_user_modified, is_active, ordinal, hint_text) VALUES(
                                    $(id_step), $(id_user), $(id_user), $(is_active), $(ordinal), $(hint_text));`,
              {
                id_step: stepId,
                id_user: appUserId,
                is_active: stepHint.is_active ? true : false,
                ordinal: stepHint.ordinal,
                hint_text: stepHint.hint_text,
              }
            );
        } else if (stepHint.id !== undefined)
          return t.none(
            `DELETE FROM public.tutorial_step_hint
                        WHERE id = $(id);`,
            {
              id: stepHint.id,
            }
          );
      }),
      t.none('SET CONSTRAINTS tutorial_code_question_hint_ordinal_uniq DEFERRED'),
      questionHints &&
        questionHints.map((questionHint) => {
          if (questionHint.hint_text.trim().length > 0) {
            if (questionHint.id !== undefined)
              return t.none(
                `UPDATE public.tutorial_question_hint
                            SET is_active = $(is_active),
                                hint_text = $(hint_text), 
                                id_user_modified = $(id_user_modified)
                            WHERE id = $(id);`,
                {
                  is_active: questionHint.is_active ? true : false,
                  hint_text: questionHint.hint_text,
                  id_user_modified: appUserId,
                  id: questionHint.id,
                }
              );
            else
              return t.none(
                `INSERT INTO public.tutorial_question_hint(
                                id_step, id_question_answer, id_user_created, id_user_modified, is_active, hint_text) VALUES(
                                    $(id_step), $(id_question_answer), $(id_user), $(id_user), $(is_active), $(hint_text));`,
                {
                  id_step: stepId,
                  id_question_answer: questionHint.id_question_answer,
                  is_active: questionHint.is_active ? true : false,
                  hint_text: questionHint.hint_text,
                  id_user: appUserId,
                }
              );
          } else if (questionHint.id !== undefined)
            return t.none(
              `DELETE FROM public.tutorial_question_hint
                        WHERE id = $(id);`,
              {
                id: questionHint.id,
              }
            );
        }),
      codeQuestionHints &&
        codeQuestionHints.map((hint) => {
          if (hint.hint_text.trim().length > 0) {
            if (hint.id !== undefined)
              return t.none(
                `UPDATE public.tutorial_code_question_hint
                            SET is_active = $(is_active),
                                ordinal = $(ordinal),
                                hint_text = $(hint_text), 
                                answer_regex_trigger = $(answer_regex_trigger),
                                id_user_modified = $(id_user_modified)
                            WHERE id = $(id);`,
                {
                  is_active: hint.is_active ? true : false,
                  ordinal: hint.ordinal,
                  hint_text: hint.hint_text,
                  answer_regex_trigger: hint.regex_trigger,
                  id_user_modified: appUserId,
                  id: hint.id,
                }
              );
            else
              return t.none(
                `INSERT INTO public.tutorial_code_question_hint(
                                id_step, id_question, id_user_created, id_user_modified, is_active, ordinal, hint_text, answer_regex_trigger) VALUES(
                                    $(id_step), $(id_question), $(id_user), $(id_user), $(is_active), $(ordinal), $(hint_text), $(answer_regex_trigger));`,
                {
                  id_step: stepId,
                  id_question: question.id,
                  is_active: hint.is_active ? true : false,
                  ordinal: hint.ordinal,
                  hint_text: hint.hint_text,
                  answer_regex_trigger: hint.regex_trigger,
                  id_user: appUserId,
                }
              );
          } else if (hint.id !== undefined)
            return t.none(
              `DELETE FROM public.tutorial_code_question_hint
                        WHERE id = $(id);`,
              {
                id: hint.id,
              }
            );
        }),
    ]);
  });
}

async function deleteStep(id) {
  await db.func('delete_tutorial_step', [id]);
}

async function deleteTutorial(id) {
  await db.func('delete_tutorial', [id]);
}

async function virginize(id) {
  return await Promise.all([
    db.tx((t) =>
      t.batch([
        t.none(
          `DELETE FROM public.tutorial_student
                    WHERE id_tutorial = $(id_tutorial)`,
          {
            id_tutorial: id,
          }
        ),
        t.none(
          `DELETE FROM public.tutorial_student_question
                    WHERE id_tutorial = $(id_tutorial)`,
          {
            id_tutorial: id,
          }
        ),
        t.none(
          `DELETE FROM public.tutorial_student_question_attempt
                    WHERE id_tutorial = $(id_tutorial)`,
          {
            id_tutorial: id,
          }
        ),
      ])
    ),
    TutorialProgress.clear(id),
  ]);
}

async function clone(tutorialId, courseId, appUserId, title) {
  const rv = await db.func('clone_tutorial', [tutorialId, courseId, appUserId, title]);

  return rv[0].clone_tutorial;
}

module.exports = {
  listTutorials: listTutorials,
  createNewTutorial: createNewTutorial,
  addStep: addStep,
  setActive: setActive,
  saveStep: saveStep,
  saveStepOld: saveStepOld, // to be deleted
  deleteStep: deleteStep,
  deleteTutorial: deleteTutorial,
  virginize: virginize,
  clone: clone,
};
