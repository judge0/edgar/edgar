db.createCollection('testlogdetails', {
  capped: true,
  size: 10737418240, // 10GB
  storageEngine: {
    wiredTiger: { configString: 'block_compressor=zlib' },
  },
});

db.createCollection('templatelogs', {
  capped: true,
  size: 104857600, // 100MB
  storageEngine: {
    wiredTiger: { configString: 'block_compressor=zlib' },
  },
});
db.createCollection('scriptlogs', {
  capped: true,
  size: 104857600, // 100MB
  storageEngine: {
    wiredTiger: { configString: 'block_compressor=zlib' },
  },
});

// db.createCollection("testlogdetails", {
//     storageEngine: {
//         wiredTiger: { configString: "block_compressor=zlib" }
//     }
// })
