
-- WINDOWS:
-- CREATE DATABASE edgar
--   WITH OWNER = postgres
--        ENCODING = 'UTF8'
--        TABLESPACE = pg_default
--        LC_COLLATE = 'Croatian_Croatia.1250'
--        LC_CTYPE = 'Croatian_Croatia.1250'
--        CONNECTION LIMIT = -1;
-- GRANT CONNECT, TEMPORARY ON DATABASE edgar TO public; 
-- GRANT ALL ON DATABASE edgar TO postgres;
-- GRANT CONNECT ON DATABASE edgar TO ro;

-- COMMENT ON DATABASE edgar
--   IS 'Edgar: On-Line Exam Web Application';

-- linux:
-- CREATE DATABASE edgar
--   WITH OWNER = postgres
--        ENCODING = 'UTF8'
--        TABLESPACE = pg_default
--        LC_COLLATE = 'hr_HR.utf8'
--        LC_CTYPE = 'hr_HR.utf8'
--        CONNECTION LIMIT = -1;
-- GRANT CONNECT, TEMPORARY ON DATABASE edgar TO public;
-- GRANT ALL ON DATABASE edgar TO postgres;
-- GRANT CONNECT ON DATABASE edgar TO ro;

-- COMMENT ON DATABASE edgar
--   IS 'Edgar: On-Line Exam Web Application';

-- 
-- CREATE ROLE danijel WITH LOGIN PASSWORD 'changethis' NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION VALID UNTIL 'infinity';
-- CREATE ROLE fran WITH LOGIN PASSWORD 'changethis' NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION VALID UNTIL 'infinity';
-- CREATE ROLE edgar WITH LOGIN PASSWORD 'changethis' NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION VALID UNTIL 'infinity';
-- 

-- "C:\Program Files\PostgreSQL\13\bin\psql" -U postgres < edgar.sql -d aedgar

- Anonymize Edgar

truncate table audit.logged_Actions;

update student set (alt_id, alt_id2, last_name, email) = ('user' || id || '@fer.hr', '0036' || id, 'LastName' || id, 'student' || id ||  '@fer.hr');

-- If I want to leave something as is:
select question.id 
into temp_dontdelete
FROM question
 JOIN question_node on question.id = question_node.id_question
  AND id_node in
      (SELECT id_child FROM v_roots_children WHERE id_root = (select id_root_node from course where course_name = 'Alchemy'))

      

update question set question_text = 'Please, select the correct answer:'
where id not in (select id FROM temp_dontdelete);



update question_answer set answer_text = 'Answer #' || id || '. Hint: this one is: ' || (case when (is_correct) then 'correct!' else 'incorrect.' end)
where id_question not in (select id FROM temp_dontdelete);

update question 
   set question_Text = (select sql_answer from sql_question_answer where id_question = question.id)
   where exists (select * From sql_question_answer where id_question = question.id)
     and id not in (select id FROM temp_dontdelete);

update c_question_answer set c_prefix= '', c_suffix = '', c_source = 'int main() { return 0; }'
   where id_question not in (select id FROM temp_dontdelete);


update code_runner set (host, path) = ('127.0.0.1', '/coderunner');
update code_runner set (host, port, path) = ('coderunner', '10080', '/run') where id = 10;
update code_runner set (host, port, path) = ('pgrunner', '10080', '/ssrunner') where db_schema is not null;

insert into course_code_runner (
      id_course,
      id_programming_language,
      id_code_runner,
      id_type
   )
select course.id,
   (
      select id
      from programming_language
      where name like 'SQL%'
   ),
   code_runner.id,
   23
from code_runner
   join course ON course_name = 'Alchemy'
where db_schema is not null
   and not exists (
      select *
      from course_code_runner ccr
      where ccr.id_course = course.id
         and ccr.id_code_runner = code_runner.id
   );


update app_user set (alt_id, alt_id2, last_name, email, username) = (null, 'user' || id || '.fer.hr', 'LastName' || id, 'user' || id ||  '@fer.hr', 'user' || id || '@fer.hr')
where last_name <> 'Mekterović';


update test
set password = 'pass' || id;

truncate table audit.logged_Actions;   

VACUUM;



REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA public FROM fran;
REVOKE ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public FROM fran;
REVOKE ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public FROM fran;
DROP USER fran;


REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA public FROM danijel;
REVOKE ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public FROM danijel;
REVOKE ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public FROM danijel;
DROP USER danijel;


-- "C:\Program Files\PostgreSQL\13\bin\pg_dump" -U postgres -d aedgar > aedgar.sql