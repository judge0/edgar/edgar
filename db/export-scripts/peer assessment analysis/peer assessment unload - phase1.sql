-- CREATE TABLE public.dataset
-- (
--   id  SERIAL PRIMARY KEY,
--   name text not null,
--   dataset_sql text not null,
--   -----------------------------------
--   ts_created timestamp with time zone NOT NULL,
--   ts_modified timestamp with time zone NOT NULL,
--   user_modified character varying(50) NOT NULL
-- );
-- SELECT audit.audit_table('dataset');
-- insert into dataset  (name, dataset_sql) values ('PA#1 export', 'SELECT...')
-- 
-- CREATE TABLE public.dataset_params
-- (
--   id  SERIAL PRIMARY KEY,
--   id_dataset int NOT NULL references dataset (id),
--   ordinal smallint not null,
--   param_name text not null,
--   param_value text not null,
--   -----------------------------------
--   ts_created timestamp with time zone NOT NULL,
--   ts_modified timestamp with time zone NOT NULL,
--   user_modified character varying(50) NOT NULL
-- );
-- SELECT audit.audit_table('dataset_params');
-- insert into dataset_params (id_dataset, ordinal, param_name, param_value)
-- values
-- (
--     (select id from dataset where name = 'PA#1 export'),
--     1, 'pa1_test_id', '12963'
-- 
-- );
-- 
-- CREATE OR REPLACE FUNCTION public.get_dataset_param(_id_dataset int, _param_name text) RETURNS text 
-- AS $$
--         BEGIN
--                 RETURN (SELECT param_value FROM dataset_params WHERE id_dataset = _id_dataset AND param_name = _param_name);
--         END;
-- $$ LANGUAGE plpgsql;

-- drop table pax;
-- drop table pax2;
-- drop table pax3;
-- drop table pax4;


CREATE TEMP TABLE pax as 

select s1.id as id_student, s1.last_name, ti1.id as id_test_instance1, tiq1.id_question as id_question_pa1,
(SELECT ti.id
    FROM
	(SELECT dense_rank() OVER(				    
	    ORDER BY id_Student1
	) as rank, id_student1, id_pa_test
	    FROM v_peer_shuffle
	WHERE id_test_instance2 = ti1.id) reviews
JOIN test_instance ti
ON ti.id_test = reviews.id_pa_test
AND ti.id_student = reviews.id_student1	
WHERE reviews.rank = 1 ) as ti21
,
(SELECT ti.id
    FROM
	(SELECT dense_rank() OVER(				    
	    ORDER BY id_Student1
	) as rank, id_student1, id_pa_test
	    FROM v_peer_shuffle
	WHERE id_test_instance2 = ti1.id) reviews
JOIN test_instance ti
ON ti.id_test = reviews.id_pa_test
AND ti.id_student = reviews.id_student1	
WHERE reviews.rank = 2 ) as ti22,
(SELECT ti.id
    FROM
	(SELECT dense_rank() OVER(				    
	    ORDER BY id_Student1
	) as rank, id_student1, id_pa_test
	    FROM v_peer_shuffle
	WHERE id_test_instance2 = ti1.id) reviews
JOIN test_instance ti
ON ti.id_test = reviews.id_pa_test
AND ti.id_student = reviews.id_student1	
WHERE reviews.rank = 3 ) as ti23,
(SELECT ti.id
    FROM
	(SELECT dense_rank() OVER(				    
	    ORDER BY id_Student1
	) as rank, id_student1, id_pa_test
	    FROM v_peer_shuffle
	WHERE id_test_instance2 = ti1.id) reviews
JOIN test_instance ti
ON ti.id_test = reviews.id_pa_test
AND ti.id_student = reviews.id_student1	
WHERE reviews.rank = 4 ) as ti24
 from test_instance ti1 
 join student s1
   on ti1.id_student = s1.id
  join test_instance_question tiq1
    on tiq1.id_test_instance = ti1.id
where ti1.id_test = @@pa1_test_id;
-- and last_name like 'Vucić'


-- ***************************************************************
-- ***************************************************************
-- ***************************************************************
-- ***************************************************************

create temp table ptg as 
select 'G' || dense_rank() over( order by id_question ) as group, 0 as QCNT, * From peer_Test_group 
 where id_peer_test = (
select pt.id
  from peer_test pt    
 where pt.id_test_phase_1 =  @@pa1_test_id);

update ptg
   set qcnt = (select count(*) 
   from peer_Test_group_question 
   where id_peer_Test_group = ptg.id); -- - 2 ;-- izbacujem free text i cjelokupni dojam

-- select * from ptg
create temp table pax2 as 
select pax.*, ptg.group, ptg.qcnt, ptg.id_peer_test, ptg.id_calib_assessments_node
  from pax join ptg
    on pax.id_question_pa1 = ptg.id_question;

DROP TABLE ptg;

-- malo statistike:
-- select avg(case when (ti21 is null) then 0 else 1 end  
--     + case when (ti22 is null) then 0 else 1 end  
--     + case when (ti23 is null) then 0 else 1 end  
--     + case when (ti24 is null) then 0 else 1 end  ),
--     min(case when (ti21 is null) then 0 else 1 end  
--     + case when (ti22 is null) then 0 else 1 end  
--     + case when (ti23 is null) then 0 else 1 end  
--     + case when (ti24 is null) then 0 else 1 end  ),
--     max(case when (ti21 is null) then 0 else 1 end  
--     + case when (ti22 is null) then 0 else 1 end  
--     + case when (ti23 is null) then 0 else 1 end  
--     + case when (ti24 is null) then 0 else 1 end  )   
-- FROM pax;
-- 
-- select t.cnt, count(*) from
-- (select (case when (ti21 is null) then 0 else 1 end  
--     + case when (ti22 is null) then 0 else 1 end  
--     + case when (ti23 is null) then 0 else 1 end  
--     + case when (ti24 is null) then 0 else 1 end  ) as cnt
-- from pax ) as t
-- group by t.cnt

CREATE TEMP TABLE pax3 as 

select pax2.*
-- 1:
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 1 ) as q1_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 2 ) as q2_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 3 ) as q3_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 4 ) as q4_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 5 ) as q5_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 6 ) as q6_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 7 ) as q7_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 8 ) as q8_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 9 ) as q9_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 10 ) as q10_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 11 ) as q11_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 12 ) as q12_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 13 ) as q13_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 14 ) as q14_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 15 ) as q15_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 16 ) as q16_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 17 ) as q17_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 18 ) as q18_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 19 ) as q19_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 20 ) as q20_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 21 ) as q21_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 22 ) as q22_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 23 ) as q23_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 24 ) as q24_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 25 ) as q25_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 26 ) as q26_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 27 ) as q27_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 28 ) as q28_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 29 ) as q29_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 30 ) as q30_1
  -- 2:
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 1 ) as q1_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 2 ) as q2_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 3 ) as q3_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 4 ) as q4_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 5 ) as q5_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 6 ) as q6_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 7 ) as q7_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 8 ) as q8_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 9 ) as q9_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 10 ) as q10_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 11 ) as q11_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 12 ) as q12_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 13 ) as q13_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 14 ) as q14_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 15 ) as q15_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 16 ) as q16_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 17 ) as q17_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 18 ) as q18_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 19 ) as q19_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 20 ) as q20_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 21 ) as q21_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 22 ) as q22_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 23 ) as q23_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 24 ) as q24_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 25 ) as q25_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 26 ) as q26_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 27 ) as q27_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 28 ) as q28_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 29 ) as q29_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 30 ) as q30_2
  -- 3:
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 1 ) as q1_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 2 ) as q2_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 3 ) as q3_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 4 ) as q4_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 5 ) as q5_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 6 ) as q6_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 7 ) as q7_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 8 ) as q8_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 9 ) as q9_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 10 ) as q10_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 11 ) as q11_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 12 ) as q12_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 13 ) as q13_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 14 ) as q14_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 15 ) as q15_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 16 ) as q16_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 17 ) as q17_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 18 ) as q18_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 19 ) as q19_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 20 ) as q20_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 21 ) as q21_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 22 ) as q22_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 23 ) as q23_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 24 ) as q24_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 25 ) as q25_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 26 ) as q26_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 27 ) as q27_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 28 ) as q28_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 29 ) as q29_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 30 ) as q30_3
  -- 2:
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 1 ) as q1_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 2 ) as q2_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 3 ) as q3_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 4 ) as q4_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 5 ) as q5_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 6 ) as q6_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 7 ) as q7_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 8 ) as q8_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 9 ) as q9_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 10 ) as q10_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 11 ) as q11_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 12 ) as q12_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 13 ) as q13_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 14 ) as q14_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 15 ) as q15_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 16 ) as q16_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 17 ) as q17_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 18 ) as q18_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 19 ) as q19_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 20 ) as q20_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 21 ) as q21_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 22 ) as q22_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 23 ) as q23_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 24 ) as q24_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 25 ) as q25_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 26 ) as q26_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 27 ) as q27_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 28 ) as q28_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 29 ) as q29_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 30 ) as q30_4
  , 0 as opgrade1
  , 0 as opgrade2
  , 0 as opgrade3
  , 0 as opgrade4
  
 from pax2;

--  "G1";21
-- "G2";23
-- "G3";21
-- "G4";25
-- "G5";21
-- "G6";22
-- "G7";23
-- "G8";24
-- "G9";22
-- "G10";19
-- kopiram "cjelokupni dojam" u posebno pitanje
UPDATE pax3 SET opgrade1 = 
CASE 
    WHEN qcnt = 30 THEN q30_1
    WHEN qcnt = 29 THEN q29_1
    WHEN qcnt = 28 THEN q28_1
    WHEN qcnt = 27 THEN q27_1
    WHEN qcnt = 26 THEN q26_1
    WHEN qcnt = 25 THEN q25_1
    WHEN qcnt = 24 THEN q24_1
    WHEN qcnt = 23 THEN q23_1
    WHEN qcnt = 22 THEN q22_1
    WHEN qcnt = 21 THEN q21_1
    WHEN qcnt = 20 THEN q20_1
    WHEN qcnt = 19 THEN q19_1
    WHEN qcnt = 18 THEN q18_1
    WHEN qcnt = 17 THEN q17_1
    WHEN qcnt = 16 THEN q16_1
    WHEN qcnt = 15 THEN q15_1
    WHEN qcnt = 14 THEN q14_1
    WHEN qcnt = 13 THEN q13_1
    WHEN qcnt = 12 THEN q12_1
    WHEN qcnt = 11 THEN q11_1
    WHEN qcnt = 10 THEN q10_1
    WHEN qcnt = 9 THEN q9_1
    WHEN qcnt = 8 THEN q8_1
    WHEN qcnt = 7 THEN q7_1
    WHEN qcnt = 6 THEN q6_1
    WHEN qcnt = 5 THEN q5_1
    WHEN qcnt = 4 THEN q4_1
    WHEN qcnt = 3 THEN q3_1
    WHEN qcnt = 2 THEN q2_1
    WHEN qcnt = 1 THEN q1_1
END;
UPDATE pax3 SET opgrade2 = 
CASE 
    WHEN qcnt = 30 THEN q30_2
    WHEN qcnt = 29 THEN q29_2
    WHEN qcnt = 28 THEN q28_2
    WHEN qcnt = 27 THEN q27_2
    WHEN qcnt = 26 THEN q26_2
    WHEN qcnt = 25 THEN q25_2
    WHEN qcnt = 24 THEN q24_2
    WHEN qcnt = 23 THEN q23_2
    WHEN qcnt = 22 THEN q22_2
    WHEN qcnt = 21 THEN q21_2
    WHEN qcnt = 20 THEN q20_2
    WHEN qcnt = 19 THEN q19_2
    WHEN qcnt = 18 THEN q18_2
    WHEN qcnt = 17 THEN q17_2
    WHEN qcnt = 16 THEN q16_2
    WHEN qcnt = 15 THEN q15_2
    WHEN qcnt = 14 THEN q14_2
    WHEN qcnt = 13 THEN q13_2
    WHEN qcnt = 12 THEN q12_2
    WHEN qcnt = 11 THEN q11_2
    WHEN qcnt = 10 THEN q10_2
    WHEN qcnt = 9 THEN q9_2
    WHEN qcnt = 8 THEN q8_2
    WHEN qcnt = 7 THEN q7_2
    WHEN qcnt = 6 THEN q6_2
    WHEN qcnt = 5 THEN q5_2
    WHEN qcnt = 4 THEN q4_2
    WHEN qcnt = 3 THEN q3_2
    WHEN qcnt = 2 THEN q2_2
    WHEN qcnt = 1 THEN q1_2
END;
UPDATE pax3 SET opgrade3 = 
CASE 
    WHEN qcnt = 30 THEN q30_3
    WHEN qcnt = 29 THEN q29_3
    WHEN qcnt = 28 THEN q28_3
    WHEN qcnt = 27 THEN q27_3
    WHEN qcnt = 26 THEN q26_3
    WHEN qcnt = 25 THEN q25_3
    WHEN qcnt = 24 THEN q24_3
    WHEN qcnt = 23 THEN q23_3
    WHEN qcnt = 22 THEN q22_3
    WHEN qcnt = 21 THEN q21_3
    WHEN qcnt = 20 THEN q20_3
    WHEN qcnt = 19 THEN q19_3
    WHEN qcnt = 18 THEN q18_3
    WHEN qcnt = 17 THEN q17_3
    WHEN qcnt = 16 THEN q16_3
    WHEN qcnt = 15 THEN q15_3
    WHEN qcnt = 14 THEN q14_3
    WHEN qcnt = 13 THEN q13_3
    WHEN qcnt = 12 THEN q12_3
    WHEN qcnt = 11 THEN q11_3
    WHEN qcnt = 10 THEN q10_3
    WHEN qcnt = 9 THEN q9_3
    WHEN qcnt = 8 THEN q8_3
    WHEN qcnt = 7 THEN q7_3
    WHEN qcnt = 6 THEN q6_3
    WHEN qcnt = 5 THEN q5_3
    WHEN qcnt = 4 THEN q4_3
    WHEN qcnt = 3 THEN q3_3
    WHEN qcnt = 2 THEN q2_3
    WHEN qcnt = 1 THEN q1_3
END;
UPDATE pax3 SET opgrade4 = 
CASE 
    WHEN qcnt = 30 THEN q30_4
    WHEN qcnt = 29 THEN q29_4
    WHEN qcnt = 28 THEN q28_4
    WHEN qcnt = 27 THEN q27_4
    WHEN qcnt = 26 THEN q26_4
    WHEN qcnt = 25 THEN q25_4
    WHEN qcnt = 24 THEN q24_4
    WHEN qcnt = 23 THEN q23_4
    WHEN qcnt = 22 THEN q22_4
    WHEN qcnt = 21 THEN q21_4
    WHEN qcnt = 20 THEN q20_4
    WHEN qcnt = 19 THEN q19_4
    WHEN qcnt = 18 THEN q18_4
    WHEN qcnt = 17 THEN q17_4
    WHEN qcnt = 16 THEN q16_4
    WHEN qcnt = 15 THEN q15_4
    WHEN qcnt = 14 THEN q14_4
    WHEN qcnt = 13 THEN q13_4
    WHEN qcnt = 12 THEN q12_4
    WHEN qcnt = 11 THEN q11_4
    WHEN qcnt = 10 THEN q10_4
    WHEN qcnt = 9 THEN q9_4
    WHEN qcnt = 8 THEN q8_4
    WHEN qcnt = 7 THEN q7_4
    WHEN qcnt = 6 THEN q6_4
    WHEN qcnt = 5 THEN q5_4
    WHEN qcnt = 4 THEN q4_4
    WHEN qcnt = 3 THEN q3_4
    WHEN qcnt = 2 THEN q2_4
    WHEN qcnt = 1 THEN q1_4
END;
-- zatim poništavam ta zadnja dva pitanja:
-- Prvo zadnje:
UPDATE pax3 SET q30_4 = null, q30_3 = null, q30_2 = null, q30_1 = null where qcnt = 30;
UPDATE pax3 SET q29_4 = null, q29_3 = null, q29_2 = null, q29_1 = null where qcnt = 29;
UPDATE pax3 SET q28_4 = null, q28_3 = null, q28_2 = null, q28_1 = null where qcnt = 28;
UPDATE pax3 SET q27_4 = null, q27_3 = null, q27_2 = null, q27_1 = null where qcnt = 27;
UPDATE pax3 SET q26_4 = null, q26_3 = null, q26_2 = null, q26_1 = null where qcnt = 26;
UPDATE pax3 SET q25_4 = null, q25_3 = null, q25_2 = null, q25_1 = null where qcnt = 25;
UPDATE pax3 SET q24_4 = null, q24_3 = null, q24_2 = null, q24_1 = null where qcnt = 24;
UPDATE pax3 SET q23_4 = null, q23_3 = null, q23_2 = null, q23_1 = null where qcnt = 23;
UPDATE pax3 SET q22_4 = null, q22_3 = null, q22_2 = null, q22_1 = null where qcnt = 22;
UPDATE pax3 SET q21_4 = null, q21_3 = null, q21_2 = null, q21_1 = null where qcnt = 21;
UPDATE pax3 SET q20_4 = null, q20_3 = null, q20_2 = null, q20_1 = null where qcnt = 20;
UPDATE pax3 SET q19_4 = null, q19_3 = null, q19_2 = null, q19_1 = null where qcnt = 19;
UPDATE pax3 SET q18_4 = null, q18_3 = null, q18_2 = null, q18_1 = null where qcnt = 18;
UPDATE pax3 SET q17_4 = null, q17_3 = null, q17_2 = null, q17_1 = null where qcnt = 17;
UPDATE pax3 SET q16_4 = null, q16_3 = null, q16_2 = null, q16_1 = null where qcnt = 16;
UPDATE pax3 SET q15_4 = null, q15_3 = null, q15_2 = null, q15_1 = null where qcnt = 15;
UPDATE pax3 SET q14_4 = null, q14_3 = null, q14_2 = null, q14_1 = null where qcnt = 14;
UPDATE pax3 SET q13_4 = null, q13_3 = null, q13_2 = null, q13_1 = null where qcnt = 13;
UPDATE pax3 SET q12_4 = null, q12_3 = null, q12_2 = null, q12_1 = null where qcnt = 12;
UPDATE pax3 SET q11_4 = null, q11_3 = null, q11_2 = null, q11_1 = null where qcnt = 11;
UPDATE pax3 SET q10_4 = null, q10_3 = null, q10_2 = null, q10_1 = null where qcnt = 10;
UPDATE pax3 SET q9_4 = null, q9_3 = null, q9_2 = null, q9_1 = null where qcnt = 9;
UPDATE pax3 SET q8_4 = null, q8_3 = null, q8_2 = null, q8_1 = null where qcnt = 8;
UPDATE pax3 SET q7_4 = null, q7_3 = null, q7_2 = null, q7_1 = null where qcnt = 7;
UPDATE pax3 SET q6_4 = null, q6_3 = null, q6_2 = null, q6_1 = null where qcnt = 6;
UPDATE pax3 SET q5_4 = null, q5_3 = null, q5_2 = null, q5_1 = null where qcnt = 5;
UPDATE pax3 SET q4_4 = null, q4_3 = null, q4_2 = null, q4_1 = null where qcnt = 4;
UPDATE pax3 SET q3_4 = null, q3_3 = null, q3_2 = null, q3_1 = null where qcnt = 3;
UPDATE pax3 SET q2_4 = null, q2_3 = null, q2_2 = null, q2_1 = null where qcnt = 2;


-- zatim predzadnje:
-- ovo zapravo ne treba - to je text pitanje i ono je null 
-- osim ako Pintar ne poreda krivo pitanja, pa stavi text na kraj, onda ipak treba:

UPDATE pax3 SET q29_4 = null, q29_3 = null, q29_2 = null, q29_1 = null where qcnt = 30;
UPDATE pax3 SET q28_4 = null, q28_3 = null, q28_2 = null, q28_1 = null where qcnt = 29;
UPDATE pax3 SET q27_4 = null, q27_3 = null, q27_2 = null, q27_1 = null where qcnt = 28;
UPDATE pax3 SET q26_4 = null, q26_3 = null, q26_2 = null, q26_1 = null where qcnt = 27;
UPDATE pax3 SET q25_4 = null, q25_3 = null, q25_2 = null, q25_1 = null where qcnt = 26;
UPDATE pax3 SET q24_4 = null, q24_3 = null, q24_2 = null, q24_1 = null where qcnt = 25;
UPDATE pax3 SET q23_4 = null, q23_3 = null, q23_2 = null, q23_1 = null where qcnt = 24;
UPDATE pax3 SET q22_4 = null, q22_3 = null, q22_2 = null, q22_1 = null where qcnt = 23;
UPDATE pax3 SET q21_4 = null, q21_3 = null, q21_2 = null, q21_1 = null where qcnt = 22;
UPDATE pax3 SET q20_4 = null, q20_3 = null, q20_2 = null, q20_1 = null where qcnt = 21;
UPDATE pax3 SET q19_4 = null, q19_3 = null, q19_2 = null, q19_1 = null where qcnt = 20;
UPDATE pax3 SET q18_4 = null, q18_3 = null, q18_2 = null, q18_1 = null where qcnt = 19;
UPDATE pax3 SET q17_4 = null, q17_3 = null, q17_2 = null, q17_1 = null where qcnt = 18;
UPDATE pax3 SET q16_4 = null, q16_3 = null, q16_2 = null, q16_1 = null where qcnt = 17;
UPDATE pax3 SET q15_4 = null, q15_3 = null, q15_2 = null, q15_1 = null where qcnt = 16;
UPDATE pax3 SET q14_4 = null, q14_3 = null, q14_2 = null, q14_1 = null where qcnt = 15;
UPDATE pax3 SET q13_4 = null, q13_3 = null, q13_2 = null, q13_1 = null where qcnt = 14;
UPDATE pax3 SET q12_4 = null, q12_3 = null, q12_2 = null, q12_1 = null where qcnt = 13;
UPDATE pax3 SET q11_4 = null, q11_3 = null, q11_2 = null, q11_1 = null where qcnt = 12;
UPDATE pax3 SET q10_4 = null, q10_3 = null, q10_2 = null, q10_1 = null where qcnt = 11;
UPDATE pax3 SET q10_4 = null, q10_3 = null, q10_2 = null, q10_1 = null where qcnt = 11;
UPDATE pax3 SET q9_4 = null, q9_3 = null, q9_2 = null, q9_1 = null where qcnt = 10;
UPDATE pax3 SET q8_4 = null, q8_3 = null, q8_2 = null, q8_1 = null where qcnt = 9;
UPDATE pax3 SET q7_4 = null, q7_3 = null, q7_2 = null, q7_1 = null where qcnt = 8;
UPDATE pax3 SET q6_4 = null, q6_3 = null, q6_2 = null, q6_1 = null where qcnt = 7;
UPDATE pax3 SET q5_4 = null, q5_3 = null, q5_2 = null, q5_1 = null where qcnt = 6;
UPDATE pax3 SET q4_4 = null, q4_3 = null, q4_2 = null, q4_1 = null where qcnt = 5;
UPDATE pax3 SET q3_4 = null, q3_3 = null, q3_2 = null, q3_1 = null where qcnt = 4;
UPDATE pax3 SET q2_4 = null, q2_3 = null, q2_2 = null, q2_1 = null where qcnt = 3;

 
create temp table pax4 as 
select pax3.*
,
coalesce(q1_1,0) + 
coalesce(q2_1,0) + 
coalesce(q3_1,0) + 
coalesce(q4_1,0) + 
coalesce(q5_1,0) + 
coalesce(q6_1,0) + 
coalesce(q7_1,0) + 
coalesce(q8_1,0) + 
coalesce(q9_1,0) + 
coalesce(q10_1,0) + 
coalesce(q11_1,0) + 
coalesce(q12_1,0) + 
coalesce(q13_1,0) + 
coalesce(q14_1,0) + 
coalesce(q15_1,0) + 
coalesce(q16_1,0) + 
coalesce(q17_1,0) + 
coalesce(q18_1,0) + 
coalesce(q19_1,0) + 
coalesce(q20_1,0) + 
coalesce(q21_1,0) + 
coalesce(q22_1,0) + 
coalesce(q23_1,0) + 
coalesce(q24_1,0) + 
coalesce(q25_1,0) +
coalesce(q26_1,0) +
coalesce(q27_1,0) +
coalesce(q28_1,0) +
coalesce(q29_1,0) +
coalesce(q30_1,0)  
as q1t
,
coalesce(q1_2, 0) + 
coalesce(q2_2, 0) + 
coalesce(q3_2, 0) + 
coalesce(q4_2, 0) + 
coalesce(q5_2, 0) + 
coalesce(q6_2, 0) + 
coalesce(q7_2, 0) + 
coalesce(q8_2, 0) + 
coalesce(q9_2, 0) + 
coalesce(q10_2, 0) + 
coalesce(q11_2, 0) + 
coalesce(q12_2, 0) + 
coalesce(q13_2, 0) + 
coalesce(q14_2, 0) + 
coalesce(q15_2, 0) + 
coalesce(q16_2, 0) + 
coalesce(q17_2, 0) + 
coalesce(q18_2, 0) + 
coalesce(q19_2, 0) + 
coalesce(q20_2, 0) + 
coalesce(q21_2, 0) + 
coalesce(q22_2, 0) + 
coalesce(q23_2, 0) + 
coalesce(q24_2, 0) + 
coalesce(q25_2, 0) +
coalesce(q26_2, 0) +
coalesce(q27_2, 0) +
coalesce(q28_2, 0) +
coalesce(q29_2, 0) +
coalesce(q30_2, 0)
 as q2t
,
coalesce(q1_3, 0) + 
coalesce(q2_3, 0) + 
coalesce(q3_3, 0) + 
coalesce(q4_3, 0) + 
coalesce(q5_3, 0) + 
coalesce(q6_3, 0) + 
coalesce(q7_3, 0) + 
coalesce(q8_3, 0) + 
coalesce(q9_3, 0) + 
coalesce(q10_3, 0) + 
coalesce(q11_3, 0) + 
coalesce(q12_3, 0) + 
coalesce(q13_3, 0) + 
coalesce(q14_3, 0) + 
coalesce(q15_3, 0) + 
coalesce(q16_3, 0) + 
coalesce(q17_3, 0) + 
coalesce(q18_3, 0) + 
coalesce(q19_3, 0) + 
coalesce(q20_3, 0) + 
coalesce(q21_3, 0) + 
coalesce(q22_3, 0) + 
coalesce(q23_3, 0) + 
coalesce(q24_3, 0) + 
coalesce(q25_3, 0) +
coalesce(q26_3, 0) +
coalesce(q27_3, 0) +
coalesce(q28_3, 0) +
coalesce(q29_3, 0) +
coalesce(q30_3, 0) 
as q3t
,
coalesce(q1_4,0) + 
coalesce(q2_4,0) + 
coalesce(q3_4,0) + 
coalesce(q4_4,0) + 
coalesce(q5_4,0) + 
coalesce(q6_4,0) + 
coalesce(q7_4,0) + 
coalesce(q8_4,0) + 
coalesce(q9_4,0) + 
coalesce(q10_4,0) + 
coalesce(q11_4,0) + 
coalesce(q12_4,0) + 
coalesce(q13_4,0) + 
coalesce(q14_4,0) + 
coalesce(q15_4,0) + 
coalesce(q16_4,0) + 
coalesce(q17_4,0) + 
coalesce(q18_4,0) + 
coalesce(q19_4,0) + 
coalesce(q20_4,0) + 
coalesce(q21_4,0) + 
coalesce(q22_4,0) + 
coalesce(q23_4,0) + 
coalesce(q24_4,0) + 
coalesce(q25_4,0) +
coalesce(q26_4,0) +
coalesce(q27_4,0) +
coalesce(q28_4,0) +
coalesce(q29_4,0) +
coalesce(q30_4,0)  
as q4t

,
1.0 * (
case when (q1_1 is null) then 0 else 1 end +
case when (q2_1 is null) then 0 else 1 end +
case when (q3_1 is null) then 0 else 1 end +
case when (q4_1 is null) then 0 else 1 end +
case when (q5_1 is null) then 0 else 1 end +
case when (q6_1 is null) then 0 else 1 end +
case when (q7_1 is null) then 0 else 1 end +
case when (q8_1 is null) then 0 else 1 end +
case when (q9_1 is null) then 0 else 1 end +
case when (q10_1 is null) then 0 else 1 end +
case when (q11_1 is null) then 0 else 1 end +
case when (q12_1 is null) then 0 else 1 end +
case when (q13_1 is null) then 0 else 1 end +
case when (q14_1 is null) then 0 else 1 end +
case when (q15_1 is null) then 0 else 1 end +
case when (q16_1 is null) then 0 else 1 end +
case when (q17_1 is null) then 0 else 1 end +
case when (q18_1 is null) then 0 else 1 end +
case when (q19_1 is null) then 0 else 1 end +
case when (q20_1 is null) then 0 else 1 end +
case when (q21_1 is null) then 0 else 1 end +
case when (q22_1 is null) then 0 else 1 end +
case when (q23_1 is null) then 0 else 1 end +
case when (q24_1 is null) then 0 else 1 end +
case when (q25_1 is null) then 0 else 1 end +
case when (q26_1 is null) then 0 else 1 end +
case when (q27_1 is null) then 0 else 1 end +
case when (q28_1 is null) then 0 else 1 end +
case when (q29_1 is null) then 0 else 1 end +
case when (q30_1 is null) then 0 else 1 end ) 
/ (qcnt - 2) as support1,

1.0 * (
case when (q1_2 is null) then 0 else 1 end +
case when (q2_2 is null) then 0 else 1 end +
case when (q3_2 is null) then 0 else 1 end +
case when (q4_2 is null) then 0 else 1 end +
case when (q5_2 is null) then 0 else 1 end +
case when (q6_2 is null) then 0 else 1 end +
case when (q7_2 is null) then 0 else 1 end +
case when (q8_2 is null) then 0 else 1 end +
case when (q9_2 is null) then 0 else 1 end +
case when (q10_2 is null) then 0 else 1 end +
case when (q11_2 is null) then 0 else 1 end +
case when (q12_2 is null) then 0 else 1 end +
case when (q13_2 is null) then 0 else 1 end +
case when (q14_2 is null) then 0 else 1 end +
case when (q15_2 is null) then 0 else 1 end +
case when (q16_2 is null) then 0 else 1 end +
case when (q17_2 is null) then 0 else 1 end +
case when (q18_2 is null) then 0 else 1 end +
case when (q19_2 is null) then 0 else 1 end +
case when (q20_2 is null) then 0 else 1 end +
case when (q21_2 is null) then 0 else 1 end +
case when (q22_2 is null) then 0 else 1 end +
case when (q23_2 is null) then 0 else 1 end +
case when (q24_2 is null) then 0 else 1 end +
case when (q25_2 is null) then 0 else 1 end +
case when (q26_2 is null) then 0 else 1 end +
case when (q27_2 is null) then 0 else 1 end +
case when (q28_2 is null) then 0 else 1 end +
case when (q29_2 is null) then 0 else 1 end +
case when (q30_2 is null) then 0 else 1 end 
) / (qcnt - 2) as support2,

1.0 * (
case when (q1_3 is null) then 0 else 1 end +
case when (q2_3 is null) then 0 else 1 end +
case when (q3_3 is null) then 0 else 1 end +
case when (q4_3 is null) then 0 else 1 end +
case when (q5_3 is null) then 0 else 1 end +
case when (q6_3 is null) then 0 else 1 end +
case when (q7_3 is null) then 0 else 1 end +
case when (q8_3 is null) then 0 else 1 end +
case when (q9_3 is null) then 0 else 1 end +
case when (q10_3 is null) then 0 else 1 end +
case when (q11_3 is null) then 0 else 1 end +
case when (q12_3 is null) then 0 else 1 end +
case when (q13_3 is null) then 0 else 1 end +
case when (q14_3 is null) then 0 else 1 end +
case when (q15_3 is null) then 0 else 1 end +
case when (q16_3 is null) then 0 else 1 end +
case when (q17_3 is null) then 0 else 1 end +
case when (q18_3 is null) then 0 else 1 end +
case when (q19_3 is null) then 0 else 1 end +
case when (q20_3 is null) then 0 else 1 end +
case when (q21_3 is null) then 0 else 1 end +
case when (q22_3 is null) then 0 else 1 end +
case when (q23_3 is null) then 0 else 1 end +
case when (q24_3 is null) then 0 else 1 end +
case when (q25_3 is null) then 0 else 1 end +
case when (q26_3 is null) then 0 else 1 end +
case when (q27_3 is null) then 0 else 1 end +
case when (q28_3 is null) then 0 else 1 end +
case when (q29_3 is null) then 0 else 1 end +
case when (q30_3 is null) then 0 else 1 end 
) / (qcnt - 2) as support3,


1.0 * (
case when (q1_4 is null) then 0 else 1 end +
case when (q2_4 is null) then 0 else 1 end +
case when (q3_4 is null) then 0 else 1 end +
case when (q4_4 is null) then 0 else 1 end +
case when (q5_4 is null) then 0 else 1 end +
case when (q6_4 is null) then 0 else 1 end +
case when (q7_4 is null) then 0 else 1 end +
case when (q8_4 is null) then 0 else 1 end +
case when (q9_4 is null) then 0 else 1 end +
case when (q10_4 is null) then 0 else 1 end +
case when (q11_4 is null) then 0 else 1 end +
case when (q12_4 is null) then 0 else 1 end +
case when (q13_4 is null) then 0 else 1 end +
case when (q14_4 is null) then 0 else 1 end +
case when (q15_4 is null) then 0 else 1 end +
case when (q16_4 is null) then 0 else 1 end +
case when (q17_4 is null) then 0 else 1 end +
case when (q18_4 is null) then 0 else 1 end +
case when (q19_4 is null) then 0 else 1 end +
case when (q20_4 is null) then 0 else 1 end +
case when (q21_4 is null) then 0 else 1 end +
case when (q22_4 is null) then 0 else 1 end +
case when (q23_4 is null) then 0 else 1 end +
case when (q24_4 is null) then 0 else 1 end +
case when (q25_4 is null) then 0 else 1 end +
case when (q26_4 is null) then 0 else 1 end +
case when (q27_4 is null) then 0 else 1 end +
case when (q28_4 is null) then 0 else 1 end +
case when (q29_4 is null) then 0 else 1 end +
case when (q30_4 is null) then 0 else 1 end 
) / (qcnt - 2) as support4

from pax3;

-- *******************************************************
-- *******************************************************
-- *******************************************************
drop table pax;
drop table pax2;
drop table pax3;

--create temp table dataset_csv as select * from pax4;




-- select pax4.group, q1t, q2t, q3t, q4t, support1, support2, support3, support4
-- from pax4
-- order by support1;

--select * from stud_bias;

--drop table pax5;
create temp table pax5 as 
select pax4.*
, (select bias from stud_bias join test_instance on stud_bias.id_student = test_instance.id_student where id = pax4.ti21) as bias1
, (select bias from stud_bias join test_instance on stud_bias.id_student = test_instance.id_student where id = pax4.ti22) as bias2
, (select bias from stud_bias join test_instance on stud_bias.id_student = test_instance.id_student where id = pax4.ti23) as bias3
, (select bias from stud_bias join test_instance on stud_bias.id_student = test_instance.id_student where id = pax4.ti24) as bias4
From pax4 ;

drop table pax4;


-- edit 2022:
update pax5 set qcnt = qcnt - 2;

COPY pax5 TO '/tmp/pa_phase_1_bias.csv' WITH (FORMAT CSV, HEADER);