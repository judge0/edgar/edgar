var assert = require('assert');
var should = require('should');
var lib = require('./../common/libeval');
var libc = require('./../common/libeval.code');
var utils = require('./../common/utils');

describe('Test evaluation functions: C questionScore (testEvalLibCodeRunner.1.js)', function () {
  //includes case sensitive tests also !!!
  it('5 - ignore whitespace: trim trailing ', function () {
    var gradingModel = {
      correct_score: 15,
      incorrect_score: 0,
      unanswered_score: 0.0,
    };

    var studentResult = {
      status: {
        id: 14,
        description: 'Finished',
      },
      internal_message: null,
      results: [
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '  First row:   \nSecond row:   \nLast row\n  ',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: 'First row:   \nSecond row:   \nLast row\n  ',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '  One row only  \n\n\n\n\n\n\n\n',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: 'First row\n\n\nThird row',
        },
      ],
    };
    var evalConfig = [
      {
        id: 0,
        percentage: '20.00',
        diff_order: false,
        case_insensitive: false,
        trim_whitespace: true,
        regex: '',
        input: 'whatevs',
        output: 'First row:\nSecond row:\nLast row',
        is_public: false,
      },
      {
        id: 1,
        percentage: '30.00',
        diff_order: false,
        case_insensitive: false,
        trim_whitespace: true,
        regex: '',
        input: 'whatevs',
        output: 'First row:\nSecond row:\nLast row',
        is_public: false,
      },
      {
        id: 2,
        percentage: '40.00',
        diff_order: false,
        case_insensitive: false,
        trim_whitespace: true,
        regex: '',
        input: 'whatevs',
        output: '  One row only  \n\n',
        is_public: false,
      },
      {
        id: 3,
        percentage: '10.00',
        diff_order: false,
        case_insensitive: false,
        trim_whitespace: true,
        regex: '',
        input: 'whatevs',
        output: 'First row\n\nThird row',
        is_public: true,
      },
    ];

    libc.getCQuestionScore(evalConfig, studentResult, gradingModel).should.eql({
      is_correct: false,
      is_incorrect: false,
      is_unanswered: false,
      is_partial: true,
      score: 0.7 * gradingModel.correct_score,
      score_perc: 0.7,
      hint: 'Partial score',
      c_outcome: [
        {
          expected: 'First row:\nSecond row:\nLast row',
          hint: 'Incorrect output',
          input: 'whatevs',
          isCorrect: false,
          mode: 'check elements order : false, case sensitive : true, ignore whitespace : true',
          output: '  First row:   \nSecond row:   \nLast row\n  ', // fails bcs leading spaces are not trimmed
          percentage: '20.00',
          stderr: undefined,
          comment: undefined,
          is_public: false,
        },
        {
          expected: 'First row:\nSecond row:\nLast row',
          hint: 'Correct. Well done!',
          input: 'whatevs',
          isCorrect: true,
          mode: 'check elements order : false, case sensitive : true, ignore whitespace : true',
          output: 'First row:   \nSecond row:   \nLast row\n  ',
          percentage: '30.00',
          stderr: undefined,
          comment: undefined,
          is_public: false,
        },
        {
          expected: '  One row only  \n\n',
          hint: 'Correct. Well done!',
          input: 'whatevs',
          isCorrect: true,
          mode: 'check elements order : false, case sensitive : true, ignore whitespace : true',
          output: '  One row only  \n\n\n\n\n\n\n\n',
          percentage: '40.00',
          stderr: undefined,
          comment: undefined,
          is_public: false,
        },
        {
          expected: 'First row\n\nThird row',
          hint: 'Incorrect output',
          input: 'whatevs',
          isCorrect: false,
          mode: 'check elements order : false, case sensitive : true, ignore whitespace : true',
          output: 'First row\n\n\nThird row',
          percentage: '10.00',
          stderr: undefined,
          comment: undefined,
          is_public: true,
        },
      ],
    });
  });
});
