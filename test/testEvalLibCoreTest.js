var should = require('should');
var assert = require('assert');
var lib = require('../common/libeval');

describe('Test gradeTest() - testEvalLibCoreTest.js', function () {
  it('1. Should pass: checking score for various question types and gms.', function (done) {
    var rs = [
      {
        answers_permutation: [1, 2, 5, 4, 3],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [],
        correct_answers_permutation: [3],
        correct_score: '10.00',
        incorrect_score: '-3.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.0',
        max_score: '100.00',
        id_test_part: 10,
        id: 100,
      }, // Cumulative: score = 0, correct = 0, incorrect = 0, parital = 0, unanswered = 1
      {
        ordinal: 2,
        answers_permutation: [4, 3, 5, 2, 1],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [1],
        correct_answers_permutation: [3],
        correct_score: '10.00',
        incorrect_score: '-3.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.0',
        max_score: '100.00',
        id_test_part: 10,
        id: 101,
      }, // Cumulative: score = -3, correct = 0, incorrect = 1, parital = 0, unanswered = 1
      {
        ordinal: 3,
        answers_permutation: [1, 3, 5, 4, 2],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [4],
        correct_answers_permutation: [3, 4],
        correct_score: '5.00',
        incorrect_score: '-1.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.0',
        max_score: '100.00',
        id_test_part: 11,
        id: 102,
      }, // Cumulative: score = 2, correct = 0, incorrect = 1, parital = 1, unanswered = 1
      {
        ordinal: 4,
        answers_permutation: [4, 3, 5, 1, 2],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [3],
        correct_answers_permutation: [3],
        correct_score: '10.00',
        incorrect_score: '-3.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.0',
        max_score: '100.00',
        id_test_part: 10,
        id: 103,
      }, // Cumulative: score = 12, correct = 1, incorrect = 1, parital = 1, unanswered = 1
      {
        ordinal: 5,
        answers_permutation: [1, 5, 3, 2, 4],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [2, 3],
        correct_answers_permutation: [3, 2],
        correct_score: '15.00',
        incorrect_score: '-5.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.0',
        max_score: '100.00',
        id_test_part: 12,
        id: 104,
      }, // Cumulative: score = 42, correct = 2, incorrect = 1, parital = 1, unanswered = 1
    ];

    var db = {};
    db.any = function (dontCare, alsoDontCare) {
      return new Promise(function (resolve, reject) {
        return resolve(rs);
      });
    };

    lib
      .gradeTest(1, db, null)
      .then(function (rv) {
        rv.should.eql({
          correct_no: 2,
          incorrect_no: 2,
          unanswered_no: 1,
          partial_no: 0,
          score: 21,
          passed: true,
          score_perc: 0.21,
          qscores: [
            {
              id: 100,
              is_correct: false,
              is_incorrect: false,
              is_partial: false,
              is_unanswered: true,
              score: 0,
              score_perc: 0,
            },
            {
              id: 101,
              is_correct: false,
              is_incorrect: true,
              is_partial: false,
              is_unanswered: false,
              score: -3,
              score_perc: -0.3,
            },
            {
              id: 102,
              is_correct: false,
              is_incorrect: true,
              is_partial: false,
              is_unanswered: false,
              score: -1,
              score_perc: -1 / 5,
            },
            {
              id: 103,
              is_correct: true,
              is_incorrect: false,
              is_partial: false,
              is_unanswered: false,
              score: 10,
              score_perc: 1,
            },
            {
              id: 104,
              is_correct: true,
              is_incorrect: false,
              is_partial: false,
              is_unanswered: false,
              score: 15,
              score_perc: 1,
            },
          ],
        });
        done();
      })
      .catch(function (reason) {
        console.log('Failed:' + reason);
        assert(false);
        done();
      });
  });

  it('2. Should fail: beacause test_part 10 fails', function (done) {
    var rs = [
      {
        answers_permutation: [1, 2, 5, 4, 3],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [],
        correct_answers_permutation: [3],
        correct_score: '10.00',
        incorrect_score: '-3.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.5',
        max_score: '100.00',
        id_test_part: 10,
        id: 100,
      }, // Cumulative: score = 0, correct = 0, incorrect = 0, parital = 0, unanswered = 1
      {
        ordinal: 2,
        answers_permutation: [4, 3, 5, 2, 1],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [1],
        correct_answers_permutation: [3],
        correct_score: '10.00',
        incorrect_score: '-3.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.5',
        max_score: '100.00',
        id_test_part: 10,
        id: 101,
      }, // Cumulative: score = -3, correct = 0, incorrect = 1, parital = 0, unanswered = 1
      {
        ordinal: 3,
        answers_permutation: [1, 3, 5, 4, 2],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [4],
        correct_answers_permutation: [3, 4],
        correct_score: '5.00',
        incorrect_score: '-1.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.0',
        max_score: '100.00',
        id_test_part: 11,
        id: 102,
      }, // Cumulative: score = 2, correct = 0, incorrect = 1, parital = 1, unanswered = 1
      {
        ordinal: 4,
        answers_permutation: [4, 3, 5, 1, 2],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [3],
        correct_answers_permutation: [3],
        correct_score: '10.00',
        incorrect_score: '-3.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.5',
        max_score: '100.00',
        id_test_part: 10,
        id: 104,
      }, // Cumulative: score = 12, correct = 1, incorrect = 1, parital = 1, unanswered = 1
      {
        ordinal: 5,
        answers_permutation: [1, 5, 3, 2, 4],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [2, 3],
        correct_answers_permutation: [3, 2],
        correct_score: '15.00',
        incorrect_score: '-5.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.0',
        max_score: '100.00',
        id_test_part: 12,
        id: 105,
      }, // Cumulative: score = 42, correct = 2, incorrect = 1, parital = 1, unanswered = 1
    ];

    var db = {};
    db.any = function (dontCare, alsoDontCare) {
      return new Promise(function (resolve, reject) {
        return resolve(rs);
      });
    };

    lib
      .gradeTest(1, db, null)
      .then(function (rv) {
        rv.qscores = 'out-of-focus';
        rv.should.eql({
          correct_no: 2,
          incorrect_no: 2,
          unanswered_no: 1,
          partial_no: 0,
          score: 21,
          passed: false,
          score_perc: 0.21,
          qscores: 'out-of-focus',
        });
        done();
      })
      .catch(function (reason) {
        console.log('Failed:' + reason);
        assert(false);
        done();
      });
  });

  it('3. Should fail: overall percentage 50% required', function (done) {
    var rs = [
      {
        answers_permutation: [1, 2, 5, 4, 3],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [],
        correct_answers_permutation: [3],
        correct_score: '10.00',
        incorrect_score: '-3.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.5',
        tp_pass_perc: '0.0',
        max_score: '100.00',
        id_test_part: 10,
        id: 100,
      }, // Cumulative: score = 0, correct = 0, incorrect = 0, parital = 0, unanswered = 1
      {
        ordinal: 2,
        answers_permutation: [4, 3, 5, 2, 1],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [1],
        correct_answers_permutation: [3],
        correct_score: '10.00',
        incorrect_score: '-3.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.5',
        tp_pass_perc: '0.0',
        max_score: '100.00',
        id_test_part: 10,
        id: 101,
      }, // Cumulative: score = -3, correct = 0, incorrect = 1, parital = 0, unanswered = 1
      {
        ordinal: 3,
        answers_permutation: [1, 3, 5, 4, 2],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [4],
        correct_answers_permutation: [3, 4],
        correct_score: '5.00',
        incorrect_score: '-1.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.5',
        tp_pass_perc: '0.0',
        max_score: '100.00',
        id_test_part: 11,
        id: 102,
      }, // Cumulative: score = 2, correct = 0, incorrect = 1, parital = 1, unanswered = 1
      {
        ordinal: 4,
        answers_permutation: [4, 3, 5, 1, 2],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [3],
        correct_answers_permutation: [3],
        correct_score: '10.00',
        incorrect_score: '-3.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.5',
        tp_pass_perc: '0.0',
        max_score: '100.00',
        id_test_part: 10,
        id: 103,
      }, // Cumulative: score = 12, correct = 1, incorrect = 1, parital = 1, unanswered = 1
      {
        ordinal: 5,
        answers_permutation: [1, 5, 3, 2, 4],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [2, 3],
        correct_answers_permutation: [3, 2],
        correct_score: '15.00',
        incorrect_score: '-5.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.5',
        tp_pass_perc: '0.0',
        max_score: '100.00',
        id_test_part: 12,
        id: 104,
      }, // Cumulative: score = 42, correct = 2, incorrect = 1, parital = 1, unanswered = 1
    ];

    var db = {};
    db.any = function (dontCare, alsoDontCare) {
      return new Promise(function (resolve, reject) {
        return resolve(rs);
      });
    };

    lib
      .gradeTest(1, db, null)
      .then(function (rv) {
        rv.qscores = 'out-of-focus';
        rv.should.eql({
          correct_no: 2,
          incorrect_no: 2,
          unanswered_no: 1,
          partial_no: 0,
          score: 21,
          passed: false,
          score_perc: 0.21,
          qscores: 'out-of-focus',
        });
        done();
      })
      .catch(function (reason) {
        console.log('Failed:' + reason);
        assert(false);
        done();
      });
  });

  it('4. Should pass: Mixed content, ABC and SQL', function (done) {
    var rs = [
      {
        ordinal: 1,
        answers_permutation: [1, 2, 5, 4, 3],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [],
        correct_answers_permutation: [3],
        correct_score: '10.00',
        incorrect_score: '-3.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.0',
        max_score: '100.00',
        id_test_part: 10,
        id: 100,
      }, // Cumulative: score = 0, correct = 0, incorrect = 0, parital = 0, unanswered = 1
      {
        ordinal: 2,
        answers_permutation: [4, 3, 5, 2, 1],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [1],
        correct_answers_permutation: [3],
        correct_score: '10.00',
        incorrect_score: '-3.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.0',
        max_score: '100.00',
        id_test_part: 10,
        id: 101,
      }, // Cumulative: score = -3, correct = 0, incorrect = 1, parital = 0, unanswered = 1
      {
        ordinal: 3,
        answers_permutation: null,
        weights_permutation: null,
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: null,
        correct_answers_permutation: null,
        sql_alt_assertion: '',
        sql_test_fixture: '',
        student_answer_code: 'SELECT * FROM student',
        sql_answer: 'SELECT * FROM mjesto',
        check_tuple_order: false,
        id_check_column_mode: 4,
        correct_score: '15.00',
        incorrect_score: '-5.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.0',
        max_score: '100.00',
        id_test_part: 11,
        id: 102,
      }, // Cumulative: score = 2, correct = 0, incorrect = 1, parital = 1, unanswered = 1
      {
        ordinal: 4,
        answers_permutation: [4, 3, 5, 1, 2],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [3],
        correct_answers_permutation: [3],
        correct_score: '10.00',
        incorrect_score: '-3.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.0',
        max_score: '100.00',
        id_test_part: 10,
        id: 103,
      }, // Cumulative: score = 12, correct = 1, incorrect = 1, parital = 1, unanswered = 1
      {
        ordinal: 5,
        answers_permutation: [1, 5, 3, 2, 4],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [2, 3],
        correct_answers_permutation: [3, 2],
        correct_score: '15.00',
        incorrect_score: '-5.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.0',
        max_score: '100.00',
        id_test_part: 12,
        id: 104,
      }, // Cumulative: score = 42, correct = 2, incorrect = 1, parital = 1, unanswered = 1
    ];

    var db = {};
    db.any = function (dontCare, alsoDontCare) {
      return new Promise(function (resolve, reject) {
        return resolve(rs);
      });
    };
    var testService = {};
    testService.execCode = function (dontCare, alsoDontCare, alsoDontCare2) {
      return new Promise(function (resolve, reject) {
        var rv = {
          success: true,
          data: {
            fields: [{ name: 'a' }, { name: 'b' }],
            rows: [
              {
                C0: 1,
                C1: 2,
              },
              {
                C0: 2,
                C1: 3,
              },
            ],
          },
        };
        return resolve(rv);
      });
    };
    lib
      .gradeTest(1, db, testService)
      .then(function (rv) {
        rv.should.eql({
          correct_no: 3,
          incorrect_no: 1,
          unanswered_no: 1,
          partial_no: 0,
          score: 37,
          passed: true,
          score_perc: 0.37,
          qscores: [
            {
              id: 100,
              is_correct: false,
              is_incorrect: false,
              is_partial: false,
              is_unanswered: true,
              score: 0,
              score_perc: 0,
            },
            {
              id: 101,
              is_correct: false,
              is_incorrect: true,
              is_partial: false,
              is_unanswered: false,
              score: -3,
              score_perc: -0.3,
            },
            {
              id: 102,
              is_correct: true,
              is_incorrect: false,
              is_partial: false,
              is_unanswered: false,
              score: 15,
              score_perc: 1.0,
              hint: 'Correct. Well done!',
            },
            {
              id: 103,
              is_correct: true,
              is_incorrect: false,
              is_partial: false,
              is_unanswered: false,
              score: 10,
              score_perc: 1,
            },
            {
              id: 104,
              is_correct: true,
              is_incorrect: false,
              is_partial: false,
              is_unanswered: false,
              score: 15,
              score_perc: 1,
            },
          ],
        });
        done();
      })
      .catch(function (reason) {
        console.log('Failed:' + reason);
        assert(false);
        done();
      });
  });

  it('5. Mixed content, ABC and SQL, sql wrong', function (done) {
    var rs = [
      {
        ordinal: 1,
        answers_permutation: [1, 2, 5, 4, 3],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [],
        correct_answers_permutation: [3],
        correct_score: '10.00',
        incorrect_score: '-3.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.0',
        max_score: '100.00',
        id_test_part: 10,
        id: 100,
      }, // Cumulative: score = 0, correct = 0, incorrect = 0, parital = 0, unanswered = 1
      {
        ordinal: 2,
        answers_permutation: [4, 3, 5, 2, 1],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [1],
        correct_answers_permutation: [3],
        correct_score: '10.00',
        incorrect_score: '-3.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.0',
        max_score: '100.00',
        id_test_part: 10,
        id: 101,
      }, // Cumulative: score = -3, correct = 0, incorrect = 1, parital = 0, unanswered = 1
      {
        ordinal: 3,
        answers_permutation: null,
        weights_permutation: null,
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: null,
        correct_answers_permutation: null,
        sql_alt_assertion: '',
        sql_test_fixture: '',
        student_answer_code: 'SELECT * FROM student',
        sql_answer: 'SELECT * From Mjesto',
        check_tuple_order: false,
        id_check_column_mode: 4,
        correct_score: '15.00',
        incorrect_score: '-5.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.0',
        max_score: '100.00',
        id_test_part: 11,
        id: 102,
      }, // Cumulative: score = 2, correct = 0, incorrect = 1, parital = 1, unanswered = 1
      {
        ordinal: 4,
        answers_permutation: [4, 3, 5, 1, 2],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [3],
        correct_answers_permutation: [3],
        correct_score: '10.00',
        incorrect_score: '-3.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.0',
        max_score: '100.00',
        id_test_part: 10,
        id: 103,
      }, // Cumulative: score = 12, correct = 1, incorrect = 1, parital = 1, unanswered = 1
      {
        ordinal: 5,
        answers_permutation: [1, 5, 3, 2, 4],
        weights_permutation: [1, 1, 1, 1, 1],
        penalty_percentage_permutation: [100, 100, 100, 100, 100],
        student_answers: [2, 3],
        correct_answers_permutation: [3, 2],
        correct_score: '15.00',
        incorrect_score: '-5.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.0',
        max_score: '100.00',
        id_test_part: 12,
        id: 104,
      }, // Cumulative: score = 42, correct = 2, incorrect = 1, parital = 1, unanswered = 1
    ];

    var db = {};
    db.any = function (dontCare, alsoDontCare) {
      return new Promise(function (resolve, reject) {
        return resolve(rs);
      });
    };
    var testService = {};
    var counter = 1;
    testService.execCode = function (dontCare, alsoDontCare, alsoDontCare2) {
      return new Promise(function (resolve, reject) {
        ++counter;
        var rv = {
          success: true,
          data: {
            fields: [{ name: 'a' }, { name: 'b' }],
            rows: [
              {
                C0: counter,
                C1: 2,
              },
              {
                C0: 2,
                C1: 3,
              },
            ],
          },
        };
        return resolve(rv);
      });
    };
    lib
      .gradeTest(1, db, testService)
      .then(function (rv) {
        rv.should.eql({
          correct_no: 2,
          incorrect_no: 2,
          unanswered_no: 1,
          partial_no: 0,
          score: 17,
          passed: true,
          score_perc: 0.17,
          qscores: [
            {
              id: 100,
              is_correct: false,
              is_incorrect: false,
              is_partial: false,
              is_unanswered: true,
              score: 0,
              score_perc: 0,
            },
            {
              id: 101,
              is_correct: false,
              is_incorrect: true,
              is_partial: false,
              is_unanswered: false,
              score: -3,
              score_perc: -0.3,
            },
            {
              id: 102,
              is_correct: false,
              is_incorrect: true,
              is_partial: false,
              is_unanswered: false,
              score: -5,
              score_perc: -5 / 15,
              hint: 'Cannot find user row #1 in correct rows (check tuple order is OFF).',
            },
            {
              id: 103,
              is_correct: true,
              is_incorrect: false,
              is_partial: false,
              is_unanswered: false,
              score: 10,
              score_perc: 1,
            },
            {
              id: 104,
              is_correct: true,
              is_incorrect: false,
              is_partial: false,
              is_unanswered: false,
              score: 15,
              score_perc: 1,
            },
          ],
        });
        done();
      })
      .catch(function (reason) {
        console.log('Failed:' + reason);
        assert(false);
        done();
      });
  });
});
