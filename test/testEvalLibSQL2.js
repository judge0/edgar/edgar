var should = require('should');
var assert = require('assert');
var lib = require('./../common/libeval');
var testService = require('./../services/testService');

var convertrs = function (arr) {
  var rv = {
    fields: [],
    rows: [],
  };
  var i;
  if (arr.length) {
    for (var property in arr[0]) {
      if (arr[0].hasOwnProperty(property)) {
        rv.fields.push({
          name: property,
        });
      }
    }
    arr.forEach(function (row) {
      var newrow = {};
      for (i = 0; i < rv.fields.length; ++i) {
        newrow['C' + i] = row[rv.fields[i].name];
      }
      rv.rows.push(newrow);
    });
  }
  return rv;
};

describe('Test evaluation functions: SQL questionScore (testEvalLib.js)', function () {
  it('00 - testExecCode', function (done) {
    var userSQL = `SELECT nastavnik.sifNastavnik,imeNastavnik,prezimeNastavnik,oznGrupa
                        FROM nastavnik LEFT OUTER JOIN predmetGrupa
                        ON nastavnik.sifNastavnik=predmetGrupa.sifNastavnik
                        WHERE pBrStanNastavnik=34000 AND (akGodina=2015 OR akGodina IS NULL)`;
    var correctSQL = `select distinct nastavnik.sifnastavnik, imeNastavnik, prezimeNastavnik, ozngrupa
                        from nastavnik left join predmetgrupa
                        on nastavnik.sifnastavnik = predmetgrupa.sifnastavnik
                        and akgodina = 2015
                        where pbrstanNastavnik = 34000`;
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    // execAndGradeSQL: function(testService, id_test_instance, ordinal, userSQL, correctSQL, checkTupleOrder, checkColumnMode, gradingModel) {
    lib
      .execAndGradeSQL(testService, 13, 1, userSQL, correctSQL, false, 4, gradingModel)
      .then(function (result) {
        result.should.eql({
          is_correct: false,
          is_incorrect: true,
          is_unanswered: false,
          is_partial: false,
          score: -5,
          score_perc: -5 / 15,
          hint: 'Uneven row count.', //'Cannot find user row #26 in correct rows (check tuple order is OFF).'
        });
        done();
      })
      .catch(function (err) {
        console.log('error', err);
      });
  });
});
