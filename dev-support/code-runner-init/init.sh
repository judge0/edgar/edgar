#!/bin/bash
set -e

cd ../

JUDGE_0_DIR=$(pwd)/judge0
if [[ ! -d "$JUDGE_0_DIR" ]]; then
    echo "$JUDGE_0_DIR not found on expected place."
    echo "Judge0 (https://judge0.com/) is a hard dependency of code runner. Its setup might take a while."
    read -p "Do you wish to download it? (Y/N): " JUDGE_Y_N
    if [[ $JUDGE_Y_N =~ ^[Yy]$ ]]; then
        echo "Downloading Judge0"
        wget -O judge0.zip https://github.com/judge0/judge0/releases/download/v1.13.0/judge0-v1.13.0.zip

        echo "Unziping Judge0"
        unzip judge0.zip -d tmp-judge
        mkdir judge0
        cp -rp tmp-judge/judge*/* judge0

        echo "Removing Judge0 download file"
        rm -rf judge0.zip
        rm -rf tmp-judge

    else
        echo "Skipping Judge0 download."
    fi
else
    echo "$JUDGE_0_DIR already exists."
fi

if [[ -d "$JUDGE_0_DIR" ]]; then
    read -p "Do you wish to start the judge0 compose? Have in mind that if this system has previous judge0 containers this may fail. In that case remove the containers and retry. (Y/N): " ATTACH_Y_N
    if [[ $ATTACH_Y_N =~ ^[Yy]$ ]]; then
        echo "Starting Judge0 compose files; redis and judge0"
        cd judge0

        docker compose down

        docker compose up -d db redis
        sleep 10s
        docker compose up -d
        sleep 5s

        echo "Judge0 docker container is UP AND READY"
        read -p "Do you wish it to attach Judge0 to edgarnet? (Y/N): " ATTACH_Y_N
        if [[ $ATTACH_Y_N =~ ^[Yy]$ ]]; then
            echo "Attaching server"
            docker network connect --alias j0server edgarnet $(docker container ls | grep judge0-.*server | awk '{print $1}')

            echo "Attaching worker"
            docker network connect edgarnet $(docker container ls | grep judge0-.*worker | awk '{print $1}')
        fi

        cd ..
    fi
fi

CODE_RUNNER_DIR=$(pwd)/code-runner
if [[ ! -d "$CODE_RUNNER_DIR" ]]; then
    echo "Cloning code-runner into $CODE_RUNNER_DIR"
    git clone git@gitlab.com:edgar-group/code-runner.git

    echo "Setting up the config files..."
    cp ./code-runner/config/development-config-docker.js ./code-runner/config/development-config.js
    cp ./code-runner/config/development-config-docker.js ./code-runner/config/production-config.js
else
    echo "$CODE_RUNNER_DIR already exists. No need to clone."
fi

while true; do
    read -p "Do you wish to start code-runner compose? (Y/N): " yn
    case $yn in
    [Yy]*)
        docker compose -f ./code-runner/docker-compose.dev.yml down
        docker compose -f ./code-runner/docker-compose.dev.yml up --detach
        break
        ;;
    [Nn]*) exit ;;
    *) echo "Please answer Y or N." ;;
    esac
done

echo "Currently running containers after core-runner init script"
docker ps

echo 'If you wish to stop them run docker stop $(docker ps -q) and docker rm $(docker ps -q).'
echo "Tip: Add flag -a to docker ps if you want to remove all containers you have on your machine."
