#!/bin/bash
set -e

cd ../

PG_RUNNER_DIR=$(pwd)/pg-runner
if [[ ! -d "$PG_RUNNER_DIR" ]]; then
    echo "Cloning pg-runner into $PG_RUNNER_DIR (might take some time...)"
    git clone git@gitlab.com:edgar-group/pg-runner.git

    echo "Setting up config..."
    cp ./pg-runner/config/development-config-docker.js ./pg-runner/config/development-config.js
    cp ./pg-runner/config/development-config-docker.js ./pg-runner/config/production-config.js
else
    echo "$PG_RUNNER_DIR already exists. No need to clone."
fi

while true; do
    read -p "Do you wish to start pg-runner compose? (Y/N): " yn
    case $yn in
    [Yy]*)
        docker compose -f ./code-runner/docker-compose.dev.yml down
        docker compose -f ./pg-runner/docker-compose.dev.yml up --detach
        break
        ;;
    [Nn]*) exit ;;
    *) echo "Please answer Y or N." ;;
    esac
done

echo "Currently running containers after init script:"
docker ps

echo 'If you wish to stop them run docker stop $(docker ps -q) and docker rm $(docker ps -q).'
echo "Tip: Add flag -a to docker ps if you want to remove all containers you have on your machine."
