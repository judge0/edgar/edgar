FROM postgres:16.1

ENV POSTGRES_DB edgar
ENV POSTGRES_USER postgres

RUN apt update && \
    apt install -y postgresql-16-cron && \
    rm -rf /var/lib/apt/lists/*

COPY ./dockerfiles/db/config/postgresql.conf /etc/postgresql/postgresql.conf
COPY ./db/db-schema/migrations/migration_*.sql /docker-entrypoint-initdb.d/
COPY ./db/db-schema/optional_migrations/optional_migration_*.sql /docker-entrypoint-initdb.d/
COPY ./db/db-schema/seeds/seed_*.sql /docker-entrypoint-initdb.d/

# SQL files are run version by version and for each version the files are run in the following order:
#   1. migration,
#   2. seed, and
#   3. optional_migration.
RUN cd /docker-entrypoint-initdb.d/ && \
    for file in *; do \
        version="$(echo "$file" | sed -e 's/^.*\([0-9]-[0-9]-[0-9][0-9]\).sql$/\1/g')"; \
        case "$file" in \
            migration*) order=0;; \
            seed*) order=1;; \
            optional_migration*) order=2;; \
        esac; \
        mv "$file" "${version}_${order}_${file}"; \
    done

CMD ["postgres", "-c", "config_file=/etc/postgresql/postgresql.conf"]
