# Edgar's Postgres

To build this image use the following command:
```bash
docker build -t edgar-postgres -f dockerfiles/db/Dockerfile .
```

To test if the container is correctly initialized, run the following:
```bash
docker run -it --rm -e POSTGRES_PASSWORD=xxx edgar-postgres
```

Notes for a test run from the above:
1. Environment variable `POSTGRES_PASSWORD` is expected but not used for anything except testing purposes.
