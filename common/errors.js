var util = require('util');

function HTTPError() {
  Error.call(this, arguments);
}

util.inherits(HTTPError, Error);

function NotFound(message) {
  HTTPError.call(this);
  Error.captureStackTrace(this, this /*arguments.callee*/);
  this.statusCode = 404;
  this.message = message;
  this.name = 'NotFound';
}
util.inherits(NotFound, HTTPError);

function DbError(message) {
  HTTPError.call(this);
  Error.captureStackTrace(this, this /*arguments.callee*/);
  this.statusCode = 500;
  this.message = message;
  this.name = 'Database error occured:' + message;
}
util.inherits(DbError, HTTPError);

function DbDataDefError(message) {
  HTTPError.call(this);
  Error.captureStackTrace(this, this /*arguments.callee*/);
  this.statusCode = 500;
  this.message = message;
  this.name = 'Database data definition error occured.';
}
util.inherits(DbDataDefError, HTTPError);

function UnknownRoleSecurityError(message) {
  HTTPError.call(this);
  Error.captureStackTrace(this, this /*arguments.callee*/);
  this.statusCode = 500;
  this.message = message;
  this.name = 'Unknown role.';
}
util.inherits(UnknownRoleSecurityError, HTTPError);

module.exports = {
  HTTPError: HTTPError,
  NotFound: NotFound,
  DbError: DbError,
  DbDataDefError: DbDataDefError,
  UnknownRoleSecurityError: UnknownRoleSecurityError,
};
