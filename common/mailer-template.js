var nodemailer = require('nodemailer');
var winston = require('winston');
var config = require.main.require('./config/config');
var crypto = require('crypto');
// var transporter = nodemailer.createTransport('smtps://:@mail.fer.hr:587');
var transporter = nodemailer.createTransport({
  host: 'some-host', // hostname
  secureConnection: false, // TLS requires secureConnection to be false
  port: 587, // port for secure SMTP
  auth: {
    user: 'user',
    pass: 'pwd',
  },
  tls: {
    ciphers: 'SSLv3',
  },
});
module.exports = {
  sendmail: function (msg) {
    transporter.sendMail(
      {
        to: process.env.ADMIN_EMAIL,
        from: 'edgar@fer.hr',
        subject: 'a2 webapp alert 👥 ✔',
        text: msg,
      },
      function (err, res) {
        if (err) {
          winston.error('Error sending email ' + err);
        } else {
          winston.debug('Email sent: ' + res);
        }
      }
    );
  },
  sendRegisterMail: function (to, first_name, id) {
    var cipher = crypto.createCipher('aes-128-ctr', config.encryptionSecret);
    var token = cipher.update(
      JSON.stringify({
        id: id,
        iat: Date.now(),
      }),
      'utf8',
      'hex'
    );
    token += cipher.final('hex');

    transporter.sendMail(
      {
        to: to,
        from: process.env.ADMIN_EMAIL,
        subject: 'Register at Edgar!',
        html:
          'Hello, ' +
          first_name +
          '<br><br>Follow this ' +
          '<a href="http://' +
          config.endpoint +
          '/register/token/' +
          token +
          '">link</a>' +
          ' to register at Edgar.<br><br>You have two weeks so hurry up!',
      },
      function (err, res) {
        if (err) {
          winston.error('Error sending email ' + err);
        } else {
          winston.info('Email sent: ' + res);
        }
      }
    );
  },
};
