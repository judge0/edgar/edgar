const ROLES = {
  STUDENT: 'Student',
  TEACHER: 'Teacher',
  ADMIN: 'Admin',
  hasRole: function (val) {
    for (var prop in this) {
      if (this.hasOwnProperty(prop) && this[prop] === val) {
        return true;
      }
    }
    return false;
  },
};
const CSV = {
  DELIM: '~~#~~',
};

const globals = {
  ROLES: ROLES,
  CSV: CSV,
  IMG_FOLDER_PLACEHOLDER: '%img_folder_placeholder%',
  TUTORIALS_IMG_FOLDER_PLACEHOLDER: '%tuts_img_folder_placeholder%',
  IMG_FOLDER: '/images/qs',
  TUTORIALS_IMG_FOLDER: '/images/tutorials',
  IMG_FACES_FOLDER: '/images/faces',
  TMP_UPLOAD_FOLDER: 'public/tmp_upload_430_396',
  TI_UPLOAD_FOLDER: 'test_inst',
  PUBLIC_TI_DOWNLOAD_FOLDER: 'download',
  PUBLIC_FOLDER: 'public',
  QUESTIONS_ATTACHMENTS_FOLDER: 'question_attachments',
  QUESTIONS_ATTACHMENTS_ROUTE: 'qattachment',
  MINIO_TMP_UPLOAD_BUCKET_NAME: 'tmpupload',
  getQuestionsAttachmentsFolder: () => {
    if (!global.appRoot) throw new Error('Global NOT set!!');
    return `${global.appRoot}/${globals.PUBLIC_FOLDER}/${globals.QUESTIONS_ATTACHMENTS_FOLDER}/`;
  },
  getQuestionsAttachmentsBucketName: () => {
    return globals.QUESTIONS_ATTACHMENTS_FOLDER.replaceAll('_', '');
  },
  SYMBOL_SPACE: '·',
  SYMBOL_ENTER: '↵',
  SYMBOL_TAB: '⭾',
  SYMBOL_NULL: '∅',
  SYMBOL_NON_PRINTABLE: '☐',
  WARNING_MESSAGE: null,
  APP_VERSION: '0.0.0',
  ALLOWED_ANONYMOUS_LOG_EVENTS: ['Lost focus', 'Exam submitted.'],
};

module.exports = globals;
