var nodemailer = require('nodemailer');
var winston = require('winston');
var db = require('../db').db;
var config = require.main.require('./config/config');
var crypto = require('crypto');

var transporter = nodemailer.createTransport({
  host: config.mail.host,
  secureConnection: config.mail.secureConnection,
  port: config.mail.port,
  auth: {
    user: config.mail.user,
    pass: config.mail.password,
  },
  tls: config.mail.tls,
});

var queue = [];

var _sendmailPromise = function (to, cc, bcc, from, subject, msg) {
  return new Promise((resolve, reject) => {
    console.log(to, cc, bcc);
    console.log('***************************');
    console.log(from, subject, msg);
    transporter.sendMail(
      {
        to: to,
        cc: cc,
        bcc: bcc,
        from: from,
        subject: subject,
        text: msg,
      },
      function (err, res) {
        if (err) {
          winston.error('Error sending email' + err);
          winston.error(
            'Attempted mail is:' +
              JSON.stringify({
                to,
                cc,
                bcc,
                from,
                subject,
                msg,
              })
          );
          winston.error('Config is:' + JSON.stringify(config.mail));

          reject(err);
        } else {
          winston.debug('Email sent (subject = ' + subject + '): ' + JSON.stringify(res));
          resolve();
        }
      }
    );
  });
};

var popQueue = function (depth) {
  if (queue.length) {
    winston.debug('Popping queue at depth ' + depth);
    var m = queue[0];
    _sendmailPromise(m.mail_to, m.mail_cc, m.mail_bcc, m.mail_from, m.mail_subject, m.mail_text)
      .then(function () {
        return db.result(`DELETE FROM email_queue WHERE id = $(id)`, {
          id: m.id,
        });
      })
      .then(function (result) {
        winston.debug('DELETE FROM email_queue returned: ' + result);
        queue.shift();
        var lb = Math.min(config.mail.waitBetweenMailsMinMillis, 500);
        var waitMillis = lb + Math.random() * Math.abs(config.mail.waitBetweenMailsMaxMillis - lb);
        winston.debug(
          `Waiting between ${config.mail.waitBetweenMailsMinMillis} and ${config.mail.waitBetweenMailsMaxMillis}: ${waitMillis} milliseconds...`
        );
        setTimeout(() => {
          popQueue(depth + 1);
        }, waitMillis);
      })
      .catch(function (error) {
        winston.error('An error occured sending mail id: ' + m.id);
        winston.error(error);
        process.exit();
      });
  } else {
    winston.debug('** Done popping email queue.');
  }
};

module.exports = {
  sendmail: function (msg) {
    _sendmailPromise(
      process.env.ADMIN_EMAIL,
      '',
      '',
      config.mail.user,
      '⚠⚠⚠ Edgar WebApp ALERT ⚠⚠⚠',
      msg
    )
      .then()
      .catch(function (error) {
        winston.error('Failed to send email:' + error);
      });
  },
  processQueue: function () {
    winston.debug(`Postman checks his bag...`);
    if (queue.length) {
      winston.debug(`Queue(${queue.length}) not empty, going back to sleep...`);
      return;
    }
    db.func('email_process_queue', [config.mail.from, config.mail.batchSize])
      .then(function (data) {
        winston.debug('email_process_queue returned: ' + data);
        return db.any(`SELECT *
                            FROM email_queue
                        ORDER BY id
                        `);
      })
      .then(function (mails) {
        if (mails) {
          winston.debug('Mails in the queue: ' + mails.length);
          queue = mails;
          popQueue(1);
        }
      })
      .catch(function (error) {
        winston.error('An error occured in processQueue mailer deamon!');
        winston.error(error);
      });
  },
  sendRegisterMail: function (user_modified, to, first_name, id, req) {
    var cipher = crypto.createCipher('aes-128-ctr', config.register.hashEncryptionSecret);
    var token = cipher.update(
      JSON.stringify({
        id: id,
        iat: Date.now(),
      }),
      'utf8',
      'hex'
    );
    token += cipher.final('hex');

    db.none(
      `INSERT INTO email_queue (user_modified, mail_to, mail_cc, mail_bcc, mail_from,
                                          mail_subject, mail_text)
                 VALUES ($(user_modified), $(mail_to), '', '', $(mail_from), 'Register at Edgar!',
                 'Hello, ` +
        first_name +
        `!

Use the following link to register at Edgar: ${config.proto || 'http'}://` +
        req.get('host') +
        `/register/token/` +
        token +
        `

You have two weeks so hurry up!')`,
      {
        mail_to: to,
        mail_from: config.mail.from,
        first_name: first_name,
        token: token,
        user_modified: user_modified,
      }
    )
      .then(() => {
        winston.debug('Register mail for ' + to + ' queued');
      })
      .catch((error) => {
        winston.error('Error queueing register mail for ' + to);
      });
  },
};
