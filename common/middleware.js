'use strict';
var express = require('express');
var winston = require('winston');
var globals = require.main.require('./common/globals');

var MiddleWare = {
  requireRole: function (role) {
    return MiddleWare.requireRoles([role]);
  },

  requireRoles: function (roles) {
    return function (req, res, next) {
      // winston.info('requireRoles: ', roles.join());
      if (req.session.rolename === globals.ROLES.ADMIN) {
        next();
      } else if (roles && roles.length && roles.indexOf(req.session.rolename) >= 0) {
        next();
      } else {
        res.send(403);
      }
    };
  },
};

module.exports = MiddleWare;
