import {
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnInit,
} from '@angular/core';
import { LogService } from '../../../../common/services/log.service';
import { LogEvent } from '../../../../common/models/log-event';
import { Question } from 'projects/common/models/question';
import { environment } from 'projects/config/environments/environment';
import { HttpClient } from '@angular/common/http';
import { firstValueFrom } from 'rxjs';

@Component({
  selector: 'file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css'],
})
export class FileUploadComponent implements OnInit, OnChanges {
  static readonly RUNNING_STRING = '..RUNNING_STRING..';
  static readonly SELECT_FILE_STRING = 'Select file and click upload ...';

  @Input() currQuestion!: Question;
  public _selectedFiles!: Array<string | null>;
  public _selectedFileNames!: Array<string>;
  constructor(
    private _logService: LogService,
    private _http: HttpClient,
    private _ref: ChangeDetectorRef
  ) {
    // console.log('FILE upload constructor', this.currQuestion);
  }

  ngOnInit(): void {}

  ngOnChanges() {
    this._selectedFiles = Array(this.currQuestion.getUploadsCount())
      .fill(0)
      .map((x, i) => null);
    this._selectedFileNames = Array(this.currQuestion.getUploadsCount())
      .fill(0)
      .map((x, i) => FileUploadComponent.SELECT_FILE_STRING);
  }
  fileSelected(idx: number) {
    let file = this._selectedFiles[idx];
    if (file) file = file.split('\\').pop() || null;
    this._selectedFileNames = this._selectedFileNames.map((x, i) =>
      i == idx ? file || '?' : x
    );
  }
  async deleteUploadedFile(idx: number) {
    let toLogDeletedFileName = this.currQuestion.tempUploadedFileNames![idx];
    this.currQuestion.tempUploadedFileNames![idx] =
      FileUploadComponent.RUNNING_STRING;
    try {
      this.currQuestion.uploaded![idx] = false;
      const resp: any = await firstValueFrom(
        this._http.post(
          `${environment.devWebApiUrl ?? ''}/exam/run/file/delete/${
            this.currQuestion.ordinal
          }/${idx}`,
          {},
          {
            withCredentials: true,
          }
        )
      );
      // console.log('DELETE ', resp);
      this.currQuestion.tempUploadedFileNames![idx] = '';
      this.currQuestion.uploaded![idx] = false;
      this._ref.markForCheck();
      this._logService.log(
        new LogEvent('Deleted uploaded file', toLogDeletedFileName)
      );
    } catch (error) {
      this.currQuestion.tempUploadedFileNames![idx] =
        'Error occurred when trying to delete your file:' + error;
      this._logService.log(
        new LogEvent(
          'Failed deleting uploaded file' + error,
          toLogDeletedFileName
        )
      );
    }
  }

  uploadButtonDisabled(idx: number) {
    let elem: any = document.getElementById(
      'fUpload' + this.currQuestion.ordinal + idx
    );
    return '' == elem.value;
  }

  async uploadFile(idx: number) {
    var form = document.forms.namedItem(
      'fileinfo' + this.currQuestion.ordinal + idx
    );
    var formData = new FormData(form!);

    this.currQuestion.tempUploadedFileNames![idx] =
      FileUploadComponent.RUNNING_STRING;
    try {
      this.currQuestion.uploaded![idx] = false;
      const resp: any = await firstValueFrom(
        this._http.post(
          `${environment.devWebApiUrl ?? ''}/exam/run/file/upload/${
            this.currQuestion.ordinal
          }/${idx}`,
          formData,
          {
            withCredentials: true,
          }
        )
      );
      // console.log('UPLAD ', resp);
      this.currQuestion.tempUploadedFileNames![idx] =
        resp.data && resp.data.originalname;
      this.currQuestion.uploaded![idx] = true;
      this._ref.markForCheck();
      this._logService.log(
        new LogEvent(
          'Uploaded file',
          this.currQuestion.tempUploadedFileNames![idx]
        )
      );
    } catch (error) {
      this.currQuestion.tempUploadedFileNames![idx] =
        'Error occurred when trying to upload your file: ' +
        JSON.stringify(error);
      this._logService.log(
        new LogEvent(
          'Error uploading file',
          this.currQuestion.tempUploadedFileNames![idx] +
            ' Error: ' +
            JSON.stringify(error)
        )
      );
    }
    this._selectedFileNames = this._selectedFileNames.map((x, i) => {
      return i == idx ? 'Select file' : x;
    });
  }
}
