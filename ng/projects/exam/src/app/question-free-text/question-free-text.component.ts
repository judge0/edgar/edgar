import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
} from '@angular/core';
import { Answer } from 'projects/common/models/answer';
import { Question } from 'projects/common/models/question';

@Component({
  selector: 'question-free-text',
  templateUrl: './question-free-text.component.html',
  styleUrls: ['./question-free-text.component.css'],
})
export class QuestionFreeTextComponent implements OnChanges {
  saved: boolean;
  @Input()
  currQuestion!: Question;
  @Input()
  currAnswer!: Answer;
  @Input()
  cmSkin!: string;

  @Output() answerUpdated = new EventEmitter();
  @Output() answerSaved = new EventEmitter();
  @Output() codeRun = new EventEmitter();

  constructor(
    private _ref: ChangeDetectorRef,
  ) {
    this.saved = true;
  }

  checkSetDefaultAnswer() {
    if (!this.currAnswer.textAnswer) {
      this.currAnswer.textAnswer = '\n\n\n\n\n';
    }
  }
  ngOnChanges(): void {
    this.saved = false;
    this.checkSetDefaultAnswer();
  }

  save() {
    // Will always save the entire contents, not just the selected part (that might come through the $event, ie runHotKey event)
    this.answerSaved.emit(); // this.currAnswer
    console.log('Lying here, this should be async....');
    this.saved = true;
  }

  onTextChanged($event: string) {
    this.saved = false;
    this.currAnswer.textAnswer = $event;
    this.answerUpdated.emit(this.currAnswer);
  }

  getCurrText() {
    return this.currAnswer.textAnswer;
  }


}
