import {
  Component,
  ChangeDetectorRef,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
} from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Question } from 'projects/common/models/question';
import { ExamService } from 'projects/exam/src/app/exam.service';
import { Exam } from 'projects/models/exam/exam';

@Component({
  selector: 'question-navigation',
  templateUrl: './question-navigation.component.html',
  styleUrls: ['./question-navigation.component.css'],
})
export class QuestionNavigationComponent implements OnInit {
  //private _idSelected: number = 0;
  private FW_ONLY_WAIT_SEC: number = 2; // TODO 20
  public waitSec: number;
  public currentOrdinal: number = 0;
  public exam: Exam;


  constructor(
    private _examService: ExamService,
    private _router: Router // private _ref: ChangeDetectorRef
  ) {
    this.waitSec = 0;
    this.exam = this._examService.getExam();
  }
  navigateTo(ordinal: number) {
    this._router.navigate([`/${ordinal}`]);
  }
  ngOnInit(): void {
    this._examService.currentQuestionObservable.subscribe((q) => {
      // console.log('QN ordinal changed (subsc)');
      this.currentOrdinal = q.ordinal;
    });

    this.currentOrdinal = this._examService.getCurrentQuestion().ordinal;

  }

  isSelected(idx: number) {
    return idx === this.currentOrdinal - 1;
  }

  isLast() {
    return this.currentOrdinal === this.exam.noOfQuestions; // this.currAnswers.length === 1 + this._idSelected;
  }

  next() {
    this.exam.last_ordinal++;
    if (!this.isLast()) {
      this.waitSec = this.FW_ONLY_WAIT_SEC;
      this.countDown();
    }
    this._router.navigate([this.exam.last_ordinal], {});
  }

  countDown() {
    // this._ref.markForCheck();
    if (this.waitSec > 0) {
      setTimeout(() => {
        this.waitSec--;
        this.countDown();
      }, 1000);
    }
  }
}
