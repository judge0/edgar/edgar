import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ScaleItem } from '../../../../common/models/scale-item';
import { Answer } from 'projects/common/models/answer';
import { Question } from 'projects/common/models/question';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'scale-item',
  templateUrl: './scale-items.component.html',
  styleUrls: ['./scale-items.component.css'],
})
export class ScaleItemsComponent implements OnInit {
  @Input()
  currQuestion!: Question;
  @Input()
  currAnswer!: Answer;
  @Output() scaleItemSelectedEmitter: EventEmitter<ScaleItem> =
    new EventEmitter();

  constructor(public sanitizer: DomSanitizer) {}

  ngOnInit(): void {}

  isSelectedScaleItem(si: ScaleItem) {
    //console.log("?isSelectd? SCALE ITEM", this.currQuestion, this.currAnswer);
    return this.currAnswer.multichoice.indexOf(si.value) >= 0;
  }

  onSelectScaleItem(si: ScaleItem) {
    this.scaleItemSelectedEmitter.emit(si);
  }
}
