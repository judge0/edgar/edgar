import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Answer } from 'projects/common/models/answer';
import { Question } from 'projects/common/models/question';

@Component({
  selector: 'app-ordered-elements',
  templateUrl: './question-ordered-elements.component.html',
  styleUrls: ['./question-ordered-elements.component.css'],
})
export class OrderedElementsComponent implements OnChanges {
  @Input() currQuestion!: Question;
  @Input() currAnswer!: Answer;

  @Output() answerUpdated = new EventEmitter();
  @Output() answerSaved = new EventEmitter();

  public saved: boolean;

  constructor(
    protected sanitizer: DomSanitizer,
    private _ref: ChangeDetectorRef,
  ) {
    this.saved = true;
  }


  ngOnChanges(changes: SimpleChanges): void {
    this.saved = false;
  }

  maybeInitMultichoice() {
    if (
      !this.currAnswer.multichoice ||
      !this.currAnswer.multichoice.length ||
      this.currAnswer.multichoice[0] == null
    ) {
      this.currAnswer.multichoice = [
        ...Array(this.currQuestion.answers.length).keys(),
      ].map((i) => i + 1); // init with ordinals 1, 2, 3, ... length
    }
  }
  drop(event: CdkDragDrop<string[]>) {
    this.maybeInitMultichoice();
    this.saved = false;
    moveItemInArray(
      this.currAnswer.multichoice,
      event.previousIndex,
      event.currentIndex
    );
    // this.changeOrdinals();
    this.answerUpdated.emit(this.currAnswer);
  }
  getElements(): Array<any> {
    if (
      this.currAnswer.multichoice &&
      this.currAnswer.multichoice.length &&
      this.currAnswer.multichoice[0] != null
    ) {
      return this.currAnswer.multichoice.map((ordinal: number) => {
        return this.currQuestion.answers[ordinal - 1];
      });
    } else {
      return this.currQuestion.answers;
    }
  }

  save() {
    this.maybeInitMultichoice();
    this.answerSaved.emit();
    this.saved = true;
  }

  reset() {
    this.currAnswer.multichoice = [];
    this.answerUpdated.emit(this.currAnswer);
    this.answerSaved.emit();
  }
}
