import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import {
  CdkDragDrop,
  CdkDragEnd,
  CdkDragStart,
} from '@angular/cdk/drag-drop';
import { Answer } from 'projects/common/models/answer';
import { Question } from 'projects/common/models/question';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-connected-elements',
  templateUrl: './connected-elements.component.html',
  styleUrls: ['./connected-elements.component.css'],
})
export class ConnectedElementsComponent implements OnInit {
  @Input() currQuestion!: Question;
  @Input() currAnswer!: Answer;

  @Output() answerUpdated = new EventEmitter();
  @Output() answerSaved = new EventEmitter();

  public saved: boolean;
  public ifAnswerExist: boolean;
  lastEnteredDropList: any = null;
  currentlyDraggedItem: any = null;

  constructor(
    protected sanitizer: DomSanitizer,
    private _ref: ChangeDetectorRef,
  ) {
    this.saved = true;
    this.ifAnswerExist = false;
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.saved = false;
  }


  onCdkDragStarted(event: CdkDragStart) {
    this.currentlyDraggedItem = event.source.data;
  }

  onCdkDragEnded(event: CdkDragEnd) {
    this.currentlyDraggedItem = null;
  }

  getDynamicAnswers() {
    return this.currQuestion.answersDynamic.filter(
      (item) => !this.currAnswer.multichoice.includes(item.ordinal)
    );
  }

  drop(event: CdkDragDrop<string[]>, ordinal?: number) {
    // console.log('drop', event, ordinal);
    if (event.previousContainer !== event.container) {
      if (event.container.id === 'dynamicList') {
        // when we return an item to dynamic list
        for (let i = 0; i < this.currAnswer.multichoice.length; i++) {
          if (this.currAnswer.multichoice[i] === event.item.data) {
            this.currAnswer.multichoice[i] = -1;
          }
        }
      } else if (ordinal !== undefined) {
        if (this.currAnswer.multichoice[ordinal - 1]) {
          this.currAnswer.multichoice[ordinal - 1] = -1;
        }
        let someWhereElseIndex = this.currAnswer.multichoice.findIndex(
          (ord) => ord === event.item.data
        );
        if (someWhereElseIndex !== -1) {
          this.currAnswer.multichoice[someWhereElseIndex] = -1;
        }
        this.currAnswer.multichoice[ordinal - 1] = event.item.data;
      }
    }

    if (this.currAnswer.multichoice.find((item) => item > 0) === undefined) {
      this.currAnswer.multichoice = [];
    }
    this.answerUpdated.emit(this.currAnswer);
    this.saved = false;
  }

  reset() {
    this.currAnswer.multichoice = [];
    this.answerUpdated.emit(this.currAnswer);
    this.answerSaved.emit();
  }
  save() {
    this.answerSaved.emit();
    this.saved = true;
  }
}
