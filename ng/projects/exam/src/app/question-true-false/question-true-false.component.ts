import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Answer } from 'projects/common/models/answer';
import { Question } from 'projects/common/models/question';

@Component({
  selector: 'app-question-true-false',
  templateUrl: './question-true-false.component.html',
  styleUrls: ['./question-true-false.component.css'],
})
export class QuestionTrueFalseComponent  {
  @Input() currQuestion!: Question;
  @Input() currAnswer!: Answer;

  @Output() answerUpdated = new EventEmitter();
  @Output() answerSaved = new EventEmitter();

  constructor(
    protected sanitizer: DomSanitizer,
    private _ref: ChangeDetectorRef,
  ) {}

  isSelected(answer: any) {
    return (
      this.currQuestion &&
      this.currAnswer.multichoice.indexOf(answer.ordinal) >= 0
    );
  }
  getButtonClass(answer: any) {
    let content = answer.toUpperCase();
    if (
      content.indexOf('FALSE') >= 0 ||
      content.indexOf('NETOČNO') >= 0 ||
      content.indexOf('INCORRECT') >= 0
    ) {
      return 'btn-danger';
    } else if (
      content.indexOf('TRUE') >= 0 ||
      content.indexOf('TOČNO') >= 0 ||
      content.indexOf('CORRECT') >= 0
    ) {
      return 'btn-success';
    } else {
      return 'btn-warning';
    }
  }
  onSelect(answer: any) {
    // TODO igor
    if (this.currQuestion) {
      var arrAnswers = this.currAnswer.multichoice;
      if (this.currQuestion.multiCorrect) {
        var idx;
        if ((idx = arrAnswers.indexOf(answer.ordinal)) >= 0) {
          arrAnswers.splice(idx, 1);
        } else {
          arrAnswers.push(answer.ordinal);
        }
      } else {
        if (arrAnswers.indexOf(answer.ordinal) >= 0) {
          arrAnswers = [];
        } else {
          arrAnswers = [answer.ordinal];
        }
      }
      this.currAnswer.multichoice = arrAnswers;

      this.answerUpdated.emit(this.currAnswer);
      this.answerSaved.emit(); // TODO this is async but there is no visual cue to the saving...
    }
  }

}
