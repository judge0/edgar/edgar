import {
  Component,
  Input,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  SimpleChanges,
} from '@angular/core';
import { Question } from 'projects/common/models/question';
import { DomSanitizer } from '@angular/platform-browser';
import { ExamService } from '../exam.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'question-content-static',
  templateUrl: './question-content-static.component.html',
  styleUrls: ['./question-content-static.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuestionContentStaticComponent {
  @Input()
  currQuestion!: Question;
  @Input() seconds: number = 0;

  public showCurrQuestion: boolean = false;
  private timerSubscription: Subscription | null = null;

  constructor(
    protected sanitizer: DomSanitizer,
    public examService: ExamService,
    private _ref: ChangeDetectorRef,
  ) {}

}
