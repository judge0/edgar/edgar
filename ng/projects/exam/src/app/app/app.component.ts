import {
  Component,
  HostListener,
  OnInit,
  ChangeDetectorRef,
} from '@angular/core';
import { LogService } from '../../../../common/services/log.service';
import { ExamService } from '../exam.service';
import { LogEvent } from '../../../../common/models/log-event';
import { environment } from 'projects/config/environments/environment';
import * as $ from 'jquery';
import {
  CODERUNNER_SERVICE_CONFIG,
  EXAM_CODERUNNER_SERVICE_CONFIG,
} from '../../../../config/code-runner.config';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  Params,
  Router,
} from '@angular/router';

@Component({
  selector: 'app',
  host: {
    '(window:focus)': 'onFocus()',
    '(window:blur)': 'onBlur()',
  },
  templateUrl: `app.component.html`,
  styleUrls: ['./app.component.css'],
  providers: [
    {
      provide: CODERUNNER_SERVICE_CONFIG,
      useValue: EXAM_CODERUNNER_SERVICE_CONFIG,
    },
  ],
})
export class AppComponent implements OnInit {
  _intialQuestionLoaded: boolean = false;
  _examLoaded: boolean = false;

  constructor(
    private _examService: ExamService,
    private _logService: LogService,
    private _router: Router,
    private _route: ActivatedRoute
  ) {}
  collectRouteParams(router: Router): any {
    let params = {};
    let stack: ActivatedRouteSnapshot[] = [router.routerState.snapshot.root];
    while (stack.length > 0) {
      const route = stack.pop()!;
      params = { ...params, ...route.params };
      stack.push(...route.children);
    }
    return params;
  }
  public async ngOnInit() {
    if (!environment.production && location.port == '4200') {
      console.log('Dev mode -> dummy login to last started exam!');
      await this._examService.devlogin();
    }
    let exam = await this._examService.loadExam();
    if (exam == null) {
      alert('exam is null!?');
    } else {
      this._examLoaded = true;
      if (this._examService.getExam().isPA2()) {
        await this._examService.loadAllQuestions();
        this._router.navigate(['/pa'], {
          relativeTo: this._route,
        });
      } else {
        let routeParams = this.collectRouteParams(this._router);
        let initialOrdinal = exam.forward_only
          ? exam.last_ordinal
          : routeParams.id || 1;

        if (
          +initialOrdinal > +this._examService.getExam().noOfQuestions ||
          +initialOrdinal < 1
        ) {
          console.log('initialOrdinal overflow, back to 1.', initialOrdinal);
          initialOrdinal = 1;
        }
        await this._examService.setCurrentQuestionOrdinal(initialOrdinal);
        this._router.navigate([initialOrdinal], {
          relativeTo: this._route,
        });
      }
      this._intialQuestionLoaded = true;
    }
    setInterval(() => {
      this._logService.hb();
    }, 60 * 1000);
    this._logService.log(
      new LogEvent(
        'Application constructed.',
        JSON.stringify(this.getClientInfo())
      )
    );
  }
  private getClientInfo() {
    try {
      return {
        timeOpened: new Date(),
        timezone: new Date().getTimezoneOffset() / 60,
        platform: navigator?.platform || 'unknown',
        pageon: window.location.pathname,
        referrer: document.referrer,
        browserName: navigator.appName,
        browserEngine: navigator.product,
        browserVersion1a: navigator.appVersion,
        browserVersion1b: navigator.userAgent,
        browserLanguage: navigator.language,
        browserOnline: navigator.onLine,
        browserPlatform: navigator.platform,
        sizeScreenW: screen.width,
        sizeScreenH: screen.height,
        sizeInW: innerWidth,
        sizeInH: innerHeight,
        sizeAvailW: screen.availWidth,
        sizeAvailH: screen.availHeight,
        scrColorDepth: screen.colorDepth,
        scrPixelDepth: screen.pixelDepth,
      };
    } catch (err) {
      return {
        warning: 'Error getting user info',
      };
    }
  }

  // //event from header
  // onShowScoreboard() {
  //   this._showScoreboard = !this._showScoreboard;
  // }

  onFocus() {
    this._logService.log(new LogEvent('Got focus', '-'));
  }

  onBlur() {
    this._logService.log(
      new LogEvent(
        'Lost focus',
        'Warning - student could be cheating (alt+tab)?'
      )
    );
  }
}
