import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { ExamComponent } from './exam/exam.component';
import { SubmitComponent } from './submit/submit.component';
import { SharedModule } from '../../../common/components/shared.module';
import { QuestionComponentsModule } from './question-components.module';
import { RequestInterceptor } from 'projects/common/interceptors/request.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {
  EXAM_LOG_SERVICE_CONFIG,
  LOG_SERVICE_CONFIG,
} from 'projects/config/log.config';
import {
  DEFAULT_TICKET_SERVICE_CONFIG,
  TICKET_SERVICE_CONFIG,
} from 'projects/config/ticket.config';
import { AppRoutingModule } from './app/app-routing.module';
import { AppComponent } from './app/app.component';
// import { QuestionNavigationComponent } from 'projects/exam/src/app/question-navigation/question-navigation.component';
import { ExamService } from './exam.service';
import {
  DEFAULT_EXAM_CONFIG,
  EXAM_SERVICE_CONFIG,
} from './exam.service.config';
import { LogService } from 'projects/common/services/log.service';
import { I_EXAM_SERVICE_TOKEN } from 'projects/common/models/IExamService';
import { BaseExamService } from './base.exam.service';
import {
  CODERUNNER_SERVICE_CONFIG,
  EXAM_CODERUNNER_SERVICE_CONFIG,
} from 'projects/config/code-runner.config';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  bootstrap: [AppComponent],
  imports: [
    // CommonModule, already in shared module
    SharedModule,
    QuestionComponentsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
  ],
  declarations: [AppComponent, SubmitComponent, ExamComponent],
  providers: [
    ExamService,
    {
      provide: I_EXAM_SERVICE_TOKEN,
      useClass: BaseExamService,
      multi: false,
    },
    LogService,
    { provide: EXAM_SERVICE_CONFIG, useValue: DEFAULT_EXAM_CONFIG },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi: true,
    },
    { provide: LOG_SERVICE_CONFIG, useValue: EXAM_LOG_SERVICE_CONFIG },
    { provide: TICKET_SERVICE_CONFIG, useValue: DEFAULT_TICKET_SERVICE_CONFIG },
    {
      provide: CODERUNNER_SERVICE_CONFIG,
      useValue: EXAM_CODERUNNER_SERVICE_CONFIG,
    },
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}
