export class TutorialStepHint {
  constructor(public ordinal: number, public html: string) {}
}

export class TutorialAnswerHint {}

export class AbcHint extends TutorialAnswerHint {
  constructor(public id_question_answer: number, public html: string) {
    super();
  }
}

export class CodeHint extends TutorialAnswerHint {
  constructor(
    public ordinal: number,
    public trigger: string,
    public html: string
  ) {
    super();
  }
}
