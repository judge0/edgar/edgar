import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app/app.component';
import { LogService } from 'projects/common/services/log.service';
import {
  TUTORIAL_LOG_SERVICE_CONFIG,
  LOG_SERVICE_CONFIG,
} from 'projects/config/log.config';
import {
  CODERUNNER_SERVICE_CONFIG,
  TUTORIAL_CODERUNNER_SERVICE_CONFIG,
} from '../../config/code-runner.config';

import { TutorialService } from './tutorial.service';
import {
  DEFAULT_TUTORIAL_SERVICE_CONFIG,
  TUTORIAL_SERVICE_CONFIG,
} from './tutorial.service.config';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './app/header/header.component';
import { TutorialAbcComponent } from './app/tutorial-abc/tutorial-abc.component';
import { TutorialComponent } from './app/tutorial/tutorial.component';
import { CodeRunnerComponent } from './code-runner/code-runner.component';
import { SharedModule } from 'projects/common/components/shared.module';
import {
  TICKET_SERVICE_CONFIG,
  TUTORIAL_TICKET_SERVICE_CONFIG,
} from 'projects/config/ticket.config';
import { TicketService } from 'projects/common/services/ticket.service';

const COMPONENTS = [
  AppComponent,
  HeaderComponent,
  TutorialAbcComponent,
  TutorialComponent,
  CodeRunnerComponent,
  // CodeMirrorComponent,
  // CodePlaygroundComponent,
  // QuestionCodeComponent,
  // QuestionAnswerPipe,
  // SqlRunLabelPipe,
];

@NgModule({
  declarations: [COMPONENTS],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule,
  ],
  providers: [
    TutorialService,
    {
      provide: TUTORIAL_SERVICE_CONFIG,
      useValue: DEFAULT_TUTORIAL_SERVICE_CONFIG,
    },
    {
      provide: CODERUNNER_SERVICE_CONFIG,
      useValue: TUTORIAL_CODERUNNER_SERVICE_CONFIG,
    },
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: TutorialRequestInterceptor,
    //   multi: true,
    // },
    LogService,
    { provide: LOG_SERVICE_CONFIG, useValue: TUTORIAL_LOG_SERVICE_CONFIG },
    TicketService,
    {
      provide: TICKET_SERVICE_CONFIG,
      useValue: TUTORIAL_TICKET_SERVICE_CONFIG,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
