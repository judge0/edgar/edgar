export class ExamReview {
  constructor(
    public idTestInstance: number,
    public title: string,
    public questionsNo: number,
    public secondsLeft: number,
    public prolonged: boolean,
    public correctNo: number,
    public incorrectNo: number,
    public unansweredNo: number,
    public partialNo: number,
    public score: any,
    public scorePerc: any,
    public passed: boolean,
    public answersOutcome: string[],
    public student: string,
    public altId: string,
    public altId2: string,
    public showSolutions: boolean,
    public cmSkin: string,
    public imgSrc: string,
    public typeName: string,
    public noReviews: number,
    public noReviewQuestions: number,
    public usesTicketing: boolean
  ) {}

  public isPA1(): boolean {
    return this.typeName === 'Peer assessment PHASE1';
  }
  public isPA2(): boolean {
    return this.typeName === 'Peer assessment PHASE2';
  }
}
