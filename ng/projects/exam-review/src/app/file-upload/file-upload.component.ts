import { Component, Input, OnInit } from '@angular/core';
import { QuestionReview } from '../../models/QuestionReview';

@Component({
  selector: 'file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css'],
})
export class FileUploadComponent {
  @Input() currQuestion!: QuestionReview;

  constructor() {}
}
