import {
  Component,
  Input,
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { QuestionReview } from '../../models/QuestionReview';

@Component({
  selector: 'question-review-connected-elements',
  templateUrl: './question-review-connected-elements.component.html',
  styleUrls: ['./question-review-connected-elements.component.css'],
})
export class QuestionReviewConnectedElementsComponent  {
  @Input() currQuestion!: QuestionReview;

  constructor(protected sanitizer: DomSanitizer) {
  }

}
