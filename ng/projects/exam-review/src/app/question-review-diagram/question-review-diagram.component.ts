import {
  Component,
  ElementRef,
  Input,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { QuestionReview } from '../../models/QuestionReview';
import { DiagramService } from 'projects/common/services/diagram.service';

@Component({
  selector: 'question-review-diagram',
  templateUrl: './question-review-diagram.component.html',
  styleUrls: ['./question-review-diagram.component.css'],
})
export class QuestionReviewDiagramComponent {
  @Input() currQuestion!: QuestionReview;
  @Input() cmSkin!: string;
  @ViewChild('mermaidOutputStudent')
  mermaidOutputStudentRef!: ElementRef<HTMLDivElement>;
  @ViewChild('mermaidOutputCorrect')
  mermaidOutputCorrectRef!: ElementRef<HTMLDivElement>;

  constructor(
    protected sanitizer: DomSanitizer,
    private diagramService: DiagramService
  ) {}
  ngAfterViewInit(): void {
    this.renderMermaid();
  }

  async renderMermaid(): Promise<void> {
    this.diagramService.renderToNativeElement(
      this.currQuestion.studentAnswerText,
      this.mermaidOutputStudentRef.nativeElement
    );

    if (
      this.currQuestion.correctAnswerDiagram &&
      this.currQuestion.correctAnswerDiagram.trim() !== ''
    ) {
      this.diagramService.renderToNativeElement(
        this.currQuestion.correctAnswerDiagram,
        this.mermaidOutputCorrectRef.nativeElement
      );
    }
  }
}
