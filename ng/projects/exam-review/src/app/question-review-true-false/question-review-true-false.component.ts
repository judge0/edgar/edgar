import {
  Component,
  Input,
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { QuestionReview } from '../../models/QuestionReview';

@Component({
  selector: 'question-review-true-false',
  templateUrl: './question-review-true-false.component.html',
  styleUrls: ['./question-review-true-false.component.css'],
})
export class QuestionReviewTrueFalseComponent {
  @Input() currQuestion!: QuestionReview;

  constructor(protected sanitizer: DomSanitizer) {}

  isSelected(answer) {
    return this.currQuestion.studentAnswers.indexOf(answer.ordinal) >= 0;
  }

  isCorrect(answer) {
    return this.currQuestion.correctAnswers.indexOf(answer.ordinal) >= 0;
  }

  getButtonClass(answer: any) {
    let content = answer.toUpperCase();
    if (content.indexOf('TRUE') >= 0 || content.indexOf('TOČNO') >= 0) {
      return 'btn-success';
    } else if (
      content.indexOf('FALSE') >= 0 ||
      content.indexOf('Netočno') >= 0
    ) {
      return 'btn-danger';
    } else {
      return 'btn-warning';
    }
  }
}
