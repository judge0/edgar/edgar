import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { QuestionReview } from '../../models/QuestionReview';

@Component({
  selector: 'question-multichoice',
  templateUrl: './question-multichoice.component.html',
  styleUrls: ['./question-multichoice.component.css'],
})
export class QuestionMultichoiceComponent implements OnInit {
  @Input() currQuestion!: QuestionReview;

  constructor(public sanitizer: DomSanitizer) {}

  ngOnInit(): void {}

  isSelected(answer) {
    return this.currQuestion.studentAnswers.indexOf(answer.ordinal) >= 0;
  }

  isCorrect(answer) {
    return this.currQuestion.correctAnswers.indexOf(answer.ordinal) >= 0;
  }
}
