import { Component, Input, OnInit } from '@angular/core';
import { QuestionReview } from '../../models/QuestionReview';

@Component({
  selector: 'free-text',
  templateUrl: './free-text.component.html',
  styleUrls: ['./free-text.component.css'],
})
export class FreeTextComponent implements OnInit {
  @Input() cmSkin!: string;
  @Input() currQuestion!: QuestionReview;

  constructor() {}

  ngOnInit(): void {
    // console.log(this.currQuestion);
  }
}
