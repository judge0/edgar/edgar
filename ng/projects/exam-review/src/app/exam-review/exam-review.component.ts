import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ExamReviewService } from '../../exam.review.service';
import { QuestionReview } from '../../models/QuestionReview';

@Component({
  selector: 'app-exam-review',
  templateUrl: './exam-review.component.html',
  styleUrls: ['./exam-review.component.css'],
})
export class ExamReviewComponent implements OnInit {
  _examLoaded: boolean = false;
  _currQuestion!: QuestionReview;

  constructor(
    private _examReviewService: ExamReviewService,
    private _route: ActivatedRoute
  ) {}

  public async ngOnInit() {
    this._examReviewService.currentQuestionObservable.subscribe(
      (currQuestion) => {
        this._currQuestion = currQuestion;
      }
    );

    this._route.params.subscribe(async (params: Params) => {
      let ordinal = +params['id'];
      if (ordinal > 0) {
        this._examLoaded = true;
        this._examReviewService.setCurrentQuestionOrdinal(ordinal);
      }
    });
  }
}
