import { Component, OnInit } from '@angular/core';
import { ExamReviewService } from '../../exam.review.service';
import { QuestionReview } from '../../models/QuestionReview';

@Component({
  selector: 'pa1-exam-review',
  templateUrl: './pa1-exam-review.component.html',
  styleUrls: ['./pa1-exam-review.component.css'],
})
export class Pa1ExamReviewComponent implements OnInit {
  _examLoaded: boolean = false;
  _PA1Questions: QuestionReview[][] = [];
  _reviewLabels: string[] = [];
  constructor(private _examReviewService: ExamReviewService) {}

  public async ngOnInit() {
    this._examLoaded = true;
    this._PA1Questions = [];
    this._reviewLabels = [];
    for (let i = 0; i < this._examReviewService.getExam().noReviews; i++) {
      this._reviewLabels.push(`Peer assessment #${i + 1}`);
      for (
        let j = 0;
        j < this._examReviewService.getExam().noReviewQuestions;
        ++j
      ) {
        if (!this._PA1Questions[i]) this._PA1Questions[i] = [];
        if (
          !this._examReviewService.getPeerAssessmentReviewLoadingError(i + 1)
        ) {
          this._PA1Questions[i][j] =
            await this._examReviewService.getPeerAssesmentQuestion(
              i + 1,
              j + 1
            );
        }
      }
    }

    // for (let i = 0; i < this._examReviewService.getExam().questionsNo; ++i) {
    //   this.questions.push(
    //     await this._examReviewService.getQuestionReview(i + 1)
    //   );
    // }
    this._examReviewService.setCurrentQuestionOrdinal(1); // required for header component
    // this._examReviewService.currentQuestionObservable.subscribe(
    //   (currQuestion) => {
    //     this._currQuestion = currQuestion;
    //   }
    // );

    // this._route.params.subscribe(async (params: Params) => {
    //   let ordinal = +params['id'];
    //   if (ordinal > 0) {
    //     this._examLoaded = true;
    //     this._examReviewService.setCurrentQuestionOrdinal(ordinal);
    //   }
    // });
  }
}
