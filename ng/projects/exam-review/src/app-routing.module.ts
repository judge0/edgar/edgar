import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from 'projects/common/components/page-not-found-component/page-not-found.component';
import { ExamReviewComponent } from './app/exam-review/exam-review.component';
import { Pa1ExamReviewComponent } from './app/pa1-exam-review/pa1-exam-review.component';
import { Pa2ExamReviewComponent } from './app/pa2-exam-review/pa2-exam-review.component';

const routes: Routes = [
  { path: '', component: ExamReviewComponent, pathMatch: 'full' },
  { path: 'pa1', component: Pa1ExamReviewComponent },
  { path: 'pa2', component: Pa2ExamReviewComponent },
  { path: ':id', component: ExamReviewComponent },
  { path: '**', component: PageNotFoundComponent }, // Wildcard route for a 404 page
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
