import { Injectable } from '@angular/core';
import { IExamService } from 'projects/common/models/IExamService';
import { ExamReviewService } from './exam.review.service';

@Injectable({
  providedIn: 'root',
})
export class BaseExamReviewService implements IExamService {
  constructor(private _service: ExamReviewService) {}
  examLoaded(): boolean {
    return this._service.examLoaded();
  }
  getExam() {
    return this._service.getExam();
  }
  devlogin() {
    return this._service.devlogin();
  }
}
