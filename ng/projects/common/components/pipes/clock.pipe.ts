import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'clockCaption',
})
export class ClockCaptionPipe implements PipeTransform {
  transform(seconds: number): string {
    seconds = Math.abs(seconds);
    let h = Math.floor(seconds / (60 * 60));
    let m = Math.floor((seconds - 60 * 60 * h) / 60);
    let s = seconds % 60;
    let days = '';
    if (h > 23) {
      days = Math.floor(h / 24) + ' days ';
      h = h % 24;
    }

    let time = days;
    time += h < 10 ? '0' + h : h;
    time += ':';
    time += m < 10 ? '0' + m : +m;
    time += ':';
    time += s < 10 ? '0' + s : s;

    return time;
  }
}
