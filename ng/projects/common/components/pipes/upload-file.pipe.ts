import { Pipe, PipeTransform } from '@angular/core';
import { environment } from 'projects/config/environments/environment';
import { Question } from '../../models/question';
import { FileUploadComponent } from 'projects/exam/src/app/file-upload/file-upload.component';

@Pipe({
  name: 'uploadFile',
  pure: false,
})
export class UploadFilePipe implements PipeTransform {
  transform(
    idx: number,
    currQuestion: Question,
    selectedFileName: string
  ): string {
    if (currQuestion.tempUploadedFileNames) {
      return currQuestion.tempUploadedFileNames[idx]
        ? currQuestion.tempUploadedFileNames[idx] ===
          FileUploadComponent.RUNNING_STRING
          ? '<i class="fa fa-2 fa-spinner fa-spin"></i>'
          : `<a href="${
              environment.devWebApiUrl ?? ''
            }/exam/run/file/download/${currQuestion.ordinal}/${idx}"
                target="_blank">
                <span class="badge badge-success"><i class="file-icon fa fa-file"></i> ${
                  currQuestion.tempUploadedFileNames[idx]
                }</span>
              </a>`
        : selectedFileName == FileUploadComponent.SELECT_FILE_STRING
        ? '<small class="text-muted"><i>(nothing uploaded yet)</i></small>'
        : '<span class="text-danger horizontal-shake">🠔 Nothing uploaded yet: click the cloud button to upload!</span>';
    } else {
      return '??';
    }
  }
}
