import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'jsonResultHint',
})
export class JsonResultHintPipe implements PipeTransform {
  transform(hint: string): unknown {
    return '<h4>Hint: </h4><pre>' + hint + '</pre>';
  }
}
