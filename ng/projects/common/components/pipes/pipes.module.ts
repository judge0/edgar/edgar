import { NgModule } from '@angular/core';

import { ClockCaptionPipe } from './clock.pipe';
import { SubmitButtonPipe } from './submit-button.pipe';
import { QuestionAnswerPipe } from './question_answer.pipe';
import { SqlRunLabelPipe } from './sql-run-label.pipe';
import { UploadFilePipe } from './upload-file.pipe';
import { JsonResultScoreHintPipe } from './json-result-score-hint.pipe';
import { JsonResultHintPipe } from './json-result-hint.pipe';
import { GridGetHintPipe } from './grid-get-hint.pipe';
import { LectureTeacherLabelPipe } from './lecture-teacher-label.pipe';
import { LectureStudentLabelPipe } from './lecture-student-label.pipe';

const PIPES = [
  ClockCaptionPipe,
  SubmitButtonPipe,
  QuestionAnswerPipe,
  UploadFilePipe,
  SqlRunLabelPipe,
  JsonResultScoreHintPipe,
  JsonResultHintPipe,
  GridGetHintPipe,
  LectureStudentLabelPipe,
  LectureTeacherLabelPipe,
];
@NgModule({
  declarations: [PIPES],
  exports: [PIPES],
})
export class PipesModule {}
