import { Observable } from 'rxjs';

export interface ITicketService {
  getTicketConversation(ordinal: number): Observable<any>;
  raiseTicket(ordinal: number, description: string): Observable<any>;
  getTicketStatuses(): Observable<any>;
  isTicketResolved(): Observable<any>;
}
