import { Component, Input, OnInit } from '@angular/core';
import { CodeResult } from '../../../models/exam/code-result';

@Component({
  selector: 'json-result',
  templateUrl: './json-result.component.html',
  styleUrls: ['./json-result.component.css'],
})
export class JsonResultComponent implements OnInit {
  @Input() c_eval_data: any;
  @Input()
  result!: CodeResult;
  @Input() queryRunning: boolean = false;
  @Input() isRevision: boolean = false;
  parsed_eval_data: any;

  ngOnInit(): void {}

  isRunning(): boolean {
    return this.queryRunning;
  }
  isKidsMode(): boolean {
    return this.result.score.is_correct !== undefined;
  }

  getHtml(): string {
    return this.result ? this.result.codeResult : '';
  }
}
