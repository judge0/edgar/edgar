import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
} from '@angular/core';
import { ProgrammingLanguage } from '../../models/programming-language';
declare var CodeMirror: any;

@Component({
  selector: 'code-mirror',
  templateUrl: './code-mirror.component.html',
  styleUrls: ['./code-mirror.component.css'],
})
export class CodeMirrorComponent implements OnInit, OnChanges, AfterViewInit {
  _editor: any;
  _editorNativeElement: any;
  @Input()
  initialCode!: string;
  // @Input()
  // ordinal!: number;
  @Input()
  langId!: number;
  @Input()
  readOnly!: boolean;
  @Input()
  cmSkin!: string;
  @Input()
  questionType!: string;
  @Input()
  programmingLanguages!: ProgrammingLanguage[];
  @Output() codeChanged = new EventEmitter();
  @Output() runCodeHotkey = new EventEmitter();
  // _prevOrdinal!: number;
  // _prevLangId!: number;
  private _currCode: string;
  constructor(elRef: ElementRef) {
    this._editorNativeElement = elRef.nativeElement;
    this._currCode = this.initialCode;
  }

  ngOnInit() {}

  ngAfterViewInit() {
    var self = this;
    // console.log("CodeMirror", this.questionType, this.cmSkin, this.initialCode, this.readOnly);
    this._editor = CodeMirror(this._editorNativeElement, {
      lineNumbers: true,
      indentUnit: this.questionType.toUpperCase().indexOf('SQL') >= 0 ? 4 : 3,
      tabSize: this.questionType.toUpperCase().indexOf('SQL') >= 0 ? 4 : 3,
      indentWithTabs:
        this.questionType.toUpperCase().indexOf('PYTHON') >= 0 ||
        (this.programmingLanguages &&
          this.programmingLanguages.find(
            (x) => x.name.toUpperCase().indexOf('PYTHON') >= 0
          ))
          ? true
          : false,
      mode:
        this.questionType.toUpperCase().indexOf('SQL') >= 0
          ? 'text/x-pgsql'
          : this.questionType.toUpperCase().indexOf('JAVA') >= 0
          ? 'text/x-java'
          : 'text/x-csrc',
      theme: this.cmSkin, // let it break: || 'ambiance',
      value: this.initialCode || '\n\n\n\n\n\n',
      viewportMargin: Infinity,
      readOnly: this.readOnly || false,
      extraKeys: {
        'Ctrl-Enter': function (_cm: any) {
          var code = self._editor.getSelection();
          if (!code || 0 == code.length) {
            code = self._editor.getDoc().getValue();
          }
          self.runCodeHotkey.emit(code);
        },
      },
    });
    // this._prevOrdinal = this.ordinal;
    // this._prevLangId = this.langId;
    this._editor.on('change', (editor: any, change: any) => {
      this._currCode = this._editor.getDoc().getValue();
      this.codeChanged.emit(this._currCode);
    });
    this._editor.on('focus', (editor: any, change: any) => {
      this._editor.refresh();
    });
  }

  ngOnChanges(changes: any) {
    // This ordinal is to prevent inf-loop, by me emitting to parent, and parent sending back.
    // That is, I need to set this only when the q number or langId has changed.
    // console.log('CM ngOnChanges', changes);
    // console.log('CM this.programmingLanguages', this.programmingLanguages);
    if (
      changes.initialCode &&
      changes.initialCode.currentValue != this._currCode &&
      this._editor
    ) {
      // console.log("refreshing existing CM instance...");
      this._currCode = changes.initialCode.currentValue;
      this._editor.getDoc().setValue(this._currCode);
    }

    if (changes.readOnly && this._editor) {
      this._editor.setOption('readOnly', changes.readOnly.currentValue);
    }
    // this._editor
    // && this._editor.getDoc().setValue(this.currCode);
    //this._prevOrdinal, this.ordinal, this._prevOrdinal === this.ordinal);
    // if (
    //     this._editor &&
    //     (this._prevOrdinal !== this.ordinal ||
    //         this._prevLangId !== this.langId)
    //     ) {
    //     this._editor.getDoc().setValue(this.currCode);
    //     this._prevOrdinal = this.ordinal;
    //     this._prevLangId = this.langId;
    // }
  }
}
