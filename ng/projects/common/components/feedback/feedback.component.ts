import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Feedback } from './feedback';
import { FeedbackService } from '../../services/feedback.service';

@Component({
  selector: 'feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css'],
})
export class FeedbackComponent implements OnInit, OnChanges {
  @Input()
  title!: string;
  @Input()
  exerciseId!: number;
  @Input()
  exerciseQuestionOrdinal!: number;
  @Input()
  tutorialId!: number;
  @Input()
  tutorialStepId!: number;
  @Input()
  hideRating!: boolean;
  @Input()
  hideComment!: boolean;
  @Input()
  ratingRequired!: boolean;
  @Input()
  commentRequired!: boolean;
  @Input()
  maxRatingScore!: number;
  @Input()
  open!: boolean;

  _previousFeedbacks: Feedback[] = [];
  _showHistory: boolean = false;
  _showBox: boolean = false;
  _showResult: boolean = false;
  _resultSuccess!: boolean;
  _comment!: string;
  _rating!: Nullable<number>;
  _ratingClasses: number = 5;
  _sending: boolean = false;

  constructor(private _feedbackService: FeedbackService) {}

  setRating(r: number) {
    this._rating = r;
  }

  toggleFeedbackBox() {
    this._showBox = !this._showBox;

    if (this._showHistory) {
      this._showHistory = false;
    }
  }

  submit() {
    const feedback = new Feedback(
      this._comment,
      this._rating,
      this._rating ? this._ratingClasses : null,
      this.exerciseId,
      this.exerciseQuestionOrdinal,
      this.tutorialId,
      this.tutorialStepId
    );
    this._sending = true;
    this._feedbackService.send(feedback).subscribe((res) => {
      const data = res;
      this._resultSuccess = data.success;
      this._showBox = false;
      this._showResult = true;
      this._sending = false;
      feedback.timestamp = new Date();
      this._previousFeedbacks.unshift(feedback);
    });
  }

  canSend() {
    return (
      (!this.ratingRequired || (this.ratingRequired && this._rating)) &&
      ((this.commentRequired && this._comment) || !this.commentRequired)
    );
  }

  reset() {
    this._comment = '';
    this._rating = null;
  }

  newFeedback() {
    this.reset();
    this._showResult = false;
    this._showBox = true;
  }

  toggleHistory() {
    this._showHistory = !this._showHistory;
  }

  close() {
    this.reset();
    this._showResult = false;
    this._showBox = false;
    this._showHistory = false;
  }

  getSubmitLabel() {
    return this._sending
      ? '<i class="fa fa-2 fa-spinner fa-spin"></i>'
      : 'Submit';
  }

  getRatingClasses() {
    return new Array(this._ratingClasses);
  }

  getFeedbackTitle() {
    return this.title ? this.title : undefined;
  }

  ngOnInit() {
    this._ratingClasses = this.maxRatingScore || 5;
    this._showBox = this.open || false;
  }

  ngOnChanges() {
    this._ratingClasses = this.maxRatingScore || 5;

    if (this.open !== undefined) {
      this._showBox = this.open;
    }

    this._feedbackService
      .get(
        new Feedback(
          '',
          0,
          0,
          this.exerciseId,
          this.exerciseQuestionOrdinal,
          this.tutorialId,
          this.tutorialStepId
        )
      )
      .subscribe((fbs) => {
        this._previousFeedbacks = fbs;
      });
  }

  onFeedbackClick() {
    if (this._showResult) {
      this.newFeedback();
    } else {
      this.toggleFeedbackBox();
    }
  }
}
