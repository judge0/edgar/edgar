export class Feedback {
  public clientTs: any;
  constructor(
    public comment: string,
    public rating: Nullable<number>,
    public maxRating: Nullable<number>,
    public exerciseId?: number,
    public exerciseQuestionOrdinal?: number,
    public tutorialId?: number,
    public tutorialStepId?: number,
    public timestamp?: Date
  ) {
    this.clientTs = new Date();
  }
}
