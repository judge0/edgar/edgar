import { Component, Input, OnInit } from '@angular/core';
import { environment } from 'projects/config/environments/environment';
import { Question } from 'projects/common/models/question';

@Component({
  selector: 'question-attachments',
  templateUrl: './question-attachments.component.html',
  styleUrls: ['./question-attachments.component.css'],
})
export class QuestionAttachmentsComponent {
  @Input()
  currQuestion!: Question;
  private _devStub: string;
  constructor() {
    this._devStub = `${environment.devWebApiUrl ?? ''}`;
  }
}
