import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'grading-model-label[gradingModelName]',
  templateUrl: './grading-model-label.component.html',
  styleUrls: ['./grading-model-label.component.css'],
  providers: [],
})
export class GradingModelLabelComponent implements OnChanges {
  @Input() gradingModelName!: string;
  correctPoints: string = '?';
  incorrectPoints: string = '?';
  unansweredPoints: string = '?';
  constructor() {}

  ngOnChanges(): void {
    let arrPoints = this.gradingModelName.split('/');
    if (arrPoints.length === 3) {
      this.correctPoints = arrPoints[0];
      this.unansweredPoints = arrPoints[1];
      this.incorrectPoints = arrPoints[2];
    }
  }
}
