import { Component, Input, OnInit } from '@angular/core';
import { CodeResult } from '../../../models/exam/code-result';

@Component({
  selector: 'clang-result',
  templateUrl: './clang-result.component.html',
  styleUrls: ['./clang-result.component.css'],
})
export class ClangResultComponent {
  @Input()
  result!: CodeResult;
  @Input()
  isRevision!: boolean;
  @Input()
  showHourglass!: boolean;

  constructor() {}

  isRunning(): boolean {
    return !!this.showHourglass;
  }

  isKidsMode(): boolean {
    return this.result.score.is_correct !== undefined;
  }

  formatHint(): string {
    return this.result.score.hint.replace(/\n/g, '<br>');
  }
  getHtml(): string {
    return this.result ? this.result.codeResult : '';
  }

  // getResult() : string {
  //     console.log('CLangResultComponent:', this.result);
  //     var score : string;
  //     if (this.result.score) {
  //         if (this.isRevision) {
  //             score = `Score: ${this.result.score.score} (=${100 * this.result.score.score_perc}%)`;
  //         } else {
  //             score = `Score: ${100 * this.result.score.score_perc}%`;
  //         }
  //         if (this.result.score.is_correct) {
  //             return `<span style="background-color:lime;">
  //                 <i class="fa fa-smile-o" aria-hidden="true"></i> Correct! Well done! </span>
  //                 ${score}`;
  //         } else if (this.result.score.is_partial) {
  //             return `<span style="background-color:yellow;">
  //                <i class="fa fa-frown-o" aria-hidden="true"></i><i class="fa fa-smile-o" aria-hidden="true"></i> Partial, almost there... </span>
  //                 ${score}`;
  //         } else {
  //             return `<span style="background-color:lemonchiffon;">
  //                <i class="fa fa-frown-o" aria-hidden="true"></i> Missed it. </span>
  //                 ${score}`;
  //         }
  //     } else {
  //         return '';
  //     }
  // }
  // getWarning() : string {
  //     if (this.result.warning) {
  //         return this.result.warning;
  //     } else {
  //         return null;
  //     }
  // }
}
