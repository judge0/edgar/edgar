import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { LogEvent } from '../models/log-event';
import { CodeResult } from '../../models/exam/code-result';
import { LogService } from './log.service';
import { map, Observable } from 'rxjs';
import {
  CodeRunnerServiceConfig,
  CODERUNNER_SERVICE_CONFIG,
} from '../../config/code-runner.config';

@Injectable({
  providedIn: 'root',
})
export class CodeRunnerService {
  private _urlStub: string;

  constructor(
    private _http: HttpClient,
    private _logService: LogService,
    @Inject(CODERUNNER_SERVICE_CONFIG) private _config: CodeRunnerServiceConfig
  ) {
    const path = window.location.pathname.split('/');
    this._urlStub = path[1];
  }
  private _getCodeResult(
    ordinal: number,
    code2: string,
    log: boolean,
    urlstub: string,
    itemModifier: string = 'question',
    programmingLanguageId: string,
    stdin?: string
  ): Observable<CodeResult> {
    if (log) {
      this._logService.log(new LogEvent('Running code', code2));
    }
    //console.log(code2)
    var headers = new HttpHeaders();
    //headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Content-Type', 'application/json');
    //return this._http.post(`/${urlstub}/question/exec/${ordinal}`, `code=${encodeURIComponent(code)}`, {

    return this._http
      .post<any>(
        ordinal > 999
          ? `${this._config.execCodeUrl}/${Math.floor(ordinal / 1000)}/${
              ordinal % 1000
            }` // tutorials have step/ordinal api
          : `${this._config.execCodeUrl}/${ordinal}`,
        {
          code: code2,
          id_programming_language: programmingLanguageId || 0,
          stdin,
        },
        {
          headers: headers,
          withCredentials: true,
        }
      )
      .pipe(
        map((res) => {
          var rv = res;
          if (log) {
            var toLog = { ...res };
            // removing potential lenghty data and coderesult properties
            // they are already logged directly from the server to a separate compressed "testlogdetails" collection
            delete toLog.data;
            delete toLog.coderesult;
            this._logService.log(
              new LogEvent('Code exec result received', JSON.stringify(toLog))
            );
          }
          return new CodeResult(
            rv.success,
            rv.error,
            rv,
            rv.db,
            rv.warning,
            rv.coderesult,
            rv.score,
            rv.runStats
          );
        })
      );
  }
  private _getCodeReviewResult(
    ordinal: number,
    urlstub: string
  ): Observable<CodeResult> {
    // var headers = new HttpHeaders();
    // headers.append('Content-Type', 'application/json');
    // `${this._config.execCodeUrl}/${ordinal}`
    return this._http
      .post<CodeResult>(
        `${this._config.execCodeReviewUrl}/${urlstub}/${ordinal}`,
        '',
        {
          withCredentials: true,
        }
      )
      .pipe(
        map((res) => {
          var rv = { ...res };
          return new CodeResult(
            rv.success,
            rv.error,
            rv,
            rv.db,
            rv.warning,
            rv.codeResult,
            rv.score,
            []
          );
        })
      );
  }
  getCodeResult(
    ordinal: number,
    code: string,
    programmingLanguageId: string,
    isPlayground?: boolean,
    stdin?: string
  ): Observable<CodeResult> {
    return this._getCodeResult(
      ordinal,
      code,
      true,
      this._urlStub,
      isPlayground ? 'code' : 'question',
      programmingLanguageId,
      stdin
    );
  }

  getCodeReviewStudentResult(ordinal: number): Observable<CodeResult> {
    return this._getCodeReviewResult(ordinal, 'student');
  }
  getCodeReviewCorrectResult(ordinal: number): Observable<CodeResult> {
    return this._getCodeReviewResult(ordinal, 'correct');
  }
}
