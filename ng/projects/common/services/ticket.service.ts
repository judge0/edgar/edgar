import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { LogEvent } from '../models/log-event';
import { LogService } from './log.service';
import { ITicketService } from '../components/ticket/ITicket';
import {
  TicketServiceConfig,
  TICKET_SERVICE_CONFIG,
} from 'projects/config/ticket.config';

@Injectable({
  providedIn: 'root',
})
export class TicketService implements ITicketService {
  constructor(
    private _http: HttpClient,
    private _logService: LogService,
    @Inject(TICKET_SERVICE_CONFIG) private _config: TicketServiceConfig
  ) {}
  getTicketConversation(ordinal: number): Observable<any> {
    var headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this._http
      .get(`${this._config.ticketConversationUrl}/${ordinal}`, {
        withCredentials: true,
        headers: headers,
      })
      .pipe(
        map((res) => {
          return res;
        })
      );
  }
  isTicketResolved(): Observable<any> {
    var headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this._http
      .get<any>(`${this._config.isTicketResolvedUrl}`, {
        headers: headers,
        withCredentials: true,
      })
      .pipe(
        map((res) => {
          let msg = res;
          if (msg && (msg.ticket_question_ordinal || msg.ticket_step_ordinal)) {
            this._logService.log(
              new LogEvent(
                'Displayed ticket message.',
                msg.ticket_question_ordinal || msg.ticket_step_ordinal
              )
            );
          }
          return msg;
        })
      );
  }
  getTicketStatuses(): Observable<any> {
    var headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this._http
      .get<any>(`${this._config.ticketStatusesUrl}`, {
        headers: headers,
        withCredentials: true,
      })
      .pipe(
        map((res) => {
          let rv = res;
          return JSON.parse(rv.ticket_statuses);
        })
      );
  }
  raiseTicket(ordinal: number, description: string): Observable<any> {
    let ticket = {
      ordinal: ordinal,
      description: description,
    };
    this._logService.log(
      new LogEvent('Submitting ticket', JSON.stringify(ticket))
    );
    var headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('access-control-request-headers', 'content-type');
    return this._http
      .post(`${this._config.raiseTicketUrl}`, ticket, {
        // headers: headers,
        withCredentials: true,
      })
      .pipe(
        map((res) => {
          var rv = res;
          this._logService.log(
            new LogEvent('Submit ticket answer received', JSON.stringify(rv))
          );
          return rv;
        })
      );
  }
}
