export class GenericResponse {
  success!: boolean;
  error!: ErrorObject;
}

class ErrorObject {
  message!: string;
}
