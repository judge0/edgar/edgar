export class LogEvent {
  public clientTs: Date; // WTF, typscript?
  public retry: number;
  constructor(public eventName: string, public eventData: string) {
    this.clientTs = new Date();
    this.retry = 0;
  }
}
