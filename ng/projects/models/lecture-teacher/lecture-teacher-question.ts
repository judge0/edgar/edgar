export class Question {
  constructor(
    public caption: string,
    public ordinal: number,
    public content,
    public answers,
    public multiCorrect,
    public type: string,
    public correctAnswers: number[]
  ) {}

  // static getAnswerLabel (answers : any) : string {
  // if (answers) {
  //     if (answers instanceof Array && answers.length) {
  //         return answers.
  //         map(function(num){
  //             return String.fromCharCode(97 - 1 + parseInt('' + num))
  //         }).join();
  //     }
  // }
  // return '-';
  // }
}
