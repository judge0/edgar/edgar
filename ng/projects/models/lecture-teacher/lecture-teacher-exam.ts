export class Exam {
  constructor(
    public noOfQuestions: number,
    public title: string,
    public password: string,
    public roomName: string
  ) {}
}
