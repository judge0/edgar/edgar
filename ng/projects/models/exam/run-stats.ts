export class RunStats {
  id!: number;
  can_run!: boolean;
  ordinal!: number;
  qcnt!: number;
  tcnt!: number;
  timeout!: number;
  time_wait!: number;
}
