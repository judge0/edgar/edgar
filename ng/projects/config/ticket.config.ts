import { InjectionToken } from '@angular/core';
import { environment } from 'projects/config/environments/environment';

export const TICKET_SERVICE_CONFIG = new InjectionToken<TicketServiceConfig>(
  'ticket.service.config'
);

export interface TicketServiceConfig {
  ticketConversationUrl: string;
  isTicketResolvedUrl: string;
  ticketStatusesUrl: string;
  raiseTicketUrl: string;
}

export const DEFAULT_TICKET_SERVICE_CONFIG: TicketServiceConfig = {
  ticketConversationUrl: `${
    environment.devWebApiUrl ?? ''
  }/ticket/conversation`,
  isTicketResolvedUrl: `${environment.devWebApiUrl ?? ''}/ticket/isresolved`,
  ticketStatusesUrl: `${environment.devWebApiUrl ?? ''}/ticket/statuses`,
  raiseTicketUrl: `${environment.devWebApiUrl ?? ''}/ticket/raise`,
};
export const TUTORIAL_TICKET_SERVICE_CONFIG: TicketServiceConfig = {
  ticketConversationUrl: `${
    environment.devWebApiUrl ?? ''
  }/tutorial/ticket/conversation`,
  isTicketResolvedUrl: `${
    environment.devWebApiUrl ?? ''
  }/tutorial/ticket/isresolved`,
  ticketStatusesUrl: `${
    environment.devWebApiUrl ?? ''
  }/tutorial/ticket/statuses`,
  raiseTicketUrl: `${environment.devWebApiUrl ?? ''}/tutorial/ticket/raise`,
};
