import { Question } from './../../common/question';
import { Answer } from './../../common/answer';
import { CodeHint } from './hints';

export class TutorialQuestion extends Question {
  constructor(
    caption: string,
    ordinal: number,
    currAnswers: number[],
    content,
    answers: { aHtml: string; ordinal: number; hint?: string },
    multiCorrect,
    modeDesc,
    type: string,
    public codeHints?: CodeHint[]
  ) {
    super(
      caption,
      ordinal,
      currAnswers,
      content,
      answers,
      multiCorrect,
      modeDesc,
      type,
      null,
      null,
      null,
      false
    );
  }
}

// export class TutorialAnswer extends Answer {
// 	ordinal: number;
// 	hint?: string
// }
