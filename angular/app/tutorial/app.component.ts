import { Component, Injectable, AfterViewInit } from 'angular2/core';
import { TutorialService } from './tutorial.service';
import { tutorialLogServiceConfigProvider } from '../common/log.service.provider';
import { LogService, LogEvent } from './../common/log.service';
import { QContentComponent } from '../test/qcontent.component';
import { QCodeComponent } from '../test/qcode.component';
import { CodeMirrorComponent } from '../common/codemirror.component';
//import {FeedbackComponent} from '../common/feedback.component';
import { WINDOW, WINDOW_PROVIDERS } from './../common/window.service';
import { BrowserDomAdapter } from 'angular2/platform/browser';
import { Tutorial } from './models/tutorial';
import { Step } from './models/step';
import { StepHint, AnswerHint, AbcHint, CodeHint } from './models/hints';
import { FeedbackComponent } from './../common/feedback.component';
import { Question } from '../common/question';
import { Answer } from '../common/answer';
declare var $: any; //  to use jQuery in angular!"

@Component({
  selector: 'tutorial-app',
  host: {
    '(window:focus)': 'onFocus()',
    '(window:blur)': 'onBlur()',
  },
  template: `<div *ngIf="_tutorial" class="tutorial-header">
      <progress
        *ngIf="_step"
        value="{{ _step.ordinal }}"
        max="{{ _tutorial.steps.length }}"
      ></progress>
      <div *ngIf="_tutorial && _tutorial.steps" class="btn-group right-buttons">
        <div class="dropdown">
          <button
            id="stepsDrop"
            class="btn btn-light dropdown-toggle"
            type="button"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Tutorial content
          </button>
          <div
            class="dropdown-menu dropdown-menu-right content-dropdown"
            aria-labelledby="stepsDrop"
          >
            <div class="list-group">
              <button
                *ngFor="#step of _tutorial.steps"
                class="list-group-item"
                (click)="
                  (step.ordinal <= _latestStepOrdinal || _tutorial.allowRandomAccess) &&
                    goToStep(step.ordinal)
                "
                [class.active]="_step && _step.ordinal == step.ordinal"
                [class.disabled]="step.ordinal > _latestStepOrdinal && !_tutorial.allowRandomAccess"
              >
                <span class="badge badge-light badge-pill">{{ step.ordinal }}</span
                >:
                <span nfIg="step.title">{{ step.title }}</span>
              </button>
            </div>
          </div>
        </div>
      </div>
      <div class="btn-group right-buttons">
        <div class="dropdown">
          <button
            id="layoutDrop"
            class="btn btn-light dropdown-toggle"
            type="button"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Layout
          </button>
          <div
            class="dropdown-menu dropdown-menu-right content-dropdown"
            aria-labelledby="layoutDrop"
          >
            <div class="list-group">
              <button
                class="list-group-item"
                [class.active]="12 == _layout"
                (click)="setLayout(12)"
              >
                Horizontal
              </button>
              <button class="list-group-item" [class.active]="9 == _layout" (click)="setLayout(9)">
                9:3
              </button>
              <button class="list-group-item" [class.active]="6 == _layout" (click)="setLayout(6)">
                6:6
              </button>
              <button class="list-group-item" [class.active]="3 == _layout" (click)="setLayout(3)">
                3:9
              </button>
            </div>
          </div>
        </div>
      </div>
      <div *ngIf="_step" class="btn-group nav-buttons right-buttons" role="group">
        <button [disabled]="_step.ordinal <= 1" (click)="previousStep()" class="btn btn-light">
          Previous step
        </button>
        <button
          (click)="submitAnswer()"
          class="btn"
          [class.btn-success]="_correctStatus == true && _showCorrectStatus"
          [class.btn-danger]="_correctStatus == false && _showCorrectStatus"
          [class.btn-primary]="!_showCorrectStatus"
          [disabled]="_showCorrectStatus || _currAnswers[_step.ordinal - 1].length === 0"
        >
          Submit answer
        </button>
        <button
          class="btn btn-light"
          (click)="nextStep()"
          [disabled]="
            _step.ordinal > _tutorial.steps.length ||
            (_step.ordinal !== _tutorial.steps.length && _step.ordinal >= _latestStepOrdinal)
          "
        >
          Next step
        </button>
      </div>

      <img
        src="/images/edgar75.png"
        id="edgar-logo"
        title="Ask Edgar for hints"
        (click)="showNextStepHint()"
      />
      <h1>{{ _tutorial.title }}</h1>
    </div>
    <section *ngIf="_currStepHint" id="step-hint" class="alert alert-info">
      <button type="button" class="close" (click)="closeHint()">
        <span aria-hidden="true">&times;</span>
      </button>
      <h3>Hint</h3>
      <div [innerHTML]="_currStepHint.html"></div>
    </section>
    <div *ngIf="_step" class="row">
      <section class="step col-{{ _layout === 0 ? 12 : _layout }}" style="overflow-x: scroll;">
        <h2>
          <span *ngIf="_tutorial" class="badge badge-secondary"
            >{{ _step.ordinal }}/{{ _tutorial.steps.length }}</span
          >: {{ _step.title }}
          <span
            *ngIf="_step && _step.hints.length > 0"
            class="text-danger"
            title="This step has hints available. Click on Edgar to get one."
            >*</span
          >
        </h2>
        <div enhance-md>
          <div [innerHTML]="_step.html"></div>
        </div>
        <hr />
      </section>
      <section class="question col-{{ 12 - (_layout === 12 ? 0 : _layout) }}">
        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" role="tab" data-toggle="tab" href="#questiontab">Question</a>
          </li>
          <li class="nav-item">
            <a
              class="nav-link"
              role="tab"
              data-toggle="tab"
              href="#playgroundtab"
              (click)="refreshCM()"
              >Playground</a
            >
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane fade show active" id="questiontab" role="tabpanel">
            <div
              *ngIf="_currAnswerHint"
              [innerHTML]="_currAnswerHint"
              id="answer-hint"
              class="alert"
              [class.alert-danger]="_correctStatus === false"
              [class.alert-success]="_correctStatus === true"
            ></div>
            <q-content
              [currAnswers]="_currAnswers"
              [currQuestion]="_step.question"
              [cmSkin]="_tutorial.cmSkin"
              [layoutTutorial]="true"
              (answerUpdated)="onAnswerUpdated($event)"
              (answerEvaluated)="onAnswerEvaluated($event)"
            >
            </q-content>
          </div>
          <div class="tab-pane fade" id="playgroundtab" role="tabpanel">
            <h2>Playground (test tutorial code here)</h2>
            <div *ngIf="_dummyQuestion" class="container-fluid">
              <q-code
                [isPlayground]="true"
                [currAnswers]="_currAnswers"
                [currQuestion]="_dummyQuestion"
                [cmSkin]="_tutorial.cmSkin"
                (answerUpdated)="onAnswerUpdated($event)"
                (codeRun)="onAnswerEvaluated($event)"
              ></q-code>
            </div>
          </div>
        </div>
      </section>
    </div>
    <feedback
      *ngIf="_tutorial"
      [title]="getFeedbackTitle()"
      [ratingRequired]="true"
      [maxRatingScore]="5"
      [tutorialId]="_tutorial.id"
      [tutorialStepId]="_step && _step.id"
    ></feedback>
    <div *ngIf="!_step && _finished">
      <h2 class="text-success">Congratulations!</h2>
      <p>You have successfully finished this tutorial!</p>
      <p>
        You may return to the
        <a href="/tutorial">list of available tutorials for this course</a> or review content,
        question and answers at any step by using the upper steps dropdown menu.
      </p>
    </div>`,
  styles: [
    `
      #edgar-logo {
        float: left;
        cursor: pointer;
      }
      #feedback {
        float: right;
      }
      .tutorial-header {
        border-bottom: 5px #9de5fb solid;
        padding-bottom: 10px;
        margin-top: 20px;
        margin-bottom: 20px;
      }
      .right-buttons {
        float: right;
        margin-right: 20px;
      }
      .content-dropdown {
        float: right;
        min-width: 320px;
      }
      .question {
        border-left: #9de5fb 5px solid;
      }
      .nav-buttons {
      }
      progress {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
      }
      #answer-hint {
      }
      #step-hint {
      }
    `,
  ],
  providers: [TutorialService, LogService, tutorialLogServiceConfigProvider, WINDOW_PROVIDERS],
  directives: [FeedbackComponent, QContentComponent, CodeMirrorComponent, QCodeComponent],
})
@Injectable()
export class AppComponent implements AfterViewInit {
  _tutorial: Tutorial;
  _step: Step;
  _latestStepOrdinal: number;
  _currAnswers: Answer[];
  _currStepHint: StepHint;
  _currAnswerHint: AnswerHint;
  _finished: boolean = false;
  _showCorrectStatus: boolean = false;
  _correctStatus: boolean;
  _win: Window;
  _dummyQuestion: Question;
  _layout: number;
  _DOM: BrowserDomAdapter;

  constructor(
    private _tutorialService: TutorialService,
    private _logService: LogService,
    win: WINDOW
  ) {
    this._win = win.nativeWindow;
    this._DOM = new BrowserDomAdapter();
    const path = window.location.pathname.split('/');
    const tutorialId = parseInt(path[2]);
    this._layout = 6;
    this._tutorialService.startTutorial(tutorialId).subscribe((status) => {
      this._tutorialService.getTutorial(tutorialId).subscribe((t) => {
        this._tutorial = t;
        this._latestStepOrdinal = status.latestStepOrdinal;

        if (status.currentAnswers && status.currentAnswers.length === t.steps.length) {
          this._currAnswers = status.currentAnswers;
        } else {
          this._currAnswers = [];
          for (let i = 0; i < t.steps.length; ++i) {
            this._currAnswers.push(new Answer());
          }
        }
        this._currAnswers = this._currAnswers.slice();

        this._tutorialService
          .getStep(status.latestStepOrdinal, this._currAnswers[status.latestStepOrdinal - 1])
          .subscribe((s) => {
            this._step = s;
            this._dummyQuestion = new Question(
              'dummy',
              status.latestStepOrdinal,
              [],
              'dummy content',
              undefined,
              '',
              '',
              'Evaluated SQL question',
              null,
              null,
              null,
              false
            );
          });
      });
    });
  }
  ngAfterViewInit() {
    this._DOM.dispatchEvent(
      this._DOM.getGlobalEventTarget('document'),
      new CustomEvent('angular-ready')
    );
    let self = this;
    setTimeout(function () {
      self.refreshCM();
    }, 1000);

    //$(document).trigger("angular-ready", ["bim", "baz"]);
  }
  refreshCM() {
    $('.CodeMirror').each(function () {
      this.CodeMirror.refresh();
    });
  }
  setLayout(newLayout: number) {
    this._layout = newLayout;
  }
  onAnswerUpdated($newValue) {
    // console.log('onAnswerUpdated', $newValue, this._currAnswers);
    this._showCorrectStatus = false;
    this._currAnswers[this._step.ordinal - 1] = $newValue;
    this._currAnswers = this._currAnswers.slice(); // :( to force ng onchange
    this._logService.log(
      new LogEvent('Answer updated', `step ${this._step.ordinal}, answer: ${$newValue}`)
    );
    this._tutorialService.updateAnswers(this._currAnswers);
    // console.log('onAnswerUpdated after', this._currAnswers);
  }

  onAnswerEvaluated($event) {
    this.showAppropriateHints();
    if ($event.rs.score && $event.rs.score.is_correct) {
      this._correctStatus = true;
    }
  }

  onFocus() {
    this._logService.log(new LogEvent('Got focus', ''));
  }

  onBlur() {
    this._logService.log(new LogEvent('Lost focus', ''));
  }

  showAppropriateHints() {
    if (this._step.question.type.includes('ABC')) {
      const answer = this._step.question.answers.find(
        (a) => a.ordinal == this._currAnswers[this._step.ordinal - 1]
      );

      if (answer && answer.hintHtml) {
        this._currAnswerHint = answer.hintHtml;
      } else {
        this._currAnswerHint = null;
      }
    } else {
      const answer: Answer = this._currAnswers[this._step.ordinal - 1];
      const codeHints: any = this._step.question.codeHints
        .map((ch) => {
          const regexp = new RegExp(ch.trigger, 'i');
          const matches = regexp.test(answer.codeAnswer.code && answer.codeAnswer.code.trim());
          //console.log('regexp', regexp, 'mateches', matches);

          return {
            ordinal: ch.ordinal,
            html: ch.html,
            matches: matches,
          };
        })
        .filter((ch) => ch.matches);

      if (codeHints[0]) {
        this._currAnswerHint = codeHints[0].html;
      } else {
        this._currAnswerHint = null;
      }
    }
  }

  submitAnswer() {
    //console.log('submitAnswer', this._currAnswers);
    this._tutorialService
      .submitAnswer(this._step.ordinal, this._currAnswers[this._step.ordinal - 1])
      .subscribe((result) => {
        this.showAppropriateHints();
        this._correctStatus = result.correct;
        this._showCorrectStatus = true;
        //console.log('result', this._correctStatus, 'unlocking?', this._latestStepOrdinal);
        // if new correctly answered question, unlock next step
        if (
          result.correct &&
          this._step.ordinal == this._latestStepOrdinal &&
          this._step.ordinal !== this._tutorial.steps.length
        ) {
          ++this._latestStepOrdinal;
          //console.log('yes', this._latestStepOrdinal);
        }
      });
  }

  goToStep(ordinal: number) {
    this._tutorialService.getStep(ordinal).subscribe((s) => {
      this._step = s;
      this._currStepHint = null;
      this._currAnswerHint = null;
      this._showCorrectStatus = false;
      this._dummyQuestion = new Question(
        'dummy',
        ordinal,
        [],
        'dummy content',
        undefined,
        '',
        '',
        'Evaluated SQL question',
        null,
        null,
        null,
        false
      );
    });
  }

  previousStep() {
    this.goToStep(this._step.ordinal - 1);
  }

  nextStep() {
    // if all steps finished, congratulate
    if (this._step.ordinal === this._tutorial.steps.length) {
      this._step = null;
      this._finished = true;
      // else if the next step is unlocked, go to next step
    } else if (this._step.ordinal < this._latestStepOrdinal) {
      this.goToStep(this._step.ordinal + 1);
    }
  }

  showNextStepHint() {
    if (this._step.hints.length > 0) {
      const currIdx = this._currStepHint ? this._currStepHint.ordinal : 0;
      const nextIdx = (currIdx + 1) % (this._step.hints.length + 1);
      this._currStepHint = this._step.hints[currIdx];
      // console.log(currIdx, nextIdx);
    } else {
      this._currStepHint = new StepHint(0, 'This step has no hints :(');
    }
  }

  closeHint() {
    this._currStepHint = null;
  }

  getFeedbackTitle() {
    let title = [];

    if (this._tutorial) {
      title.push(this._tutorial.title);
    }

    if (this._step) {
      title.push(`Step ${this._step.ordinal}: ${this._step.title}`);
    }

    return title.join(' - ');
  }
}
