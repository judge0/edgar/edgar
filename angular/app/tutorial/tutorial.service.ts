import { Injectable } from 'angular2/core';
import { Http, Headers } from 'angular2/http';
import { Observable } from 'rxjs/Rx';
import { LogService, LogEvent } from './../common/log.service';

import { Tutorial } from './models/tutorial';
import { Step } from './models/step';
import { StepHint } from './models/hints';
import { TutorialQuestion } from './models/question';
import { Answer } from '../common/answer';

@Injectable()
export class TutorialService {
  private _tutorial: Tutorial;
  private _step: Step;
  private _httpPostHeaders: Headers;

  constructor(private _http: Http, private _logService: LogService) {
    this._httpPostHeaders = new Headers();
    this._httpPostHeaders.append('Content-Type', 'application/json');
  }

  startTutorial(id: number) {
    this._logService.log(new LogEvent('Starting tutorial', ''));
    return this._http
      .post(`/tutorial/api/start/${id}`, '', { headers: this._httpPostHeaders })
      .map((res) => {
        return res.json();
      });
  }

  getTutorial(id: number) {
    if (this._tutorial) {
      return Observable.of(this._tutorial);
    } else {
      this._logService.log(new LogEvent('Request tutorial', ''));
      return this._http.get(`/tutorial/api/${id}`).map((res) => {
        let t = res.json();

        this._tutorial = new Tutorial(
          t.id,
          t.title,
          t.description,
          t.steps,
          t.cmSkin,
          t.allow_random_access
        );
        this._logService.log(new LogEvent('Receive tutorial', ''));
        return this._tutorial;
      });
    }
  }

  getStep(ordinal: number, currAnswers?: any) {
    if (this._step && this._step.ordinal == ordinal) {
      return Observable.of(this._step);
    } else {
      this._logService.log(new LogEvent('Request step', 'ordinal: ' + ordinal));
      return this._http.get(`/tutorial/api/${this._tutorial.id}/step/${ordinal}`).map((res) => {
        const s = res.json();
        const q = s.question;
        const h = s.hints;

        const question = new TutorialQuestion(
          s.ordinal.toString(),
          s.ordinal,
          currAnswers || [],
          q.html,
          q.answers,
          q.multiCorrect,
          q.modeDesc,
          q.type,
          q.codeHints
        );

        const hints = h.map((hint) => new StepHint(hint.ordinal, hint.html));

        this._step = new Step(s.id, s.ordinal, s.title, s.html, hints, question);

        this._logService.log(
          new LogEvent('Receive step', `ordinal: ${this._step.ordinal}, id: ${this._step.id}`)
        );
        return this._step;
      });
    }
  }

  updateAnswers(answers: any) {
    return this._http
      .put(`/tutorial/api/${this._tutorial.id}/answers`, JSON.stringify({ answers: answers }), {
        headers: this._httpPostHeaders,
      })
      .map((resp) => resp.json())
      .subscribe((r) => r);
  }

  submitAnswer(ordinal: number, answer: Answer) {
    this._logService.log(
      new LogEvent('Submitting answer', `Step ordinal: ${ordinal}, answer: ${answer}`)
    );
    return this._http
      .post(
        `/tutorial/api/${this._tutorial.id}/step/${ordinal}`,
        JSON.stringify({ answer: answer }),
        {
          headers: this._httpPostHeaders,
        }
      )
      .map((resp) => resp.json());
  }
}
