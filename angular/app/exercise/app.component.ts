import { Component, Injectable } from 'angular2/core';
import { ExerciseService } from './exercise.service';
import { LogService, LogEvent } from './../common/log.service';
import { exerciseLogServiceConfigProvider } from './../common/log.service.provider';
import { QHistory } from './qhistory.component';
import { QContentComponent } from '../test/qcontent.component';
import { CodeMirrorComponent } from '../common/codemirror.component';
//import {FeedbackComponent} from '../common/feedback.component';
import { WINDOW, WINDOW_PROVIDERS } from './../common/window.service';
import { exerciseCodeRunnerServiceConfigProvider } from './../common/coderunner.service.provider';

import { Exercise } from './models/exercise';
import { ExerciseQuestion } from './models/question';
import { FeedbackComponent } from './../common/feedback.component';

@Component({
  selector: 'exercise-app',
  host: {
    '(window:focus)': 'onFocus()',
    '(window:blur)': 'onBlur()',
  },
  template: `<div
      *ngIf="_exercise"
      class="container-fluid bg-light d-flex justify-content-between align-items-center"
    >
      <h1>{{ _exercise.title }}</h1>
      <div>
        <div class="btn-group">
          <div *ngIf="_question" class="btn-group" role="group">
            <button
              [disabled]="_question.ordinal <= 1"
              (click)="previousQuestion()"
              class="btn btn-light"
            >
              Previous question
            </button>
            <button
              (click)="submitAnswer()"
              class="btn"
              [class.btn-success]="_correctStatus == true && _showCorrectStatus"
              [class.btn-danger]="_correctStatus == false && _showCorrectStatus"
              [class.btn-primary]="!_showCorrectStatus"
              [disabled]="_showCorrectStatus || _currAnswers[_question.ordinal - 1].length === 0"
            >
              Submit answer
            </button>
            <button
              [disabled]="_question.ordinal >= _latestQuestionOrdinal"
              (click)="nextQuestion()"
              class="btn btn-light"
            >
              Next question
            </button>
            <button
              [disabled]="_question.ordinal < _latestQuestionOrdinal"
              (click)="skipQuestion()"
              class="btn btn-danger"
            >
              Skip question
            </button>
          </div>
        </div>
      </div>
    </div>
    <div
      *ngIf="_exercise && _exercise.description"
      class="container-fluid bg-light d-flex justify-content-between"
    >
      <p>{{ _exercise.description }}</p>
    </div>
    <div
      *ngIf="_exercise"
      class="container-fluid bg-light d-flex justify-content-between border-bottom mb-3 pb-3"
    >
      <div>
        <q-history
          [currQuestionOrdinal]="(_question && _question.ordinal) || 1"
          [currAnswers]="_currAnswers"
          [questionDifficultyColors]="_questionDifficultyColors"
          (questionSelected)="onQuestionSelected($event)"
        >
        </q-history>
      </div>

      <div class="btn-group border rounded p-1">
        <div
          *ngFor="#dl of _exercise.difficultyLevels"
          class="form-check"
          data-edgar-color-underline-select
        >
          <input
            type="radio"
            name="difficulty"
            [value]="dl.id"
            [checked]="_exercise.adaptivityProgressTracker.currentDifficultyLevelId === dl.id"
          />
          <label
            class="form-check-input"
            style="border-color: #{{ dl.rgbColor }}"
            (click)="onDifficultyLevelChange(dl.id)"
            >{{ dl.name }}</label
          >
        </div>
      </div>
    </div>
    <div *ngIf="_intro && _exercise" class="container">
      <h3>Welcome to the exercise</h3>
      <p>
        In this exercise, there are
        {{ _exercise.difficultyLevels.length }} difficulty levels that you can see in the upper
        right corner.
      </p>
      <p>
        You will be given questions starting with the lowest difficulty level. As you progress with
        questions, based on your success rate, you can level up or level down to another difficulty
        level. You can also change the difficulty level by yourself by clicking the desired
        Difficulty level so the next question you get will match the chosen difficulties. If there
        are no more questions from the current Difficulty level left, you will be prompted to pick
        another difficulty level order to get another question. All the questions will be listed by
        number in the horizontal question navigation bar and their text color indicates their
        difficulty level. The goal is to get proficient (have a decent success rate when providing
        answers) at questions with the highest difficulty level. Feel free to switch to a higher
        difficulty level if you think the provided questions are not enough of a challenge to you.
        You can skip questions, but please provide a textual feedback if you think the question is
        problematic. We appreciate your feedback
      </p>
      <p>Good luck and have fun!</p>
      <button class="btn btn-lg btn-primary" (click)="closeIntro()">Understood</button>
    </div>
    <div *ngIf="_finished" class="container">
      <h2 class="text-success">Congratulations!</h2>
      <p>You have successfully finished this exercise!</p>
      <p>
        You may return to the
        <a href="/exercise">list of available exercises for this course</a> or review questions and
        answers by using the question navigation.
      </p>
    </div>
    <div *ngIf="_exhaustedDifficultyLevelMessage" class="container">
      <h3>
        There are no more questions for the
        <span
          style="border-bottom: 2px solid #{{ _exercise.getCurrentDifficultyLevel().rgbColor }}"
          >{{ _exercise.getCurrentDifficultyLevel().name }}</span
        >
        difficulty level
      </h3>
      <p [innerHTML]="exhaustedDifficultyLevelsStatus()"></p>
    </div>
    <div *ngIf="_question && !_intro" class="question">
      <div class="col-12">
        <q-content
          [currAnswers]="_currAnswers"
          [currQuestion]="_question"
          [cmSkin]="_exercise.cmSkin"
          (answerUpdated)="onAnswerUpdated($event)"
          (answerEvaluated)="onAnswerEvaluated($event)"
        >
        </q-content>
      </div>
    </div>
    <feedback
      *ngIf="_exercise"
      [open]="_openFeedback"
      [title]="getFeedbackTitle()"
      [commentRequired]="true"
      [maxRatingScore]="5"
      [exerciseId]="_exercise.id"
      [exerciseQuestionOrdinal]="_question && _question.ordinal"
    ></feedback>`,
  styles: [
    `
      #edgar-logo {
        float: left;
        cursor: pointer;
      }
      #feedback {
        float: right;
      }
      .form-check {
        min-width: 60px;
      }
      .form-check[data-edgar-color-underline-select] {
        margin: -5px 5px;
        display: inline-block;
      }
      .form-check[data-edgar-color-underline-select] input[type='radio'] {
        display: none;
      }
      .form-check[data-edgar-color-underline-select] input[type='radio'] + label {
        cursor: pointer;
        border-bottom-style: solid;
        border-bottom-width: 2px;
        padding: 3px;
      }
      .form-check[data-edgar-color-underline-select] input[type='radio']:checked + label {
        font-weight: bold;
        border-width: 10px;
        padding: 0;
      }
    `,
  ],
  providers: [
    ExerciseService,
    LogService,
    exerciseLogServiceConfigProvider,
    exerciseCodeRunnerServiceConfigProvider,
    WINDOW_PROVIDERS,
  ],
  directives: [FeedbackComponent, QHistory, QContentComponent, CodeMirrorComponent],
})
@Injectable()
export class AppComponent {
  _intro: boolean = true;
  _exercise: Exercise;
  _question: ExerciseQuestion;
  _latestQuestionOrdinal: number = 1;
  _currAnswers: any[] = [];
  _questionDifficultyColors: any[] = [];
  _finished: boolean = false;
  _showCorrectStatus: boolean = false;
  _correctStatus: boolean;
  _exhaustedDifficultyLevelIds: number[] = [];
  _exhaustedDifficultyLevelMessage: boolean = false;
  _openFeedback: boolean = false;
  _skippedQuestionOrdinals: number[] = [];
  _win: Window;

  constructor(
    private _exerciseService: ExerciseService,
    private _logService: LogService,
    win: WINDOW
  ) {
    this._win = win.nativeWindow;
    const path = window.location.pathname.split('/');
    const exerciseId = parseInt(path[2]);

    this._exerciseService.startExercise(exerciseId).subscribe((status) => {
      this._exerciseService.getExercise(exerciseId).subscribe((exercise) => {
        this._exercise = exercise;
        this._latestQuestionOrdinal = status.latestQuestionOrdinal;

        if (status.currentAnswers && status.currentAnswers.length >= status.latestQuestionOrdinal) {
          this._currAnswers = status.currentAnswers;
        } else {
          this._currAnswers = [];
          for (let i = 0; i < this._latestQuestionOrdinal; ++i) {
            this._currAnswers.push([]);
          }
        }

        if (status.currentDifficultyLevelId) {
          this._exercise.adaptivityProgressTracker.currentDifficultyLevelId =
            status.currentDifficultyLevelId;
        }

        this._currAnswers = this._currAnswers.slice();
        this.goToQuestion(status.latestQuestionOrdinal);
      });
    });
  }

  onAnswerUpdated($newValue) {
    this._showCorrectStatus = false;
    this._currAnswers[this._question.ordinal - 1] = $newValue;
    this._currAnswers = this._currAnswers.slice(); // :( to force ng onchange
    this._logService.log(
      new LogEvent(
        'Answer updated',
        `question ordinal: ${this._question.ordinal}, answer: ${$newValue}`
      )
    );
    this._exerciseService.updateAnswers(this._currAnswers);
  }

  onAnswerEvaluated($event) {
    //console.log('answer evaluated', $event);
    if ($event.success) {
      this.attemptProgress($event.rs.score.is_correct);
    }
  }

  onFocus() {
    this._logService.log(new LogEvent('Got focus', ''));
  }

  onBlur() {
    this._logService.log(new LogEvent('Lost focus', ''));
  }

  onQuestionSelected($event) {
    this._logService.log(new LogEvent('Question selected', 'ordinal: ' + $event.ordinal));
    this.goToQuestion($event.ordinal);
  }

  onDifficultyLevelChange($newLevelId) {
    this._exhaustedDifficultyLevelMessage = false;
    this._exercise.adaptivityProgressTracker.setDifficultyLevel($newLevelId);
    this._exerciseService.updateDifficultyLevel($newLevelId);
    this.goToQuestion(this._latestQuestionOrdinal);
  }

  submitAnswer() {
    this._exerciseService
      .submitAnswer(this._question.ordinal, this._currAnswers[this._question.ordinal - 1])
      .subscribe((result) => {
        this.attemptProgress(result.correct);
      });
  }

  attemptProgress(questionCorrect) {
    this._correctStatus = questionCorrect;
    this._showCorrectStatus = true;
    const apt = this._exercise.adaptivityProgressTracker;
    const finished = apt.attempt(this._question.ordinal, questionCorrect);

    if (finished) {
      this.markAsFinished();
    } else if (questionCorrect && this._question.ordinal === this._latestQuestionOrdinal) {
      ++this._latestQuestionOrdinal;
    }
  }

  goToQuestion(ordinal: number) {
    if (!this._question || (this._question && this._question.ordinal !== ordinal)) {
      this._openFeedback = false;
      this._exerciseService.getQuestion(ordinal, this._currAnswers[ordinal - 1]).subscribe((q) => {
        if (q !== null) {
          this._question = q;
          this._showCorrectStatus = false;
          this._questionDifficultyColors[q.ordinal - 1] = this._exercise.difficultyLevels.find(
            (dl) => dl.id === q.difficultyLevelId
          ).rgbColor;

          if (this._currAnswers.length < ordinal) {
            this._currAnswers[q.ordinal - 1] = [];
            this._currAnswers = this._currAnswers.slice();
          }

          if (this._latestQuestionOrdinal < this._question.ordinal) {
            this._latestQuestionOrdinal = this._question.ordinal;
          }
        } else {
          const currDiffLevelId = this._exercise.adaptivityProgressTracker.currentDifficultyLevelId;
          if (!this._exhaustedDifficultyLevelIds.includes(currDiffLevelId)) {
            this._exhaustedDifficultyLevelIds.push(currDiffLevelId);
          }

          this._question = null;
          this._exhaustedDifficultyLevelMessage = true;
          if (this.isFinishedCauseExhausted()) {
            this.markAsFinished();
          }
        }
      });
    }
  }

  previousQuestion() {
    this.goToQuestion(this._question.ordinal - 1);
  }

  nextQuestion() {
    this.goToQuestion(this._question.ordinal + 1);
  }

  skipQuestion() {
    if (this._skippedQuestionOrdinals.includes(this._question.ordinal)) {
      this.nextQuestion();
    } else {
      this._openFeedback = true;
      this._skippedQuestionOrdinals.push(this._question.ordinal);
    }
  }

  isFinishedCauseExhausted() {
    return (
      this._exhaustedDifficultyLevelIds.sort().join(',') ===
      this._exercise.difficultyLevels
        .map((dl) => dl.id)
        .sort()
        .join(',')
    );
  }

  exhaustedDifficultyLevelsStatus() {
    if (this.isFinishedCauseExhausted()) {
      return `You have passed all the questions available from the exercise.`;
    } else {
      return `Please select another difficulty level.`;
    }
  }

  getFeedbackTitle() {
    let title = [];

    if (this._exercise) {
      title.push(this._exercise.title);
    }

    if (this._question) {
      title.push(`Question ${this._question.ordinal}`);
    }

    return title.join(' - ');
  }

  markAsFinished() {
    this._finished = true;
    this._question = null;
    this._exerciseService.markAsFinished();
  }

  closeIntro() {
    this._intro = false;
  }
}
