export class DifficultyLevel {
  constructor(
    public id: number,
    public name: string,
    public ordinal: number,
    public rgbColor: string
  ) {}
}
