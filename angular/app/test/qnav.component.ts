import {
  Component,
  Input,
  OnInit,
  EventEmitter,
  Output,
  OnChanges,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from 'angular2/core';
// import {Router, RouteParams, ROUTER_DIRECTIVES} from 'angular2/router'; // CanDeactivate
import { Question } from './../common/question';
import { Test } from './test.service';

@Component({
  selector: 'q-nav',
  template: `
    <span *ngIf="!currTest"
      ><i class="fa fa-2 fa-spinner fa-spin"></i>please wait....<i
        class="fa fa-2 fa-spinner fa-spin"
      ></i
    ></span>

    <ng-container *ngIf="currTest && !currTest.forward_only">
      <span
        *ngFor="#a of currAnswers; #i = index"
        class="aii-qn aii-cursor"
        [class.selected]="isSelected(i)"
        (click)="onSelect(i)"
      >
        <span class="badge">{{ i + 1 }}(<span [innerHTML]="getLabel(a)"></span>)</span>
      </span>
    </ng-container>

    <div *ngIf="currTest && currTest.forward_only">
      <span *ngFor="#a of currAnswers; #i = index" class="aii-qn" [class.selected]="isSelected(i)">
        <span class="badge">{{ i + 1 }}(<span [innerHTML]="getLabel(a)"></span>)</span></span
      >
      <div class="forward-only-div">
        <button
          class="btn btn-warning forward-only-btn"
          title="This is a forward-only test, it is not allowed to go back. Once you click this button you cannot change your answer to this question anymore."
          [disabled]="isLast() || waitSec > 0"
          (click)="next()"
        >
          <i class="fas fa-forward"></i>&nbsp;
          {{ waitSec > 0 ? '' + waitSec : "Next question (can't go back!)" }}
          &nbsp;<i class="fas fa-forward"></i>
        </button>
      </div>
    </div>
  `,
  styles: [
    `
      .aii-qn {
        color: #36363c;
        font-size: 1.5rem;
        margin-right: 3px;
        margin-bottom: 3px;
        padding: 3px;
        border-radius: 0.25rem;
      }
      .aii-cursor {
        cursor: pointer;
      }
      .forward-only-div {
        margin-top: 10px;
      }
      .forward-only-btn {
        border: 1px solid black;
        color: black;
      }
      .selected {
        border: black solid 2px;
        background-color: aliceblue;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QNavComponent implements OnInit, OnChanges {
  // routerCanDeactivate(n, p)
  private _idSelected: number;
  @Input() currAnswers: any[];
  @Input() currTest: Test;
  @Output() questionSelected = new EventEmitter();
  private FW_ONLY_WAIT_SEC: number = 20;
  private waitSec: number;

  constructor(private _ref: ChangeDetectorRef) {
    this.waitSec = 0;
  }
  getLabel(a: any): string {
    return Question.getAnswerLabel(a);
  }

  isSelected(i: number) {
    return i === this._idSelected;
  }

  next() {
    this._idSelected++;
    this.currTest.last_ordinal++;
    this.questionSelected.emit({ ordinal: this._idSelected + 1 });
    if (!this.isLast()) {
      this.waitSec = this.FW_ONLY_WAIT_SEC;
      this.countDown();
    }
  }

  countDown() {
    let self = this;
    this._ref.markForCheck();
    if (self.waitSec > 0) {
      setTimeout(function () {
        self.waitSec--;
        self.countDown();
      }, 1000);
    }
  }

  isLast() {
    return this.currAnswers.length === 1 + this._idSelected;
  }
  onSelect(i: number) {
    this._idSelected = i;
    this.questionSelected.emit({ ordinal: i + 1 });
  }
  ngOnInit() {
    this._idSelected = this.currTest.forward_only ? this.currTest.last_ordinal - 1 : 0;
  }
  ngOnChanges() {}
}
