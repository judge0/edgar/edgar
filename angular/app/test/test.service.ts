import { Injectable } from 'angular2/core';
import { Http, Headers } from 'angular2/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';
import { LogService, LogEvent } from './../common/log.service';
import { Question } from './../common/question';
import { Answer } from '../common/answer';
import { RunStats } from '../common/coderunner.service';
@Injectable()
export class TestService {
  private _questions: Question[];
  private _test: Test;
  constructor(private _http: Http, private _logService: LogService) {
    this._questions = [];
  }
  hasQuestion(num: number): boolean {
    return !!this._questions[num];
  }
  getCachedQuestion(num: number) {
    return this.hasQuestion(num) ? this._questions[num] : null;
  }
  getQuestion(num: number) {
    if (this._questions[num]) {
      return Observable.of(this._questions[num]);
    } else {
      this._logService.log(new LogEvent('Get question', 'num:' + num));
      return this._http.get(`/test/question/${num}`).map((res) => {
        let q = res.json();
        this._questions[num] = new Question(
          q.ordinal.toString(),
          q.ordinal,
          q.answers,
          q.content,
          q.answers,
          q.multiCorrect,
          q.modeDesc,
          q.type,
          q.programmingLanguages,
          q.attachments,
          q.scaleItems,
          q.can_upload
        );
        this._logService.log(new LogEvent('Received question', 'num:' + q.ordinal));
        return this._questions[num];
      });
    }
  }
  /*    getStaticQuestion1() {
        return new Question("1", 1, [], '<i class="fa fa-2 fa-spinner fa-spin"></i> please wait... ---', [], false, undefined, 'SQL');
    }*/

  getTest() {
    if (this._test) {
      return Observable.of(this._test);
    } else {
      this._logService.log(new LogEvent('Get test', '-'));
      return this._http.get(`/test/instance`).map((res) => {
        let t = res.json();
        if (t.success === false) {
          this._logService.log(new LogEvent('Received NULL test', JSON.stringify(t)));
          return null;
        } else {
          if (typeof t.currAnswers === 'string') {
            t.currAnswers = JSON.parse(t.currAnswers);
          }
          /*if (typeof t.currCodeAnswers === 'string') {
                            t.currCodeAnswers = JSON.parse(t.currCodeAnswers);
                        }*/
          // for (let i = 0; i < t.noOfQuestions; i++) {
          //     let answer = new Answer();
          //
          //     currentAnwers.push(answer)
          // }
          this._test = new Test(
            t.noOfQuestions,
            t.secondsLeft,
            t.prolonged,
            t.title,
            t.currAnswers,
            t.student,
            t.alt_id,
            t.alt_id2,
            t.cmSkin ? t.cmSkin : 'ambinace',
            t.imgSrc,
            t.is_competition,
            t.upload_file_no,
            t.forward_only,
            t.last_ordinal,
            t.type_name,
            t.runStats,
            t.uses_ticketing
          );
          this._logService.log(new LogEvent('Received test', JSON.stringify(this._test)));
          return this._test;
        }
      });
    }
  }
  submitTest(answers2) {
    this._logService.log(new LogEvent('Submitting test', JSON.stringify(answers2)));
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this._http
      .post(`/test/submit`, JSON.stringify({ answers: answers2 }), {
        headers: headers,
      })
      .map((res) => {
        var rv = res.json();
        this._logService.log(new LogEvent('Test submitted', JSON.stringify(rv)));
        return rv;
      });
  }

  getScoreboard() {
    this._logService.log(new LogEvent('Get scoreboard', '-'));
    return this._http.get(`/test/scoreboard/`).map((res) => {
      let scoreboard = res.json();

      this._logService.log(new LogEvent('Received scoreboard', scoreboard));

      console.log('scoreboard', scoreboard);
      return scoreboard;
    });
  }
}
export class Test {
  constructor(
    public noOfQuestions: number,
    public secondsLeft: number,
    public prolonged: boolean,
    public title: string,
    public currAnswers: Answer[],
    public student: string,
    public alt_id: string,
    public alt_id2: string,
    public cmSkin: string,
    public imgSrc: string,
    public is_competition: boolean,
    public upload_file_no: number,
    public forward_only: boolean,
    public last_ordinal: number,
    public type_name: string,
    public runStats: RunStats[],
    public usesTicketing: boolean
  ) {
    if (!(this.currAnswers && this.currAnswers.length > 0)) {
      this.currAnswers = [];
      for (let i = 0; i < this.noOfQuestions; ++i) {
        this.currAnswers.push(new Answer());
      }
    }
  }
  isPA2() {
    return this.type_name === 'Peer assessment PHASE2';
  }
}
