import {
  Component,
  OnInit,
  ElementRef,
  OnChanges,
  AfterViewInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectorRef,
} from 'angular2/core';
import { CodeMirrorComponent } from './../common/codemirror.component';
import { JSONResultComponent } from './../common/jsonresult/jsonresult.component';
import { CLangResultComponent } from './../common/clangresult/clangresult.component';
import { GridComponent } from './../common/grid/grid.component';
import { LogService, LogEvent } from './../common/log.service';
import { Question } from './../common/question';
import { CodeRunnerService, CodeResult } from './../common/coderunner.service';
import { Answer, CodeAnswer } from '../common/answer';
declare var Swal: any;
@Component({
  selector: 'q-code',
  template: ` <div class="row">
      <div class="col-12">
        <div
          *ngIf="currQuestion.programmingLanguages && currQuestion.programmingLanguages.length > 1"
          class="pull-right"
        >
          <div class="form-inline">
            <label for="question-answer-programming-language">
              <small>Language:</small>
            </label>
            <select class="form-control-sm" (change)="onLanguageChanged($event.target.value)">
              <option
                *ngFor="#language of currQuestion.programmingLanguages; #i = index"
                [value]="language.id"
                [selected]="getCurrLangId() === language.id"
              >
                {{ language.name }}
              </option>
            </select>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="edgar-overlap border">
          <code-mirror
            [currCode]="getCurrCode()"
            [questionType]="currQuestion.type"
            [programmingLanguages]="currQuestion.programmingLanguages"
            [ordinal]="currQuestion.ordinal"
            [langId]="getCurrLangId()"
            [cmSkin]="cmSkin"
            (runCodeHotkey)="run($event)"
            (codeChanged)="onCodeChanged($event)"
          ></code-mirror>
          <div class="edgar-right-overlap">
            <button
              class="btn btn-warning col-1 edgar-overlap-btn-w"
              type="submit"
              [disabled]="queryRunning || !canRun()"
              title="Ctrl+Enter"
              (click)="runAll($event)"
            >
              <span [innerHTML]="getRunLabel()"></span>
            </button>
          </div>
          <div class="edgar-right-overlap2">
            <button
              class="btn btn-success col-1 edgar-overlap-btn-w"
              type="submit"
              [disabled]="saved"
              (click)="save($event)"
            >
              Save
            </button>
          </div>
        </div>
      </div>
    </div>
    <div
      *ngIf="currQuestion.attachments && currQuestion.attachments.length > 0"
      class="row"
      style="margin-top: 20px"
    >
      <div class="form-group">
        <label for="attachments_row">Attachments:</label>
        <div id="attachments_row" class="filemanager">
          <div class="row" style="margin-left: 0;">
            <div class="data" style="display: block;">
              <div>
                <div class="row">
                  <div *ngFor="#attachment of currQuestion.attachments">
                    <div
                      *ngIf="attachment.is_public === true"
                      class="col-sm-2 resultitem resultitemFile"
                    >
                      <div class="details detailsFiles">
                        <a
                          style="display: table-row"
                          href="/upload/attachment/{{ attachment.filename }}"
                          target="_blank"
                          class="files visible"
                          download
                        >
                          <div class="icon-pos">
                            <i class="file-icon fa fa-file"></i>
                          </div>
                          <div class="file-info">
                            <div class="file_name" style="display: table-cell">
                              <span class="name">{{ attachment.label }}</span>
                            </div>
                            <div class="file_size">
                              <span class="details"></span>
                            </div>
                          </div>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div *ngIf="getResult()" class="mt-2">
      <div *ngIf="getResult().success">
        <ng-container *ngIf="isSQL()">
          <grid [result]="getResult()"></grid>
        </ng-container>
        <ng-container *ngIf="isCodeLang()">
          <clang-result [isRevision]="false" [result]="getResult()"></clang-result>
        </ng-container>
        <ng-container *ngIf="isJSON()">
          <json-result
            [isRevision]="false"
            [queryRunning]="queryRunning"
            [result]="getResult()"
          ></json-result>
        </ng-container>
      </div>
      <div *ngIf="!getResult().success">
        <div class="text-left aii-error">
          {{ getResult().error.message }}
          <div *ngIf="getResult().error.position">Position: {{ getResult().error.position }}</div>
        </div>
      </div>
    </div>`,
  styles: [``],
  directives: [CodeMirrorComponent, GridComponent, CLangResultComponent, JSONResultComponent],
  providers: [LogService, CodeRunnerService],
})
export class QCodeComponent implements OnInit, OnChanges, AfterViewInit {
  // <q-code [currAnswers]="currAnswers" [currQuestion]="_currQuestion" [ordinal]="currQuestion.ordinal" (answerUpdated)="onAnswerUpdated($event)"></q-code>
  saved: boolean;
  @Input() currQuestion: Question;
  @Input() currQuestionWait: number;
  @Input() currAnswers: Answer[];
  @Input() cmSkin: string;
  @Input() isPlayground: boolean;
  @Output() answerUpdated = new EventEmitter();
  @Output() codeRun = new EventEmitter();
  //@Output() codeChanged = new EventEmitter();
  //result : CodeResult;
  currResults: any;
  queryRunning: boolean;
  questionLangCode: Map<number, Map<number, string>> = new Map();

  constructor(private _ref: ChangeDetectorRef, private _codeRunnerService: CodeRunnerService) {
    this.saved = true;
    this.currResults = {};
    this.queryRunning = false;
  }

  ngOnInit() {}

  ngAfterViewInit() {}

  getResult(): CodeResult {
    return this.currResults[this.currQuestion.ordinal - 1];
  }

  isSQL(): boolean {
    return this.currQuestion.type.toUpperCase().indexOf('SQL') >= 0;
  }

  isCodeLang(): boolean {
    return (
      this.currQuestion.type.toUpperCase().indexOf('C-LANG') >= 0 ||
      this.currQuestion.type.toUpperCase().indexOf('JAVA') >= 0 ||
      this.currQuestion.type.toUpperCase().indexOf('CODE') >= 0
    );
  }

  isJSON(): boolean {
    return this.currQuestion.type.toUpperCase().indexOf('JSON') >= 0;
  }

  getRunLabel() {
    if (this.queryRunning) {
      return '<i class="fa fa-2 fa-spinner fa-spin"></i>';
    } else if (this.currQuestionWait > 1e6) {
      return '<i class="fas fa-infinity" title="Quoth the Raven “Nevermore.”"></i>';
    } else if (this.currQuestionWait > 0) {
      return Math.round(this.currQuestionWait);
    } else {
      return 'Run';
    }
  }
  canRun() {
    return !(this.currQuestionWait && this.currQuestionWait > 0);
  }
  runAll($event) {
    this.run(this.getCurrCode());
  }

  run(code) {
    if (this.canRun()) {
      var self = this;
      this.save();
      this.queryRunning = true;
      this.currResults[this.currQuestion.ordinal - 1] = undefined;
      this._codeRunnerService
        .getCodeResult(
          this.currQuestion.ordinal,
          code,
          '' + this.getCurrLangId(),
          this.isPlayground
        )
        .subscribe((result) => {
          this.currResults[this.currQuestion.ordinal - 1] = result;

          //this.currQuestion.runs = result.runStats ? result.;
          this.queryRunning = false;
          this.codeRun.emit(result);
          self._ref.markForCheck();
        });
    } else {
      Swal.fire({
        title: 'Can not run code',
        icon: 'error',
        html: 'You can not run code (just now). Please wait while the button becomes enabled...',
        showCloseButton: true,
        showCancelButton: false,
        focusConfirm: false,
        confirmButtonText: '<i class="fa fa-thumbs-up"></i> OK',
        confirmButtonAriaLabel: 'OK',
      });
    }
  }

  save() {
    // Will always save the entire contents, not just the selected part (that might come through the $event, ie runHotKey event)
    this.answerUpdated.emit(this.currAnswers[this.currQuestion.ordinal - 1]);
    this.saved = true;
  }

  onCodeChanged($event) {
    this.saved = false;
    if (!this.currAnswers[this.currQuestion.ordinal - 1].codeAnswer) {
      this.currAnswers[this.currQuestion.ordinal - 1].codeAnswer = new CodeAnswer();
    }
    let currCodeAnswer = this.currAnswers[this.currQuestion.ordinal - 1].codeAnswer;
    currCodeAnswer.code = $event;

    if (currCodeAnswer.languageId && currCodeAnswer.code) {
      this.questionLangCode
        .get(this.currQuestion.ordinal)
        .set(currCodeAnswer.languageId, currCodeAnswer.code);
    }
  }

  onLanguageChanged(newLanguageId) {
    this.saved = false;
    let currCodeAnswer = this.currAnswers[this.currQuestion.ordinal - 1].codeAnswer;

    currCodeAnswer.languageId = parseInt(newLanguageId);

    if (
      !this.questionLangCode.get(this.currQuestion.ordinal).has(currCodeAnswer.languageId) &&
      this.currQuestion.programmingLanguages &&
      this.currQuestion.programmingLanguages.length
    ) {
      this.questionLangCode
        .get(this.currQuestion.ordinal)
        .set(
          currCodeAnswer.languageId,
          this.currQuestion.programmingLanguages.find((pl) => pl.id === currCodeAnswer.languageId)
            .hello_world + '\n\n\n\n\n'
        );
    }

    this.onCodeChanged(
      this.questionLangCode.get(this.currQuestion.ordinal).get(currCodeAnswer.languageId)
    );
  }

  getCurrCode() {
    if (
      this.currAnswers[this.currQuestion.ordinal - 1].codeAnswer &&
      this.currAnswers[this.currQuestion.ordinal - 1].codeAnswer.code
    ) {
      return this.currAnswers[this.currQuestion.ordinal - 1].codeAnswer.code;
    } else {
      return '\n\n\n\n\n';
    }
  }

  getCurrLangId() {
    if (this.currAnswers[this.currQuestion.ordinal - 1].codeAnswer) {
      // this should always end in this branch, but for legacy issues, there is an else branch
      return this.currAnswers[this.currQuestion.ordinal - 1].codeAnswer.languageId;
    } else if (this.currQuestion.programmingLanguages) {
      // this is reserved for CONTINUED test thata did not have lang ids
      console.log('recovering legacy tests...');
      return this.currQuestion.programmingLanguages[0].id;
    } else {
      return undefined;
    }
  }

  ngOnChanges(changes) {
    if (
      changes.currQuestion &&
      changes.currQuestion.currentValue &&
      this.currAnswers[this.currQuestion.ordinal - 1].codeAnswer &&
      this.currAnswers[this.currQuestion.ordinal - 1].codeAnswer.languageId
    ) {
      if (!this.questionLangCode.has(this.currQuestion.ordinal)) {
        this.questionLangCode.set(this.currQuestion.ordinal, new Map<number, string>());

        if (this.currAnswers[this.currQuestion.ordinal - 1].codeAnswer.code) {
          this.questionLangCode
            .get(this.currQuestion.ordinal)
            .set(
              this.currAnswers[this.currQuestion.ordinal - 1].codeAnswer.languageId,
              this.currAnswers[this.currQuestion.ordinal - 1].codeAnswer.code
            );
        } else {
          this.onLanguageChanged(
            this.currAnswers[this.currQuestion.ordinal - 1].codeAnswer.languageId
          );
        }
      }
    }
  }
}
