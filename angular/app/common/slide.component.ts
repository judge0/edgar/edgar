import { ChangeDetectionStrategy, Component, Input } from 'angular2/core';

@Component({
  selector: 'slider',
  template: `
    <div class="parent">
      <div><ng-content select="[leftPane]"></ng-content></div>
      <div><ng-content select="[rightPane]"></ng-content></div>
    </div>
  `,
  styles: [
    `
      :host {
        display: block;
        overflow: hidden; /* Hide everything that doesn't fit compoennt */
      }
      .parent {
        height: 100%;
        width: 200%; /* Make the parent element to take up twice
                               of the component's width */
        display: flex; /* Align all children in a row */
        div {
          flex: 1;
        } /* Evenly divide width between children */
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SlidePanelComponent {
  @Input() activePane: PaneType = 'left';
}

type PaneType = 'left' | 'right';
