import { Inject, Injectable, OpaqueToken } from 'angular2/core';
import { Http, Headers } from 'angular2/http';
import 'rxjs/add/operator/map';

export const LOG_SERVICE_CONFIG = new OpaqueToken('log.service.config');

export interface LogServiceConfig {
  logUrl: string;
  hbUrl: string;
}

export const EXERCISE_LOG_SERVICE_CONFIG: LogServiceConfig = {
  logUrl: '/exercise/log',
  hbUrl: '/exercise/hb',
};

export const TEST_LOG_SERVICE_CONFIG: LogServiceConfig = {
  logUrl: '/test/log',
  hbUrl: '/test/hb',
};

export const TUTORIAL_LOG_SERVICE_CONFIG: LogServiceConfig = {
  logUrl: '/tutorial/log',
  hbUrl: '/tutorial/hb',
};

declare var Swal: any;

@Injectable()
export class LogService {
  constructor(private _http: Http, @Inject(LOG_SERVICE_CONFIG) private _config: LogServiceConfig) {}
  log(event: LogEvent) {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    // console.log('saving event:', event);
    this._http
      .post(this._config.logUrl, JSON.stringify({ e: event }), {
        headers: headers,
      })
      .subscribe((res) => {
        //console.log('Server (log) returned: ',  res.json());
      });
  }

  logLecture(event: LogEvent) {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    //console.log('saving event:', event.eventName);
    this._http
      .post(`/lecture/log`, JSON.stringify({ e: event }), {
        headers: headers,
      })
      .subscribe((res) => {
        //console.log('Server (log) returned: ',  res.json());
      });
  }

  // heartbeat
  hb() {
    this._http
      .post(this._config.hbUrl, '"Trying is the 1st step towards failure." H.Simpson')
      .subscribe((res) => {
        var r = res.json();
        if (r.error) {
          alert('Edgar says: ' + r.error);
          Swal.fire({
            title: 'Error on heartbeat',
            icon: 'error',
            html: r.error,
            showCloseButton: true,
            showCancelButton: false,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> OK',
            confirmButtonAriaLabel: 'OK',
          });
        } else if (r.msg) {
          Swal.fire({
            title: r.msg.message_title,
            icon: 'info',
            html: r.msg.message_body,
            showCloseButton: true,
            showCancelButton: false,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> OK',
            confirmButtonAriaLabel: 'OK',
          });
          this.log(new LogEvent('Displayed test message.', r.msg.id));
        }
      });
  }
}

export class LogEvent {
  public clientTs: any; // WTF, typscript?
  constructor(public eventName: string, public eventData: string) {
    this.clientTs = new Date();
  }
}
