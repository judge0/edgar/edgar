import { Injectable } from 'angular2/core';
import { Http, Headers } from 'angular2/http';
import { Observable } from 'rxjs/Rx';
import { Feedback } from './feedback';

@Injectable()
export class FeedbackService {
  private _feedbackHistory: Feedback[] = [];

  constructor(private _http: Http) {}

  get(feedback: Feedback) {
    let previousFeedbacks = this._feedbackHistory.filter(
      (fb) =>
        fb.exerciseId == feedback.exerciseId &&
        fb.exerciseQuestionOrdinal == feedback.exerciseQuestionOrdinal &&
        fb.tutorialId == feedback.tutorialId &&
        fb.tutorialStepId == feedback.tutorialStepId
    );

    if (previousFeedbacks.length) {
      // console.log('using previous feedbacks');
      return Observable.of(previousFeedbacks);
    } else {
      // console.log('getting previous feedbacks from api');
      return this._http
        .get(
          `/feedback/api?eid=${feedback.exerciseId}&eqo=${feedback.exerciseQuestionOrdinal}&tid=${feedback.tutorialId}&tsid=${feedback.tutorialStepId}`
        )
        .map((res) => {
          const feedbacks = res.json();
          this._feedbackHistory.push(feedbacks);
          return feedbacks;
        });
    }
  }

  send(feedback: Feedback) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this._http.post(`/feedback/api`, JSON.stringify(feedback), {
      headers: headers,
    });
  }
}
