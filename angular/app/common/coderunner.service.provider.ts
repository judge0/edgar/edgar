import { Provider } from 'angular2/core';
import {
  CODERUNNER_SERVICE_CONFIG,
  EXERCISE_CODERUNNER_SERVICE_CONFIG,
  TEST_CODERUNNER_SERVICE_CONFIG,
  TUTORIAL_CODERUNNER_SERVICE_CONFIG,
} from './coderunner.service';

export const exerciseCodeRunnerServiceConfigProvider = new Provider(CODERUNNER_SERVICE_CONFIG, {
  useValue: EXERCISE_CODERUNNER_SERVICE_CONFIG,
});

export const testCodeRunnerServiceConfigProvider = new Provider(CODERUNNER_SERVICE_CONFIG, {
  useValue: TEST_CODERUNNER_SERVICE_CONFIG,
});

export const tutorialCodeRunnerServiceConfigProvider = new Provider(CODERUNNER_SERVICE_CONFIG, {
  useValue: TUTORIAL_CODERUNNER_SERVICE_CONFIG,
});
