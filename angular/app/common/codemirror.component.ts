import {
  Component,
  OnInit,
  ElementRef,
  OnChanges,
  AfterViewInit,
  Input,
  Output,
  EventEmitter,
} from 'angular2/core';
import { ProgrammingLanguage } from './question';
declare var CodeMirror: any;
@Component({
  selector: 'code-mirror',
  template: `<div style="display: flex">
    <textarea id="codeMirrorEditor" style="display: none"></textarea>
  </div>`,
  // ,
  //      styles: [
  //     `.CodeMirror {
  //         min-height: 200px;
  //         height: auto;
  //      }`
  //     ]
})
export class CodeMirrorComponent implements OnInit, OnChanges, AfterViewInit {
  _editor: any;
  _editorNativeElement: any;
  @Input() currCode: string;
  @Input() ordinal: number;
  @Input() langId: number;
  @Input() readOnly: boolean;
  @Input() cmSkin: string;
  @Input() questionType: string;
  @Input() programmingLanguages: ProgrammingLanguage[];
  @Output() codeChanged = new EventEmitter();
  @Output() runCodeHotkey = new EventEmitter();
  _prevOrdinal: number;
  _prevLangId: number;
  constructor(elRef: ElementRef) {
    this._editorNativeElement = elRef.nativeElement;
  }

  ngOnInit() {
    // console.log("CodeMirrorComponent on init");
  }

  ngAfterViewInit() {
    var self = this;
    this._editor = CodeMirror(this._editorNativeElement, {
      lineNumbers: true,
      indentUnit: this.questionType.toUpperCase().indexOf('SQL') >= 0 ? 4 : 3,
      tabSize: this.questionType.toUpperCase().indexOf('SQL') >= 0 ? 4 : 3,
      indentWithTabs:
        this.questionType.toUpperCase().indexOf('PYTHON') >= 0 ||
        (this.programmingLanguages &&
          this.programmingLanguages.find((x) => x.name.toUpperCase().indexOf('PYTHON') >= 0))
          ? true
          : false,
      mode:
        this.questionType.toUpperCase().indexOf('SQL') >= 0
          ? 'text/x-pgsql'
          : this.questionType.toUpperCase().indexOf('JAVA') >= 0
          ? 'text/x-java'
          : 'text/x-csrc',
      theme: this.cmSkin,
      value: this.currCode,
      viewportMargin: Infinity,
      readOnly: this.readOnly || false,
      extraKeys: {
        'Ctrl-Enter': function (cm) {
          var code = self._editor.getSelection();
          if (!code || 0 == code.length) {
            code = self._editor.getDoc().getValue();
          }
          self.runCodeHotkey.emit(code);
        },
      },
    });
    this._prevOrdinal = this.ordinal;
    this._prevLangId = this.langId;
    this._editor.on('change', (editor: any, change: any) => {
      var value = this._editor.getDoc().getValue();
      this.codeChanged.emit(value);
    });
    this._editor.on('focus', (editor: any, change: any) => {
      this._editor.refresh();
    });
  }
  ngOnChanges(changes: {}) {
    // This ordinal is to prevent inf-loop, by me emitting to parent, and parent sending back.
    // That is, I need to set this only when the q number or langId has changed.
    //console.log('cm onchanges', (this._editor) ? "set" : "undefined", this._prevOrdinal, this.ordinal, this._prevOrdinal === this.ordinal);
    if (this._editor && (this._prevOrdinal !== this.ordinal || this._prevLangId !== this.langId)) {
      this._editor.getDoc().setValue(this.currCode);
      this._prevOrdinal = this.ordinal;
      this._prevLangId = this.langId;
    }
  }
}
