import { Injectable } from 'angular2/core';
import { Http, Headers } from 'angular2/http';
import 'rxjs/add/operator/map';
import { ITicketService } from './ITicket';
import { LogEvent, LogService } from '../log.service';

@Injectable()
export class TicketService implements ITicketService {
  constructor(private _http: Http, private _logService: LogService) {}
  getTicketConversation(ordinal: number) {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this._http
      .get(`/ticket/conversation/${ordinal}`, {
        headers: headers,
      })
      .map((res) => {
        return res.json();
      });
  }
  isTicketResolved(): any {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this._http
      .get(`/ticket/isresolved`, {
        headers: headers,
      })
      .map((res) => {
        let msg = res.json();
        if (msg && msg.ticket_question_ordinal) {
          this._logService.log(
            new LogEvent('Displayed ticket message.', msg.ticket_question_ordinal)
          );
        }
        return msg;
      });
  }
  getTicketStatuses() {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this._http
      .get(`/ticket/statuses`, {
        headers: headers,
      })
      .map((res) => {
        let rv = res.json();
        return JSON.parse(rv.ticket_statuses);
      });
  }
  raiseTicket(ordinal: number, description: string) {
    let ticket = {
      ordinal: ordinal,
      description: description,
    };
    this._logService.log(new LogEvent('Submitting ticket', JSON.stringify(ticket)));
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this._http
      .post(`/ticket/raise`, JSON.stringify(ticket), {
        headers: headers,
      })
      .map((res) => {
        var rv = res.json();
        this._logService.log(new LogEvent('Submit ticket answer received', JSON.stringify(rv)));
        return rv;
      });
  }
}
