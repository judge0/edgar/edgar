import { Component, Input, AfterViewInit, Injectable, OnChanges } from 'angular2/core';
//import { ITicketService } from "./ITicket";
import { TicketService } from './ticket.service';
declare var Swal: any;
declare var $: any;
@Component({
  selector: 'ticket',
  template: `<div class="edgar-ticket-button d-flex flex-column" *ngIf="showComponent()">
    <button
      class="btn btn-dark"
      type="button"
      [disabled]="isTicketPending()"
      (click)="raiseTicket()"
    >
      <span style="font-size: 1.5rem; color:{{ getTicketColor() }}">
        <i class="fas fa-tools"></i>
      </span>
      <span *ngIf="isTicketPending()" style="font-size: 1.5rem; color:{{ getTicketColor() }}">
        <i class="fas fa-hourglass-half"></i>
      </span>
    </button>
    <button
      *ngIf="showPopover()"
      class="btn ticket-popover edgar-ticket-button"
      data-toggle="popover"
      title="Ticket conversation"
    >
      <span style="font-size: 2.0rem; color:green">
        <i class="fa fa-comment-dots"></i>
      </span>
    </button>
  </div>`,
  providers: [TicketService],
})
@Injectable()
export class TicketComponent implements OnChanges {
  @Input() ordinal: number;
  ticketStatuses: string[];
  refreshIntervalId: number;
  constructor(private _ticketService: TicketService) {
    this.refreshIntervalId = 0;
    this._ticketService.getTicketStatuses().subscribe((statuses) => {
      this.ticketStatuses = statuses;
      this.checkSetInterval();
      setTimeout(() => this.bindTicketPopover(), 200);
    });
  }
  showComponent() {
    if (this.ticketStatuses && this.ticketStatuses.length) {
      return true;
    } else {
      return false;
    }
  }
  showPopover() {
    return (
      this.ticketStatuses[this.ordinal - 1] === 'closed' ||
      this.ticketStatuses[this.ordinal - 1] === 'reopened'
    );
  }
  checkSetInterval() {
    let self = this;
    if (this.refreshIntervalId) clearInterval(this.refreshIntervalId);
    for (let i = 0; i < this.ticketStatuses.length; ++i) {
      if (this.ticketStatuses[i] && this.ticketStatuses[i].indexOf('open') >= 0) {
        this.refreshIntervalId = window.setInterval(() => {
          this._ticketService.isTicketResolved().subscribe((result) => {
            if (result.ticket_question_ordinal) {
              self.onTicketResolved(result.ticket_question_ordinal);
            }
          });
        }, 20 * 1000);
        break;
      }
    }
  }

  isTicketPending() {
    return this.getTicketColor() === 'red';
  }
  getTicketColor() {
    let status = this.ticketStatuses[this.ordinal - 1];
    if (status === 'open' || status === 'reopened') {
      return 'red';
    } else if (status === 'closed') {
      return 'limegreen';
    }
    return 'goldenrod';
  }

  ngOnChanges() {
    setTimeout(() => this.bindTicketPopover(), 200);
  }
  bindTicketPopover() {
    let self = this;
    $('.ticket-popover[data-toggle="popover"]').popover('dispose'); // give this guy a chance first, probably fails bcs does not exist any more
    $('div.popover').remove(); // then remove brute force
    let elem = $('.ticket-popover[data-toggle="popover"]');
    elem.popover({
      trigger: 'click',
      placement: 'bottom',
      html: true,
      content: function () {
        let popoverId = 'ticket-popover-' + Date.now();
        let popover = elem.data('bs.popover');
        let tip = $(popover.tip);
        tip.css('width', '100%');
        self._ticketService.getTicketConversation(self.ordinal).subscribe((resp) => {
          $('#' + popoverId).html(resp.conversation);
          popover._popper.update();
        });
        return (
          '<div id="' +
          popoverId +
          '"><i class="fa fa-2 fa-spinner fa-spin"></i>Please wait...</div>'
        );
      },
    });
  }
  onTicketResolved(ordinal) {
    Swal.fire({
      title: 'Ticket resolved',
      icon: 'info',
      html:
        'Your ticked for question ' +
        ordinal +
        ' has been resolved. <br>Please, click on the <span style="font-size: 2.0rem; color:green"><i class="fa fa-comment-dots"></i></span> icon to see the details.',
      showCloseButton: true,
      showCancelButton: false,
      focusConfirm: false,
      confirmButtonText: '<i class="fa fa-thumbs-up"></i> OK',
      confirmButtonAriaLabel: 'OK',
    });
    this.ticketStatuses[parseInt(ordinal) - 1] = 'closed';
    this.checkSetInterval();
    //this._test.ticketStatuses[parseInt(message.ticket_question_ordinal) - 1] = 'closed';
    setTimeout(() => this.bindTicketPopover(), 200);
  }
  raiseTicket() {
    let self = this;
    Swal.fire({
      title: '<strong>Raise ticket?</strong>',
      html: 'Do you really want to report a problem for this question?<br><i>If so, a teacher will be notified and once your issue is handled you will receive the answer here.</i>',
      showCloseButton: true,
      showCancelButton: true,
      focusConfirm: true,
      confirmButtonText: '<i class="fas fa-bug"></i> Yes, raise ticket',
      confirmButtonAriaLabel: 'Yes, raise ticket',
      cancelButtonText: 'Cancel',
      cancelButtonAriaLabel: 'Cancel',
      input: 'textarea',
      inputPlaceholder: 'Please describe your issue here ...',
      inputAttributes: {
        'aria-label': 'Type your message here',
      },
    }).then((result) => {
      // this result object does NOT behave as in the docs?
      if (result.value) {
        this._ticketService
          .raiseTicket(this.ordinal, result.value)
          .subscribe((raiseTicketResult) => {
            if (raiseTicketResult.success) {
              self.ticketStatuses[self.ordinal - 1] = 'open';
              //self._test.ticketStatuses[self._currQuestion.ordinal - 1] = 'open';
              self.checkSetInterval();
              Swal.fire({
                title: 'Ticket submitted!',
                icon: 'info',
                html: 'Ticket submitted.<br>Keep calm and proceed with your exam, you will be notified when your ticket is handled.',
                showCloseButton: true,
                showCancelButton: false,
                focusConfirm: false,
                confirmButtonText: '<i class="fa fa-thumbs-up"></i> OK',
                confirmButtonAriaLabel: 'OK',
              });
            } else {
              Swal.fire(
                'Sorry, error occured submiting ticket, please check your network and try again.'
              );
            }
          });
      }
    });
  }
}
