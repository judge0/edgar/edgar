export interface ITicketService {
  getTicketConversation(ordinal: number);
  raiseTicket(ordinal: number, description: string);
  getTicketStatuses();
  isTicketResolved();
}
// export interface ITicketListener {
//     onTicketResolved(message: any);
// }
