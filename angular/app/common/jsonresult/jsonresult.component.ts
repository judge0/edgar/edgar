import { Component, Input, OnChanges } from 'angular2/core';
import { CodeResult } from './../coderunner.service'; // to be changed
import { TimerObservable } from 'rxjs/observable/TimerObservable';

@Component({
  selector: 'json-result',
  template: `
    <br />
    <div *ngIf="result && result.score" style="margin-top:10px;">
      <div [innerHTML]="getHint()"></div>
      <hr />
    </div>
    <div *ngIf="isRunning()" class="text-center">
      <div class="text-center">
        <h2>Please wait, your answers (and correct answers) are being reevaluated.</h2>
      </div>
      <img
        src="/images/waiting.gif"
        alt="Patience, grasshoper"
        style="width: 480px; height: 317px; left: 0px; top: 0px;"
      />
    </div>
    <div *ngIf="result && result.score && result.score.hint && !result.score.is_correct">
      <div class="row">
        <div class="col-md-6">
          <h4>Result:</h4>
          <div [innerHTML]="getHtml()"></div>
        </div>
        <div class="col-md-6">
          <div [innerHTML]="getJSONHint()"></div>
        </div>
      </div>
    </div>
    <div *ngIf="!(result && result.score && result.score.hint && !result.score.is_correct)">
      <h4>Result:</h4>
      <div [innerHTML]="getHtml()"></div>
    </div>
    <div *ngIf="isRevision && c_eval_data">
      <a
        class="btn btn-primary"
        data-toggle="collapse"
        href="#loggedEvalData"
        role="button"
        aria-expanded="false"
        aria-controls="collapseExample"
      >
        See logged data
      </a>
      <div class="collapse" id="loggedEvalData">
        <div class="card card-body">
          <h3>Logged evaluation data (should match the above):</h3>
          <pre>{{ c_eval_data }}</pre>
        </div>
      </div>
    </div>
  `,
})
export class JSONResultComponent implements OnChanges {
  @Input() c_eval_data: any;
  @Input() result: CodeResult;
  @Input() queryRunning: boolean;
  parsed_eval_data: any;
  @Input() isRevision: boolean;
  // constructor() {
  // }
  isRunning(): boolean {
    // console.log('isRunning = ', !(this.result && this.result.codeResult));
    // console.log('this.result = ', this.result);
    return this.queryRunning;
  }
  isKidsMode(): boolean {
    return this.result.score.is_correct !== undefined;
  }
  getHint(): string {
    if (this.result.score.is_correct) {
      return '<span style="background-color:lime;"> <i class="far fa-smile" aria-hidden="true"></i> Correct! Well done! </span>';
    } else {
      return '<span style="background-color:lemonchiffon;"> <i class="far fa-frown" aria-hidden="true"></i> Missed it.</span>';
    }
  }
  getJSONHint(): string {
    return '<h4>Hint: </h4><pre>' + this.result.score.hint + '</pre>';
  }
  getHtml(): string {
    // console.log('getHtml');
    //console.log('this.result = ', this.result);
    return this.result ? this.result.codeResult : '';
  }
  ngOnChanges(change) {}
}
