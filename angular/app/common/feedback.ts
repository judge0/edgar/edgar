export class Feedback {
  public clientTs: any;
  constructor(
    public comment: string,
    public rating: number,
    public maxRating: number,
    public exerciseId?: number,
    public exerciseQuestionOrdinal?: number,
    public tutorialId?: number,
    public tutorialStepId?: number,
    public timestamp?: Date
  ) {
    this.clientTs = new Date();
  }
}
