import { Component, Injectable, Input, OnInit, OnChanges } from 'angular2/core';
import { FeedbackService } from './feedback.service';
import { Feedback } from './feedback';

@Component({
  selector: 'feedback',
  template: `<div id="feedback">
    <div class="btn-group-vertical buttons" [class.margins]="_showBox">
      <button
        (click)="(!_showResult && toggleFeedbackBox()) || (_showResult && newFeedback())"
        class="btn btn-sm btn-primary feedback-button"
        type="button"
      >
        Feedback
      </button>
      <button
        *ngIf="_showBox && _previousFeedbacks.length > 0"
        (click)="toggleHistory()"
        class="btn btn-sm history-button"
      >
        History
      </button>
    </div>
    <div *ngIf="_showHistory" class="history">
      <dl>
        <ng-container *ngFor="#fb of _previousFeedbacks, #i = index">
          <dt [innerHTML]="fb.timestamp"></dt>
          <dd *ngIf="fb.comment">{{ fb.comment }}</dd>
          <dd *ngIf="fb.rating">{{ fb.rating }}/{{ fb.maxRating }}</dd>
        </ng-container>
      </dl>
    </div>
    <div class="feedback-box" *ngIf="_showBox">
      <h6>{{ getFeedbackTitle() }}</h6>
      <small *ngIf="_tsSend">submitted {{ _tsSent }}</small>
      <div *ngIf="!hideComment">
        <label for="feedbackEditor" class="comment-label">Comment</label>
        <textarea
          id="feedbackEditor"
          class="form-control"
          rows="5"
          [disabled]="_sending"
          [(ngModel)]="_comment"
        ></textarea>
        <label>Rating</label>
      </div>
      <div class="rating">
        <span *ngIf="!hideRating">
          <span *ngFor="#n of getRatingClasses(), #i = index">
            <input
              type="radio"
              (click)="setRating(i + 1)"
              [value]="i + 1"
              [checked]="i + 1 === _rating"
              id="feedback-rating-{{ i + 1 }}"
              name="rating"
            />
            <label attr.for="feedback-rating-{{ i + 1 }}">{{ i + 1 }}</label>
          </span>
        </span>
        <button
          (click)="submit()"
          type="button"
          class="btn btn-primary submit-button"
          [innerHTML]="getSubmitLabel()"
          [disabled]="_sending || !canSend()"
        ></button>
      </div>
    </div>
    <div *ngIf="_showResult" class="feedback-result">
      <h6 *ngIf="_resultSuccess" class="text-success">Feedback successfully sent</h6>
      <h6 *ngIf="!_resultSuccess" class="text-danger">Failed to send feedback</h6>
      <p>
        Click on the feedback button for a new feedback or
        <a (click)="close()">here to close</a>.
      </p>
    </div>
  </div>`,
  styles: [
    `
      #feedback {
        position: fixed;
        bottom: 0;
        right: 0;
        background: white;
        margin: 0;
        padding: 0;
        border: 1px solid;
        z-index: 100;
      }
      #feedback .buttons {
        margin: 0;
        max-width: 100px;
        float: left;
      }
      #feedback .buttons.margins {
        margin: 0 10px 10px 0;
      }
      #feedback .feedback-button,
      #feedback .history-button {
        border-radius: 0;
      }
      #feedback h6 {
        word-wrap: none;
      }

      #feedback .history {
        padding: 0 15px;
        border-bottom: 1px solid black;
      }
      #feedback .history > div {
        border-top: 1px dotted black;
        word-wrap: break-word;
      }

      #feedback .history {
        max-height: 400px;
        overflow-y: scroll;
      }

      #feedback .feedback-box,
      #feedback .feedback-result {
        padding: 10px;
      }
      #feedback .comment-label,
      #feedback .submit-button {
        float: right;
      }

      #feedback .rating input {
        display: none;
      }
      #feedback .rating label {
        border-radius: 2px;
        padding: 5px 10px;
        display: inline-block;
        background-color: #9de5fb;
        cursor: pointer;
      }
      #feedback .rating input:checked ~ label {
        background-color: #ffca08;
      }
      #feedback .rating input:hover ~ label {
        background-color: #ffca08;
      }

      #feedback .feedback-result h6 {
        margin-left: 100px;
      }
    `,
  ],
  providers: [FeedbackService],
})
@Injectable()
export class FeedbackComponent implements OnInit, OnChanges {
  @Input() title: string;
  @Input() exerciseId: number;
  @Input() exerciseQuestionOrdinal: number;
  @Input() tutorialId: number;
  @Input() tutorialStepId: number;
  @Input() hideRating: boolean;
  @Input() hideComment: boolean;
  @Input() ratingRequired: boolean;
  @Input() commentRequired: boolean;
  @Input() maxRatingScore: number;
  @Input() open: boolean;

  _previousFeedbacks: Feedback[] = [];
  _showHistory: boolean = false;
  _showBox: boolean = false;
  _showResult: boolean = false;
  _resultSuccess: boolean;
  _comment: string;
  _rating: number;
  _ratingClasses: number = 5;
  _sending: boolean = false;

  constructor(private _feedbackService: FeedbackService) {}

  setRating(r: number) {
    this._rating = r;
  }

  toggleFeedbackBox() {
    this._showBox = !this._showBox;

    if (this._showHistory) {
      this._showHistory = false;
    }
  }

  submit() {
    const feedback = new Feedback(
      this._comment,
      this._rating,
      this._rating ? this._ratingClasses : null,
      this.exerciseId,
      this.exerciseQuestionOrdinal,
      this.tutorialId,
      this.tutorialStepId
    );
    this._sending = true;
    this._feedbackService.send(feedback).subscribe((res) => {
      const data = res.json();
      this._resultSuccess = data.success;
      this._showBox = false;
      this._showResult = true;
      this._sending = false;
      feedback.timestamp = new Date();
      this._previousFeedbacks.unshift(feedback);
    });
  }

  canSend() {
    return (
      (!this.ratingRequired || (this.ratingRequired && this._rating)) &&
      ((this.commentRequired && this._comment) || !this.commentRequired)
    );
  }

  reset() {
    this._comment = '';
    this._rating = null;
  }

  newFeedback() {
    this.reset();
    this._showResult = false;
    this._showBox = true;
  }

  toggleHistory() {
    this._showHistory = !this._showHistory;
  }

  close() {
    this.reset();
    this._showResult = false;
    this._showBox = false;
    this._showHistory = false;
  }

  getSubmitLabel() {
    return this._sending ? '<i class="fa fa-2 fa-spinner fa-spin"></i>' : 'Submit';
  }

  getRatingClasses() {
    return new Array(this._ratingClasses);
  }

  getFeedbackTitle() {
    return this.title ? this.title : undefined;
  }

  ngOnInit() {
    this._ratingClasses = this.maxRatingScore || 5;
    this._showBox = this.open || false;
  }

  ngOnChanges() {
    this._ratingClasses = this.maxRatingScore || 5;

    if (this.open !== undefined) {
      this._showBox = this.open;
    }

    this._feedbackService
      .get(
        new Feedback(
          '',
          0,
          0,
          this.exerciseId,
          this.exerciseQuestionOrdinal,
          this.tutorialId,
          this.tutorialStepId
        )
      )
      .subscribe((fbs) => {
        this._previousFeedbacks = fbs;
      });
  }
}
