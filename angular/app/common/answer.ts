export class Answer {
  multichoice: number[] = [];
  codeAnswer: CodeAnswer = new CodeAnswer();
  text: string = '';
}

export class CodeAnswer {
  code: string = undefined;
  languageId: number = undefined;
}
