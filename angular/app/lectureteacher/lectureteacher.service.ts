/// <reference path="../../../node_modules/@types/socket.io-client/index.d.ts"/>

import { Injectable } from 'angular2/core';
import { Http, Headers } from 'angular2/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';
import { LogService, LogEvent } from '../common/log.service';
import * as io from 'socket.io-client';

@Injectable()
export class LectureTeacherService {
  private _questions: Question[];
  private _test: Test;
  private socket;

  constructor(private _http: Http, private _logService: LogService) {
    this._questions = [];
  }

  getQuestion(num: number) {
    let rv: Question;
    if (this._questions[num]) {
      return Observable.of(this._questions[num]);
    } else {
      this._logService.log(new LogEvent('Get question', 'num:' + num));
      return this._http.get(`/lecture/teacher/getquestion/${num}`).map((res) => {
        let q = res.json();
        this._questions[num] = new Question(
          q.ordinal.toString(),
          q.ordinal,
          q.content,
          q.answers,
          q.multiCorrect,
          q.type,
          q.correctAnswers
        );
        this._logService.log(new LogEvent('Received question', 'num:' + q.ordinal));
        return this._questions[num];
      });
    }
  }

  getTest() {
    if (this._test) {
      return Observable.of(this._test);
    } else {
      this._logService.log(new LogEvent('Get test', '-'));
      return this._http.get(`/lecture/teacher/gettest`).map((res) => {
        let t = res.json();
        if (t.success === false) {
          this._logService.log(new LogEvent('Received NULL test', JSON.stringify(t)));
          return null;
        } else {
          this._test = new Test(t.noOfQuestions, t.title, t.password, t.roomName);
          this._logService.log(new LogEvent('Received test', JSON.stringify(this._test)));
          return this._test;
        }
      });
    }
  }

  getResults(roomName) {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this._http
      .post('/lecture/getresults', JSON.stringify({ room: roomName }), {
        headers: headers,
      })
      .map((res) => {
        return res.json();
      });
  }

  getStudentAnswers(qordinal, noOfAnswers) {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this._http
      .post(
        '/lecture/getstudanswers',
        JSON.stringify({
          qordinal: qordinal,
          room: this._test.roomName,
          noOfAns: noOfAnswers,
        }),
        { headers: headers }
      )
      .map((res) => {
        return res.json();
      });
  }

  connect() {
    this.socket = io.connect(window.location.host, {
      path: '/wss/socket.io',
      query: 'room=' + this._test.roomName,
    });
    this.socket.emit('subscribe-to-user-count');
  }

  userJoined() {
    let observable = new Observable((observer) => {
      this.socket.on('user-joined', (user) => {
        observer.next(user);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  startStudent(ordinal) {
    this.socket.emit('start-student', ordinal);
  }

  stopQuestion() {
    this.socket.emit('stop-question');
  }

  endTest() {
    // this.socket.emit('stop-question');
    //
    // // todo
    //
    // this.socket.emit('end-test');

    this.socket.emit('end-test');
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this._http
      .post('/lecture/endtest', JSON.stringify({ room: this._test.roomName }), {
        headers: headers,
      })
      .map((res) => {
        return res.json();
      });
  }

  getFreeTextAnswer() {
    let observable = new Observable((observer) => {
      this.socket.on('free-text-answer', (answer) => {
        observer.next(answer);
      });

      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  getAllAnswers() {
    let observable = new Observable((observer) => {
      this.socket.on('all-answers', (answers) => {
        observer.next(answers);
      });

      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  getScore() {
    let observable = new Observable((observer) => {
      this.socket.on('score', (score) => {
        observer.next(score);
      });

      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }
}

export class Test {
  constructor(
    public noOfQuestions: number,
    public title: string,
    public password: string,
    public roomName: string
  ) {}
}

export class Question {
  constructor(
    public caption: string,
    public ordinal: number,
    public content,
    public answers,
    public multiCorrect,
    public type: string,
    public correctAnswers: number[]
  ) {}

  static getAnswerLabel(answers: any): string {
    if (answers) {
      if (answers instanceof Array && answers.length) {
        return answers
          .map(function (num) {
            return String.fromCharCode(97 - 1 + parseInt('' + num));
          })
          .join();
      }
    }
    return '-';
  }
}
