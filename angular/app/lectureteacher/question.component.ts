import { Component, Input } from 'angular2/core';
import { Question } from './lectureteacher.service';

@Component({
  selector: 'question-cont',
  template: `<div class="aii-q-content" [innerHTML]="currQuestion && currQuestion.content"></div>
    <ng-container *ngIf="currQuestion">
      <div *ngIf="currQuestion.type != 'Free text'">
        <div class="aii-q-answer" *ngFor="#a of currQuestion.answers">
          <div *ngIf="!showCorrect" class="aii-answer-badge badge badge-secondary">
            {{ getLabel([a.ordinal]) }}
          </div>
          <div
            *ngIf="showCorrect"
            class="aii-answer-badge badge badge-secondary"
            [class.badge-success]="isCorrect(a)"
          >
            {{ getLabel([a.ordinal]) }}
          </div>
          <div style="margin-left: 50px; min-width: 100px; text-align: left;">
            <div style="display:inline-block; margin-right: 15px;" [innerHTML]="a.aHtml"></div>
            <div
              *ngIf="showCorrect"
              style="display:inline-block; margin-left: 40px; margin-right: 5px"
            >
              <i class="fa fa-user" aria-hidden="true"></i>
            </div>
            <span *ngIf="showCorrect">{{ getNoOfAns([a.ordinal]) }}</span>
          </div>
           
        </div>
      </div>

      <div *ngIf="currQuestion.type == 'Free text' && showCorrect">
        <br />
        <h1>Answers:</h1>
        <br />
        <div style="text-align: left; font-size: 25px;">
          <div *ngFor="#a of freeTextAnswers" class="col-10 offset-1" style="margin-bottom: 10px;">
            <div style="display:inline-block; margin-right: 5px;">
              <i class="fa fa-user" aria-hidden="true"></i>
            </div>
            <span>{{ a }}</span>
          </div>
        </div>
        <!--<div *ngFor="#a of freeTextAnswers">-->
        <!--<h2>{{a.answer}}</h2>-->
        <!--</div>-->
      </div>
    </ng-container>`,
  styles: [
    `
      .aii-q-content {
        /*min-height: 100px;*/
        font-size: 34px !important;
        margin-bottom: 10px;
      }
      .aii-q-answer {
        /*min-height: 50px;*/
        font-size: 30px;
      }
      .aii-answer-badge {
        width: 35px;
        float: left;
        /*font-size: 1.5rem;*/
      }
    `,
  ],
  providers: [],
})
export class QuestionComponent {
  @Input() currQuestion: Question;
  @Input() showCorrect: boolean;
  @Input() studAnswers;
  @Input() freeTextAnswers;

  getLabel(ordinal): string {
    return Question.getAnswerLabel(ordinal);
  }

  isCorrect(answer) {
    return this.currQuestion.correctAnswers.indexOf(answer.ordinal) >= 0;
  }

  getNoOfAns(ordinal) {
    return this.studAnswers[ordinal - 1];
  }
}
