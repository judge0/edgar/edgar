import {
  Component,
  Input,
  OnInit,
  EventEmitter,
  Output,
  OnChanges,
  ChangeDetectionStrategy,
} from 'angular2/core';
// import {Router, RouteParams, ROUTER_DIRECTIVES} from 'angular2/router'; // CanDeactivate
import { Question } from './revtest.service';
//import {CourseService} from './course.service';
//import {AutoGrowDirective} from './auto-grow-directive';
// [routerLink]="['Question', { qno: q.ordinal }]"
// <a > {{q.caption}}&nbsp;&nbsp; </a>

@Component({
  selector: 'q-nav',
  template: `
    <ng-container *ngFor="#a of answersOutcome; #i = index">
      &nbsp;<span class="aii-qn-tag" [class.selected]="isSelected(i)" (click)="onSelect(i)">
        <span class="badge">{{ i + 1 }}</span>
        <i *ngIf="a === 'correct'" class="far fa-smile text-green" aria-hidden="true"></i>
        <i *ngIf="a === 'incorrect'" class="far fa-frown text-red" aria-hidden="true"></i>
        <i *ngIf="a === 'unanswered'" class="far fa-meh text-gray" aria-hidden="true"></i>
        <i *ngIf="a === 'partial'" class="fas fa-flushed text-orange" aria-hidden="true"></i>
         
      </span>
    </ng-container>
  `,
  styles: [
    `
      .aii-qn-tag {
        color: #36363c;
        cursor: pointer;
        font-size: 1.5rem;
        // margin-right: 3px;
        // padding: 3px;
        border-radius: 0.25rem;
        border: 1px aliceblue solid;
        white-space: nowrap;
      }
      .selected {
        border: black solid 1px;
        background-color: aliceblue;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  // ,    providers: [ROUTER_PROVIDERS]
  //, directives: [ROUTER_DIRECTIVES]
})
export class QNavComponent implements OnInit, OnChanges {
  // routerCanDeactivate(n, p)
  // public questions: Question[];
  private _idSelected: number;
  @Input() answersOutcome: string[];
  @Output() questionSelected = new EventEmitter();
  constructor() {
    // private _router: Router // routeParams: RouteParams
    this._idSelected = 0;
    // this.questions = [];
    // this._selectedId = +routeParams.get('id');
  }
  getLabel(a: any): string {
    return Question.getAnswerLabel(a);
  }

  isSelected(i: number) {
    // console.log('q-nav isSelected?', i, this._idSelected, i === this._idSelected, i == this._idSelected);
    return i === this._idSelected;
  }

  onSelect(i: number) {
    this._idSelected = i;
    this.questionSelected.emit({ ordinal: i + 1 });
  }
  ngOnInit() {
    // for (let i = 0; i < this.currAnswers.length; ++i) {
    //     this.questions.push(new Question((i+1) + '', i+1));
    // }
  }
  ngOnChanges() {
    // console.log('************* q-nav on changes', this.currAnswers);
    // for (var a in this.currAnswers) {
    //     console.log('-->  a', a, this.currAnswers.length, Question.getAnswerLabel(a));
    // }
    // this.questions = [];
    // for (let i = 0; i < this.currAnswers.length; ++i) {
    //     this.questions.push(new Question((i + 1) + '', i + 1, this.currAnswers[i]));
    // }
  }
  // routerCanDeactivate(n, p) {
  //     console.log('routerCanDeactivate(n, p) = ', n, p);
  // }
}
