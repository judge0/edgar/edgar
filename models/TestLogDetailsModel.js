var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var winston = require('winston');
var testLogDetailsSchema = new Schema({
  id_test_instance: { type: Number },
  ordinal: { type: Number },
  code: { type: String },
  result: Schema.Types.Mixed,
});

testLogDetailsSchema.statics.getLoggedResults = function (id_test_instance) {
  return new Promise((resolve, reject) => {
    this.model('TestLogDetails').find({ id_test_instance: id_test_instance }, function (err, rv) {
      if (err) {
        reject(err);
      } else {
        if (rv) {
          // flatten:
          resolve(
            rv.map((x) => {
              return {
                ...x.result,
                code: x.code,
                ordinal: x.ordinal,
              };
            })
          );
        } else {
          resolve([]);
        }
      }
    });
  });
};

testLogDetailsSchema.statics.logResult = function (id_test_instance, ordinal, code, entry) {
  return new Promise((resolve, reject) => {
    entry.mongoTs = new Date();

    let logEntry = new TestLogDetailsModel({
      id_test_instance: id_test_instance,
      ordinal,
      code,
      result: entry,
    });

    logEntry.save(function (err) {
      if (err) {
        winston.error('Error on saving TestLogDetails ' + err);
        reject(err);
      } else {
        winston.debug('TestLogDetails saved.');
        resolve();
      }
    });
  });
};

const TestLogDetailsModel = mongoose.model('TestLogDetails', testLogDetailsSchema);
testLogDetailsSchema.index({
  id_test_instance: 1,
});

module.exports = TestLogDetailsModel;
