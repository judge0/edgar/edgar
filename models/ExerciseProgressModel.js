'use strict';
const mongoose = require('mongoose');
const winston = require('winston');
const Schema = mongoose.Schema;

const exerciseProgressSchema = new Schema(
  {
    exerciseId: Schema.Types.Number,
    studentId: Schema.Types.Number,
    startedTs: {
      type: Schema.Types.Date,
      default: Date.now,
    },
    finishedTs: {
      type: Schema.Types.Date,
    },
    latestQuestionOrdinal: {
      type: Schema.Types.Number,
      default: 1,
    },
    events: [Schema.Types.Mixed],
    currentAnswers: [Schema.Types.Mixed],
    currentDifficultyLevelId: Schema.Types.Number,
  },
  {
    _id: false,
  }
);

exerciseProgressSchema.statics.getLog = function (exerciseId, studentId) {
  return new Promise((resolve, reject) => {
    const model = this.model('exerciseProgress');
    model.findOne(
      {
        exerciseId: exerciseId,
        studentId: studentId,
      },
      (err, item) => {
        if (err) {
          winston.error(`Error on getting exercise progress log: ` + err);
          reject(err);
        } else {
          winston.debug(`Getting exercise progress log.`);
          resolve(item);
        }
      }
    );
  });
};

exerciseProgressSchema.statics.getStatus = function (exerciseId, studentId) {
  return new Promise((resolve, reject) => {
    const model = this.model('exerciseProgress');
    model.findOne(
      {
        exerciseId: exerciseId,
        studentId: studentId,
      },
      (err, item) => {
        if (err) {
          winston.error(`Error on getting exercise progress status: ` + err);
          reject(err);
        } else {
          winston.debug(`Getting exercise progress status`);
          if (item) {
            resolve({
              currentAnswers: item.currentAnswers,
              currentDifficultyLevelId: item.currentDifficultyLevelId,
              latestQuestionOrdinal: item.latestQuestionOrdinal,
              startedTs: item.startedTs,
            });
          } else {
            resolve();
          }
        }
      }
    );
  });
};

exerciseProgressSchema.statics.getExerciseAnswers = function (exerciseId, studentId) {
  const model = this.model('exerciseProgress');
  return new Promise((resolve, reject) => {
    const query = {
      exercise: exerciseId,
      studentId: studentId,
    };
    model.findOne(query, function (err, rv) {
      if (err) {
        winston.error('getExerciseAnswers: error ' + err);
        reject(err);
      } else {
        resolve(rv);
      }
    });
  });
};

exerciseProgressSchema.statics.updateExerciseAnswers = function (exerciseId, studentId, answers) {
  const model = this.model('exerciseProgress');
  return new Promise((resolve, reject) => {
    const query = {
      exerciseId: exerciseId,
      studentId: studentId,
    };
    const update = {
      currentAnswers: answers,
    };
    const options = {
      upsert: true,
    };
    model.findOneAndUpdate(query, update, options, function (err, rv) {
      if (err) {
        winston.error('updateExerciseAnswers: error ' + err);
        reject(err);
      } else {
        resolve(rv.currentAnswers);
      }
    });
  });
};

exerciseProgressSchema.statics.updateLatestQuestionOrdinal = function (
  exerciseId,
  studentId,
  newOrdinal
) {
  const model = this.model('exerciseProgress');
  return new Promise((resolve, reject) => {
    const query = {
        exerciseId: exerciseId,
        studentId: studentId,
      },
      update = {
        latestQuestionOrdinal: newOrdinal,
        $push: {
          events: {
            eventName: 'Question progress',
            eventData: `From ${newOrdinal - 1} to ${newOrdinal}`,
            mongoTs: new Date(),
          },
        },
      };
    model.findOneAndUpdate(query, update, function (err, rv) {
      if (err) {
        winston.error('updateLatestQuestionOrdinal: error ' + err);
        reject(err);
      } else {
        resolve(rv);
      }
    });
  });
};

exerciseProgressSchema.statics.updateDifficultyLevel = function (
  exerciseId,
  studentId,
  difficultyLevelId
) {
  const model = this.model('exerciseProgress');
  return new Promise((resolve, reject) => {
    const query = {
        exerciseId: exerciseId,
        studentId: studentId,
      },
      update = {
        currentDifficultyLevelId: difficultyLevelId,
        $push: {
          events: {
            eventName: 'Difficulty level change',
            eventData: `To difficulty level id ${difficultyLevelId}`,
            mongoTs: new Date(),
          },
        },
      };
    model.findOneAndUpdate(query, update, function (err, rv) {
      if (err) {
        winston.error('updateLatestQuestionOrdinal: error ' + err);
        reject(err);
      } else {
        resolve(rv);
      }
    });
  });
};

exerciseProgressSchema.statics.markAsFinished = function (exerciseId, studentId, finishedTs) {
  const model = this.model('exerciseProgress');
  return new Promise((resolve, reject) => {
    const query = {
        exercise: exerciseId,
        studentId: studentId,
      },
      update = {
        latestQuestionOrdinal: 1,
        finishedTs: finishedTs,
      };
    model.findOneAndUpdate(query, update, function (err, rv) {
      if (err) {
        winston.error('markAsFinished: error ' + err);
        reject(err);
      } else {
        resolve(rv);
      }
    });
  });
};

exerciseProgressSchema.statics.logEvent = function (exerciseId, studentId, event) {
  return new Promise((resolve, reject) => {
    event.mongoTs = new Date();
    const query = {
        exerciseId: exerciseId,
        studentId: studentId,
      },
      update = {
        $push: {
          events: event,
        },
      },
      options = {
        upsert: true,
        setDefaultsOnInsert: true,
      };
    this.model('exerciseProgress').findOneAndUpdate(query, update, options, function (err, item) {
      if (err) {
        winston.error('Error on saving exercise log event ' + err);
        reject(err);
      } else {
        winston.debug('Exercise log event saved.');
        resolve(item);
      }
    });
  });
};

module.exports = mongoose.model('exerciseProgress', exerciseProgressSchema);
