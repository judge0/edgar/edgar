var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OnlineUsersSchema = new Schema(
  {
    _id: String, // username
    imgSrc: String,
    rolename: String,
    username: String,
    fullname: String,
    currCourse: String,
    currAcademicYear: String,
    lastRoute: String,
    ts_modified: {
      type: Date,
      default: Date.now,
      expires: 60 * 60,
    },
  },
  { _id: false }
);

OnlineUsersSchema.statics.getOnlineUsers = function () {
  return new Promise((resolve, reject) => {
    this.model('OnlineUsers')
      .find({})
      .sort([['ts_modified', -1]])
      .exec(function (err, users) {
        if (err) {
          reject(err);
        } else {
          resolve(users);
        }
      });
  });
};

module.exports = mongoose.model('OnlineUsers', OnlineUsersSchema);
// db.OnlineUsers.createIndex( { "ts_modified": 1 }, { expireAfterSeconds: 60*60 } )  created via mongoose expires attr (above)
