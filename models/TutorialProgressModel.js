'use strict';
const mongoose = require('mongoose');
const winston = require('winston');
const Schema = mongoose.Schema;

const tutorialProgressSchema = new Schema({
  _id: Schema.Types.Number,
  courseId: Schema.Types.Number,
  studentId: Schema.Types.Number,
  tutorialId: Schema.Types.Number,
  startedTs: {
    type: Schema.Types.Date,
    default: Date.now,
  },
  finishedTs: {
    type: Schema.Types.Date,
  },
  latestStepOrdinal: {
    type: Schema.Types.Number,
    default: 1,
  },
  events: [Schema.Types.Mixed],
  currentAnswers: [Schema.Types.Mixed],
});

tutorialProgressSchema.statics.getTutorialProgress = function (tutorialId, courseId, studentId) {
  return new Promise((resolve, reject) => {
    const model = this.model('tutorialProgress');
    model.findOne(
      {
        tutorialId: tutorialId,
        courseId: courseId,
        studentId: studentId,
      },
      (err, item) => {
        if (err) {
          winston.error(`Error on getting tutorial progress log: ` + err);
          reject(err);
        } else {
          winston.debug(`Got tutorial progress`);
          resolve(item);
        }
      }
    );
  });
};

// tutorialProgressSchema.statics.getStatus = function (courseId, studentId, tutorialId) {
//     return new Promise((resolve, reject) => {
//         const model = this.model('tutorialProgress');
//         model.findOne({
//             courseId: courseId,
//             studentId: studentId,
//             tutorialId: tutorialId
//         }, (err, item) => {
//             if (err) {
//                 winston.error(`Error on getting tutorial progress status: ` + err);
//                 reject(err);
//             } else {
//                 winston.debug(`Getting tutorial progress status.`);
//                 if (item) {
//                     resolve(item);
//                 } else {
//                     resolve();
//                 }
//             }
//         });
//     });
// };
// tutorialProgressSchema.statics.getTutorialAnswers = function (courseId, studentId, tutorialId) {
//     const model = this.model('tutorialProgress');
//     return new Promise((resolve, reject) => {
//         const query = {
//             courseId: courseId,
//             studentId: studentId,
//             tutorialId: tutorialId
//         };
//         model.findOne(query, function (err, rv) {
//             if (err) {
//                 winston.error('getTutorialAnswers: error ' + err);
//                 reject(err);
//             } else {
//                 resolve(rv);
//             }
//         });
//     });
// };
tutorialProgressSchema.statics.updateLatestStepOrdinal = function (
  courseId,
  studentId,
  tutorialId,
  newOrdinal
) {
  return new Promise((resolve, reject) => {
    const model = this.model('tutorialProgress');
    model.findOneAndUpdate(
      {
        courseId: courseId,
        studentId: studentId,
        tutorialId: tutorialId,
      },
      {
        latestStepOrdinal: newOrdinal,
      },
      {
        upsert: true,
      },
      function (err, item) {
        if (err) reject(err);
        else resolve();
      }
    );
  });
};

tutorialProgressSchema.statics.updateTutorialAnswers = function (
  courseId,
  studentId,
  tutorialId,
  answers
) {
  const model = this.model('tutorialProgress');
  return new Promise((resolve, reject) => {
    const query = {
      courseId: courseId,
      studentId: studentId,
      tutorialId: tutorialId,
    };
    const update = {
      currentAnswers: answers,
      $push: {
        events: {
          mongoTs: new Date(),
          eventName: 'Update tutorial answers',
          eventData: JSON.stringify(answers),
        },
      },
    };
    const options = {
      upsert: true,
    };
    model.findOneAndUpdate(query, update, options, function (err, rv) {
      if (err) {
        winston.error('updateTutorialAnswers: error ' + err);
        reject(err);
      } else {
        resolve(rv.currentAnswers);
      }
    });
  });
};

tutorialProgressSchema.statics.updateLatestStepOrdinal = function (
  courseId,
  studentId,
  tutorialId,
  newOrdinal
) {
  const model = this.model('tutorialProgress');
  return new Promise((resolve, reject) => {
    const query = {
        courseId: courseId,
        studentId: studentId,
        tutorialId: tutorialId,
      },
      update = {
        latestStepOrdinal: newOrdinal,
        $push: {
          events: {
            mongoTs: new Date(),
            eventName: 'Step progress',
            eventData: `From step ${newOrdinal - 1} to step ${newOrdinal}`,
          },
        },
      };
    model.findOneAndUpdate(query, update, function (err, rv) {
      if (err) {
        winston.error('updateLatestStepOrdinal: error ' + err);
        reject(err);
      } else {
        resolve(rv);
      }
    });
  });
};

tutorialProgressSchema.statics.markAsFinished = function (
  courseId,
  studentId,
  tutorialId,
  finishedTs
) {
  const model = this.model('tutorialProgress');
  return new Promise((resolve, reject) => {
    const query = {
        courseId: courseId,
        studentId: studentId,
        tutorialId: tutorialId,
      },
      update = {
        finishedTs: finishedTs,
      };
    model.findOneAndUpdate(query, update, function (err, rv) {
      if (err) {
        winston.error('markAsFinished: error ' + err);
        reject(err);
      } else {
        resolve(rv);
      }
    });
  });
};

tutorialProgressSchema.statics.clear = function (tutorialId) {
  const model = this.model('tutorialProgress');
  return new Promise((resolve, reject) => {
    const query = {
      tutorialId: tutorialId,
    };
    model.deleteMany(query, function (err, rv) {
      if (err) {
        winston.error('deleteTutorialProgress: error ' + err);
        reject(err);
      } else {
        resolve(rv);
      }
    });
  });
};

tutorialProgressSchema.statics.logEvent = function (courseId, studentId, tutorialId, event) {
  return new Promise((resolve, reject) => {
    event.mongoTs = new Date();
    const query = {
        courseId: courseId,
        studentId: studentId,
        tutorialId: tutorialId,
      },
      update = {
        $push: {
          events: event,
        },
      },
      options = {
        upsert: true,
        setDefaultsOnInsert: true,
      };
    this.model('tutorialProgress').findOneAndUpdate(query, update, options, function (err, item) {
      if (err) {
        winston.error('Error on saving tutorial log event ' + err);
        reject(err);
      } else {
        winston.debug('Tutorial log event saved for ' + JSON.stringify(query));
        resolve(item);
      }
    });
  });
};

module.exports = mongoose.model('tutorialProgress', tutorialProgressSchema);
