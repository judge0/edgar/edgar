var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var winston = require('winston');
var codeRunLogSchema = new Schema({
  _id: { type: String }, // id_test+id_question,
  id_test: Number,
  id_question: Number,
  ts_modified: Date,
  code_runs: [Schema.Types.Mixed],
});

getCodeRunId = (id_test, id_question) => `${id_test}+${id_question}`;

codeRunLogSchema.statics.getCodeRuns = function (id_test, id_question) {
  return new Promise((resolve, reject) => {
    this.model('CodeRunLog').findById(getCodeRunId(id_test, id_question), function (err, rv) {
      if (err) {
        reject(err);
      } else {
        if (rv && rv.code_runs) {
          resolve(rv.code_runs);
        } else {
          resolve([]);
        }
      }
    });
  });
};

codeRunLogSchema.statics.getCodeRunsTail = function (id_test, id_question, length, seconds) {
  return new Promise((resolve, reject) => {
    this.model('CodeRunLog').find(
      { _id: getCodeRunId(id_test, id_question) },
      length ? { code_runs: { $slice: -1 * length } } : {},
      function (err, rv) {
        if (err) {
          reject(err);
        } else {
          if (rv[0]) rv = rv[0];
          if (rv && rv.code_runs) {
            // TODO: move the filtering to the mongo, can mongoose do that? could not find $filter operator
            if (seconds) {
              let now = new Date().getTime();
              let mseconds = 1000 * seconds;
              rv.code_runs = rv.code_runs.filter((x) => now - x.ts.getTime() < mseconds);
            }
            resolve(rv.code_runs);
          } else {
            resolve([]);
          }
        }
      }
    );
  });
};

codeRunLogSchema.statics.logCodeRun = function (
  session,
  code,
  id_question,
  ordinal,
  score,
  is_correct
) {
  return new Promise((resolve) => {
    var username = session.anon_alt_id || (session.passport && session.passport.user);
    if (!username || !session.id_test_instance) {
      winston.error(
        'Warning: logCodeRun: unknown session user or id_test_instance for log entry:' +
          JSON.stringify({
            code,
            id_question,
            ordinal,
            score,
            is_correct,
          })
      );
      resolve(); // don't want logging to crash anything
    } else {
      var query = {
        _id: getCodeRunId(session.id_test, id_question),
      };
      var update = {
        id_test: session.id_test,
        id_question: id_question,
        ts_modified: new Date(),
        $push: {
          code_runs: {
            ts: new Date(),
            username: username,
            id_student: session.studentId,
            id_question,
            ordinal,
            code,
            score,
            is_correct,
          },
        },
      };
      var options = {
        upsert: true,
      };
      this.model('CodeRunLog').findOneAndUpdate(query, update, options, function (err, item) {
        if (err) {
          winston.error('Error on saving CodeRunLog ', err);
          winston.error('Attempted test query: ' + JSON.stringify(query));
          winston.error('Attempted test update: ' + JSON.stringify(update));
          resolve(); // don't want logging to crash anything
        } else {
          winston.debug('CodeRunLog saved.');
          resolve(item);
        }
      });
    }
  });
};

module.exports = mongoose.model('CodeRunLog', codeRunLogSchema);
