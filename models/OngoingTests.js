'use strict';
var mongoose = require('mongoose');
var winston = require('winston');
var Schema = mongoose.Schema;

var ongoingTestsSchema = new Schema({
  _id: String, // username is id
  currAnswers: Schema.Types.Mixed,
  currTestProperties: Schema.Types.Mixed,
});

ongoingTestsSchema.statics.setCurrTestProperties = async function (session, alltp) {
  let username = session.anon_alt_id || session.passport.user;
  try {
    var query = {
      _id: username,
    };
    var update = {
      currTestProperties: alltp,
    };
    var options = {
      upsert: true,
    };
    await this.model('OngoingTests').findOneAndUpdate(query, update, options);
    winston.debug('CurrTestProperties saved.');
  } catch (error) {
    winston.error(error);
  }
};

ongoingTestsSchema.statics.updateCurrTestProperties = async function (
  session,
  tp,
  id_test_instance
) {
  var model = this.model('OngoingTests');
  var username = session.anon_alt_id || session.passport.user;
  try {
    let rv = (await model.findById(username)) || {};
    let alltp = rv.currTestProperties || {};
    alltp[id_test_instance || session.id_test_instance] = tp;
    await model.setCurrTestProperties(session, alltp);
    return tp;
  } catch (err) {
    winston.error(err);
  }
};

ongoingTestsSchema.statics.setCurrAnswers = function (session, ca, usernameToUpdate) {
  //var model = this.model('OngoingTests');

  // for lecture quiz, check if it's an anonymous user
  var username = session.anon_alt_id || usernameToUpdate || session.passport.user;
  winston.debug('Updating CA for ' + username);
  return new Promise((resolve, reject) => {
    var query = {
      _id: username,
    };
    var update = {
      currAnswers: ca,
    };
    var options = {
      upsert: true,
    };
    this.model('OngoingTests').findOneAndUpdate(query, update, options, function (err, item) {
      if (err) {
        winston.error('Error on saving OngoingTests ' + err);
        reject(err);
      } else {
        winston.debug('Current answers saved.');
        resolve(item);
      }
    });
  });
};

ongoingTestsSchema.statics.updateCurrAnswers = function (session, usernameToUpdate) {
  var model = this.model('OngoingTests');

  // for lecture quiz, check if it's an anonymous user
  var username = session.anon_alt_id || usernameToUpdate;

  return new Promise((resolve, reject) => {
    model.findById(username, function (err, rv) {
      if (err) {
        winston.error(err);
        reject(err);
      } else {
        let ca = (rv && rv.currAnswers) || {};
        ca[session.id_test_instance] = session.currAnswers[session.id_test_instance];
        // console.log('new ca:', ca, session.passport.user, typeof session.passport.user);

        return model.setCurrAnswers(session, ca, username);
      }
    });
  });
};

ongoingTestsSchema.statics.clearTestData2 = function (session, currTI) {
  var model = this.model('OngoingTests');
  var username = session.anon_alt_id || session.passport.user;
  return new Promise((resolve, reject) => {
    model.findById(username, function (err, rv) {
      if (err) {
        reject(err);
      } else {
        // Deleteing just my 'part', ie id_test_instance
        let ca = (rv && rv.currAnswers) || {};
        delete ca[currTI];

        let tp = (rv && rv.currTestProperties) || {};
        delete tp[currTI];

        if (Object.keys(ca).length === 0) {
          resolve(model.clearAllTestsData(session));
        } else {
          resolve(
            Promise.all([
              model.setCurrAnswers(session, ca, username),
              model.setCurrTestProperties(session, tp),
            ])
          );
        }
      }
    });
  });
};

ongoingTestsSchema.statics.clearTestData = function (session, usernameToUpdate) {
  var model = this.model('OngoingTests');
  var username = session.anon_alt_id || usernameToUpdate || session.passport.user;
  return new Promise((resolve, reject) => {
    model.findById(username, function (err, rv) {
      if (err) {
        reject(err);
      } else {
        // Deleteing just my 'part', ie id_test_instance
        let ca = (rv && rv.currAnswers) || {};
        delete ca[session.id_test_instance];

        let tp = (rv && rv.currTestProperties) || {};
        delete tp[session.id_test_instance];

        if (Object.keys(ca).length === 0) {
          resolve(model.clearAllTestsData(session));
        } else {
          resolve(
            Promise.all([
              model.setCurrAnswers(session, ca, username),
              model.setCurrTestProperties(session, tp),
            ])
          );
        }
      }
    });
  });
};

ongoingTestsSchema.statics.clearAllTestsData = function (session) {
  var username = session.anon_alt_id || session.passport.user;
  return new Promise((resolve, reject) => {
    this.model('OngoingTests').findByIdAndRemove(username, function (err, item) {
      if (err) {
        winston.error('Error on saving OngoingTests ' + err);
        reject(err);
      } else {
        resolve(item);
      }
    });
  });
};

ongoingTestsSchema.statics.getOngoingTests = function (session, username) {
  return new Promise((resolve, reject) => {
    this.model('OngoingTests').findById(username, function (err, rv) {
      if (err) {
        reject(err);
      } else {
        if (rv && rv.currAnswers) {
          session.currAnswers = rv.currAnswers;
          //session.currTestProperties = rv.currTestProperties;
          winston.info(
            `getOngoingTests: recovered currAnswers for ${username}. Length:  ${
              Object.keys(rv.currAnswers).length
            }`
          );
        }
        resolve(rv);
      }
    });
  });
};

ongoingTestsSchema.statics.getCurrTestProperties = function (session, id_test_instance) {
  return new Promise((resolve, reject) => {
    this.model('OngoingTests').findById(session.passport.user, function (err, rv) {
      if (err) {
        reject(err);
      } else {
        if (rv && rv.currTestProperties) {
          resolve(rv.currTestProperties[id_test_instance || session.id_test_instance]);
        } else {
          resolve(null);
        }
      }
    });
  });
};

// find student's answers for given test instance
// username = student's alt_id
// id_test_instance = test instance id
ongoingTestsSchema.statics.getStudentAnswers = function (username, id_test_instance) {
  return new Promise((resolve, reject) => {
    this.model('OngoingTests').findById(username, function (err, rv) {
      if (err) {
        reject(err);
      } else {
        if (rv && rv.currAnswers) {
          resolve(rv.currAnswers[id_test_instance]);
        }
      }
    });
  });
};

//var ongoingTestsModel = mongoose.model('OngoingTests', ongoingTestsSchema, 'OngoingTests');

module.exports = mongoose.model('OngoingTests', ongoingTestsSchema);
