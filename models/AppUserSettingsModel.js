var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var appUserSettingsSchema = new Schema({
  username: String,
  currCourseId: Number,
  currAcademicYearId: Number,
  cmSkin: String,
  markWhitespace: {
    type: Boolean,
    default: false,
  },
  showQuestionHeader: {
    type: Boolean,
    default: false,
  },
});
module.exports = mongoose.model('AppUserSettings', appUserSettingsSchema);
