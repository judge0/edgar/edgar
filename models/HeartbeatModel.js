var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var HeartbeatSchema = new Schema({
  _id: {
    type: Number,
  }, //'test_instance_id': Number,
  id_test: Number,
  username: String,
  fullname: String,
  progress: Number,
  expireTs: {
    type: Date,
    default: Date.now,
  },
  beats: [Schema.Types.Mixed],
});

module.exports = mongoose.model('Heartbeat', HeartbeatSchema);
// db.Heartbeat.createIndex( { "expireTs": 1 }, { expireAfterSeconds: 604800 } )
// // that's SEVEN days.
