var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const winston = require('winston');

QuestionSequenceSchema = new Schema({
  created: String,
  expireTs: {
    type: Date,
    default: Date.now,
  },
  sequence: [Number],
});
QuestionSequenceSchema.statics.insertSequence = async function (userCreated, sequence) {
  return new Promise((resolve, reject) => {
    var newQuestionSequence = new this({
      created: userCreated,
      sequence: sequence,
    });
    newQuestionSequence.save(function (err, qs) {
      if (!err) {
        resolve(qs.id);
      } else {
        reject('error saving sequence:' + JSON.stringify(sequence) + ':' + err);
        winston.error(err);
      }
    });
  });
};

// this is just a wrapper that returns callback to promise so that i can use it more elegantly later
QuestionSequenceSchema.statics.getById = function (id) {
  const model = this.model('QuestionSequence');
  return new Promise((resolve, reject) => {
    model.findById(id, function (err, rv) {
      if (err) {
        winston.error('QuestionSequenceSchema.statics.getById : error ' + err);
        resolve(undefined);
      } else {
        resolve(rv);
      }
    });
  });
};

module.exports = mongoose.model('QuestionSequence', QuestionSequenceSchema);

// Execute this @server:
// db.questionsequences.createIndex( { "expireTs": 1 }, { expireAfterSeconds: 2 * 604800 } )
// db.QuestionSequence.createIndex( { "expireTs": 1 }, { expireAfterSeconds: 2 * 604800 } )
// that's 4 * SEVEN days = 2 weeks.
