var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var winston = require('winston');
var utils = require('../common/utils');
var testLogSchema = new Schema({
  _id: { type: Number }, //'test_instance_id': Number,
  username: String,
  fullname: String,
  events: [Schema.Types.Mixed],
  //   eventsCount: Number,
});
const MAX_EVENT_ITEMS = 5000;
const LAST_EVENT_ITEMS = 4500;

testLogSchema.statics.getLogEvents = function (_id) {
  return new Promise((resolve, reject) => {
    this.model('TestLog').findById(_id, function (err, rv) {
      if (err) {
        reject(err);
      } else {
        if (rv && rv.events) {
          resolve(rv.events);
        } else {
          resolve([]);
        }
      }
    });
  });
};

testLogSchema.statics.sliceLogEvents = function (username, _id) {
  return new Promise((resolve, reject) => {
    const self = this;
    this.model('TestLog').findById(_id, function (err, rv) {
      if (err) {
        reject(err);
      } else {
        let arrSize = rv && rv.events && rv.events.length;
        winston.info('Slicing TestLog for ' + username + ' and _id = ' + _id);
        self.model('TestLog').findOneAndUpdate(
          {
            _id,
          },
          { $set: { events: rv.events.slice(-LAST_EVENT_ITEMS) } },
          {
            upsert: true,
            new: true,
            select: {
              _id: 1,
              arrSize: { $size: '$events' },
            },
          },
          function (err2, item2) {
            if (err2) {
              winston.error('Error on slicing TestLog ' + err2.message);
              reject(err2);
            } else {
              winston.info(
                `TestLog sliced for ${username} and _id = ${_id} from ${arrSize} to ${item2} events.`
              );
              resolve(item2);
            }
          }
        );
      }
    });
  });
};

testLogSchema.statics.logEvent = function (session, entry, currTI) {
  return new Promise((resolve) => {
    entry.mongoTs = new Date();
    if (!session.id_test_instance && !utils.isAllowedAnonymousLogEvent(entry.eventName)) {
      winston.error('************  PANIC **************');
      winston.info(
        'logEvent: ' +
          JSON.stringify({
            username: session.anon_alt_id || (session.passport && session.passport.user),
            id_test_instance: session.id_test_instance,
            currTI: currTI,
            entry,
          })
      );
    }
    let id_test_instance = session.id_test_instance || currTI;
    var username = session.anon_alt_id || (session.passport && session.passport.user);
    if (!username || !id_test_instance) {
      if (!utils.isAllowedAnonymousLogEvent(entry.eventName)) {
        winston.error(
          'Warning: unknown session user (' +
            username +
            ') or (id_test_instance, currTI) = (' +
            session.id_test_instance +
            ', ' +
            currTI +
            ') for log entry:' +
            JSON.stringify(entry)
        );
      }
      resolve(); // don't want logging to crash anything
    } else {
      var query = {
        _id: id_test_instance,
      };
      entry.username = username;
      entry.fullname = session.fullname;
      var update = {
        // id_test: session.id_test,
        //username: username, // session.passport.user,
        //fullname: session.fullname,
        $push: {
          events: entry,
        },
        // eventsCount: { $size: '$events' },
      };
      var options = {
        upsert: true,
        select: {
          _id: 1,
          arrSize: { $size: '$events' },
        },
      };
      const self = this;
      this.model('TestLog').findOneAndUpdate(query, update, options, function (err, item) {
        if (err) {
          winston.error('Error on saving TestLog ', err);
          winston.error('Attempted test query: ' + JSON.stringify(query));
          winston.error('Attempted test update: ' + JSON.stringify(update));
          winston.error('Attempted test log entry is ' + JSON.stringify(entry));
          resolve(); // don't want logging to crash anything
        } else {
          winston.debug('TestLog saved.');
          let arrSize = item && item._doc && item._doc.arrSize;
          if (arrSize > MAX_EVENT_ITEMS) {
            self.sliceLogEvents(username, id_test_instance); // don't wait for this to finish
          }
          resolve(item);
        }
      });
    }
  });
};

//var testLogModel = mongoose.model('TestLog', testLogSchema, 'TestLog'); // collection name

module.exports = mongoose.model('TestLog', testLogSchema);
