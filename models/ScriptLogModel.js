var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var winston = require('winston');
var ScriptLogSchema = new Schema({
  _id: { type: Number }, // id_test_instance_question,
  ts_modified: Date,
  success: Boolean,
  log: String,
});

ScriptLogSchema.statics.log = function (id_test_instance_question, success, log, error) {
  return new Promise((resolve, reject) => {
    var query = {
      _id: id_test_instance_question,
    };
    var update = {
      success,
      _id: id_test_instance_question,
      ts_modified: new Date(),
      log,
      error,
    };
    var options = {
      upsert: true,
    };
    this.model('ScriptLog').findOneAndUpdate(query, update, options, function (err, item) {
      if (err) {
        winston.error('Error on saving ScriptLog ', err);
        winston.error('Attempted test query: ' + JSON.stringify(query));
        winston.error('Attempted test update: ' + JSON.stringify(update));
        resolve(); // don't want logging to crash anything
      } else {
        winston.debug('ScriptLog saved.');
        resolve(item);
      }
    });
  });
};

module.exports = mongoose.model('ScriptLog', ScriptLogSchema);
